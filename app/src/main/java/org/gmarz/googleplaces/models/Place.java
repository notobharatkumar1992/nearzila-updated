package org.gmarz.googleplaces.models;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;

import com.nearzilla.AppDelegate;
import com.networkTask.URLsClass;
import com.utilities.JSONParser;
import com.utilities.UtilsClass;

import org.gmarz.googleplaces.GooglePlaces;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Place implements Parcelable {

    public static GooglePlaces gPlaces;
    public String placeId = "";
    private String mName = "";
    private String mAddress = "";
    private double mLatitude = 0;
    private double mLongitude = 0;
    private String mRating = "";
    private String mReference = "";
    private String mIcon = "";
    private String mIcon_grey = "";
    public String mIsOpenNow = null;
    private PlaceDetails mDetails;
    private double mDistance;
    private String ssid;
    private String suffix;
    private String url;
    private String dateTime;
    private boolean updated;
    public boolean isLocation = false;
    // private String date;
    public boolean valuetest = false;
    boolean checkvaluetru = false;
    public boolean isBussiness;
    public String mCategory;
    private String mBusinessName;
    // private String mPlaceId;
    public String mCatId;
    public String DBisBussiness;
    public String isFavorites = "false";
    public String businesstype;
    public String phoneNo = "";
    JSONArray offers = new JSONArray();

    public void addOfferList(JSONArray jOffer) {
        offers = jOffer;
    }

    public JSONArray getOffersList() {
        return offers;
    }

    public String getIsFavorites() {
        return isFavorites;
    }

    public void setIsFavorites(String isFavorites) {
        this.isFavorites = isFavorites;
    }

    public String getDBisBussiness() {
        return DBisBussiness;
    }

    public void setDBisBussiness(String dBisBussiness) {
        DBisBussiness = dBisBussiness;
    }

    public boolean isValuetest() {
        return valuetest;
    }

    public void setValuetest(boolean valuetest) {
        this.valuetest = valuetest;
    }

    static {
        gPlaces = new org.gmarz.googleplaces.GooglePlaces(URLsClass.GOOGLE_API_KEY);
    }

    public Place() {
    }

    private Place(Parcel in) {
        mName = in.readString();
        mAddress = in.readString();
        mLatitude = in.readDouble();
        mLongitude = in.readDouble();
        mRating = in.readString();
        mIsOpenNow = in.readString();
        mReference = in.readString();
        placeId = in.readString();
        mIcon = in.readString();
        mIcon_grey = in.readString();
        // mIsOpenNow = in.read();
        mDistance = in.readDouble();

        mDetails = in.readParcelable(PlaceDetails.class.getClassLoader());
        dateTime = in.readString();
        isBussiness = in.readInt() == 1 ? true : false;
        businesstype = in.readString();
        phoneNo = in.readString();
        try {
            offers = new JSONArray(in.readString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getmRating() {
        return mRating;
    }

    public void setmRating(String mRating) {
        this.mRating = mRating;
    }

    public String getSuffix() {
        return suffix;
    }

    public String getUrl() {
        return url;
    }

    public Place(JSONObject jsonPlace) {
        try {
            isBussiness = false;

            placeId = jsonPlace.optString("place_id");
            mName = jsonPlace.optString("name");
            mLatitude = jsonPlace.optJSONObject("geometry")
                    .optJSONObject("location").optDouble("lat");
            mLongitude = jsonPlace.optJSONObject("geometry")
                    .optJSONObject("location").optDouble("lng");

            mDistance = UtilsClass.getDistance(mLatitude, mLongitude,
                    AppDelegate.getCustLat(),
                    AppDelegate.getCustLng());
            mRating = jsonPlace.optString("rating");
            mIcon = jsonPlace.optString("icon");
            mIcon_grey = JSONParser.getString(jsonPlace, "category_img_gray");
            try {
                mIsOpenNow = jsonPlace.optJSONObject("opening_hours").optString("open_now");
                JSONArray add = jsonPlace.optJSONObject("opening_hours").optJSONArray("weekday_text");

            } catch (Exception e) {
                // e.printStackTrace();
            }
            mReference = jsonPlace.optString("reference");

            if (jsonPlace.has("vicinity")) {
                mAddress = jsonPlace.optString("vicinity");
            } else {
                mAddress = jsonPlace.optString("formatted_address");
            }

            dateTime = getCurrentDateTime();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public Place(JSONObject jsonPlace, int req) {
        try {
            isBussiness = true;
            /*
             * if (jsonPlace.optString("ssid") != null) { isLocation = false; }
			 * else { isLocation = true; }
			 */
            if (jsonPlace.isNull("ssid")) {
                isLocation = true;
            } else {
                isLocation = false;
            }

            mName = jsonPlace.optString("headline");
            mAddress = jsonPlace.optString("description");
            dateTime = getCurrentDateTime();
            if (isLocation) {// ssid is null
                mLatitude = jsonPlace.optDouble("lat");
                mLongitude = jsonPlace.optDouble("lng");
                mDistance = UtilsClass.getDistance(mLatitude, mLongitude,
                        AppDelegate.getCustLat(),
                        AppDelegate.getCustLng());
                mBusinessName = jsonPlace.optString("businessName");
                businesstype = jsonPlace.optString("businesstype");
                mIsOpenNow = jsonPlace.optString("businesstype");
                phoneNo = jsonPlace.optString("businessPhone");
                placeId = jsonPlace.optString("place_id");
                mCatId = jsonPlace.getString("category_id");
                url = jsonPlace.optString("locationURL");
                mIcon = jsonPlace.optString("category_img");
                mIcon_grey = JSONParser.getString(jsonPlace, "category_img_gray");
                AppDelegate.LogT("mIcon => " + mIcon + ", mIcon_grey = " + mIcon_grey);
            } else {
                // ssid is present
                mLatitude = jsonPlace.optDouble("lat");
                mLongitude = jsonPlace.optDouble("lng");
                url = jsonPlace.optString("url");
                placeId = jsonPlace.optString("id");
                ssid = jsonPlace.optString("ssid");
                mCategory = jsonPlace.optString("category");
                suffix = jsonPlace.optString("suffix");
                mIcon = jsonPlace.optString("category_img");
                mIcon_grey = JSONParser.getString(jsonPlace, "category_img_gray");
                AppDelegate.LogT("mIcon => " + mIcon + ", mIcon_grey = " + mIcon_grey);
                phoneNo = jsonPlace.optString("businessPhone");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // current time and date.
    private String getCurrentDateTime() {
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy, HH:mm:ss");
        return df.format(Calendar.getInstance().getTime());
    }

    public Double getDistance() {
        return mDistance;
    }

    public void setDistance(Double mDistance) {
        this.mDistance = mDistance;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(double latitude) {
        mLatitude = latitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(double longitude) {
        mLongitude = longitude;
    }

    public double getDistanceTo(Location location) {
        Location source = new Location("");
        source.setLatitude(mLatitude);
        source.setLongitude(mLongitude);

        return source.distanceTo(location);
    }

    public String getReference() {
        return mReference;
    }

    public void setReference(String value) {
        mReference = value;
    }

    public void setDetail(PlaceDetails detail) {
        mDetails = detail;
    }

    public PlaceDetails getDetail() {
        return mDetails;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeString(mName);
        out.writeString(mAddress);
        out.writeDouble(mLatitude);
        out.writeDouble(mLongitude);
        out.writeString(mRating);
        out.writeString(mIsOpenNow);
        out.writeString(mReference);
        out.writeString(placeId);
        out.writeString(mIcon);
        out.writeString(mIcon_grey);
        out.writeDouble(mDistance);
        out.writeParcelable(mDetails, flags);
        out.writeString(dateTime);
        out.writeInt(isBussiness ? 1 : 0);
        out.writeString(businesstype);
        out.writeString(phoneNo);
        if (offers != null)
            out.writeString(offers.toString());
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public static final Parcelable.Creator<Place> CREATOR = new Parcelable.Creator<Place>() {

        public Place createFromParcel(Parcel in) {
            return new Place(in);
        }

        public Place[] newArray(int size) {
            return new Place[size];
        }
    };

    public String getIcon() {
        return mIcon;
    }

    public void setIcon(String mIcon) {
        this.mIcon = mIcon;
    }

    public String getIconGrey() {
        return mIcon_grey;
    }

    public void setIconGrey(String mIcon_grey) {
        this.mIcon_grey = mIcon_grey;
    }

    public String isIsOpenNow() {
        return mIsOpenNow;
    }

    public void setIsOpenNow(String mIsOpenNow) {
        this.mIsOpenNow = mIsOpenNow;
    }

    public void setDecice_ssid(String ssid) {
        this.setSsid(ssid);
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setDateTime(String currentDateTime) {
        this.dateTime = currentDateTime;
    }

    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    public String getDateTime() {
        return dateTime;
    }

    public boolean isUpdate() {
        return updated;
    }

    public void setUpdate(boolean updated) {
        this.updated = updated;
    }

    public void setLocation(boolean b) {
        isLocation = b;
    }

    public boolean isLocation() {
        return isLocation;
    }

    @Override
    public String toString() {
        /*	return "Name=" + mName + " Address=" + mAddress + " Lat=" + mLatitude
                + " Lng=" + mLongitude;*/
        return "Name=" + mName + "\n";
        // return super.toString();
    }

	/*
     * @Override public int compare(Place arg0, Place arg1) { // TODO
	 * Auto-generated method stub return
	 * arg0.getDistance().compareTo(arg1.getDistance()); }
	 */

	/*
     * @Override public int compareTo(Place arg0) { // TODO Auto-generated
	 * method stub if (p1.getY() < p2.getY()) return -1; if (p1.getY() >
	 * p2.getY()) return 1; return 0; return (int) (
	 * mDistance-arg0.getDistance()); }
	 */
    /*
	 * @Override public int compare(Place lhs, Place rhs) { // TODO
	 * Auto-generated method stub if (lhs.getDistance() < rhs.getDistance())
	 * return -1; if (lhs.getDistance() > rhs.getDistance()) return 1; return 0;
	 * 
	 * }
	 */

	/*
	 * public String getDate() { return date; }
	 * 
	 * public void setDate(String dateLoc) { date = dateLoc; }
	 */
}