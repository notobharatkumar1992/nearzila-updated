package org.gmarz.googleplaces.models;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

public class PlaceDetails implements Parcelable {

	private String mPhoneNumber = "";
	private String mWebsite = "";
	private ArrayList<PlaceReview> mReviews = new ArrayList<PlaceReview>();
	private String mAddressFull;
	private String mIcon="";
	//private Boolean isOpenNow=null;
	private JSONArray mWeekDayTiming;
	private String mTotalUserRating="";
	private JSONArray type;
	private Boolean isBussiness;
	public String businessName;

	private PlaceDetails(Parcel in) {
		mPhoneNumber = in.readString();
		mWebsite = in.readString();
		businessName = in.readString();
		in.readTypedList(mReviews, PlaceReview.CREATOR);
	}

	private PlaceDetails() {
		// Do nothing.  For returning an empty object when a place has no details.
	}

	public PlaceDetails(JSONObject jsonDetail) {
		try {
			if(jsonDetail.getString("formatted_phone_number") != null){
				mPhoneNumber = jsonDetail.getString("formatted_phone_number");
			}

			if (jsonDetail.has("website")) {
				mWebsite = jsonDetail.optString("website");
			}else{
				mWebsite = jsonDetail.optString("url");
			}

			mAddressFull = jsonDetail.optString("formatted_address");
			mIcon = 	jsonDetail.optString("icon");
			if(jsonDetail.optJSONObject("opening_hours")!=null){
				JSONObject jOpenHours = jsonDetail.optJSONObject("opening_hours");
				//	System.out.println("tttt====="+jOpenHours.optBoolean("open_now"));
				//	if(jOpenHours.optBoolean("open_now")!=null){
				//	isOpenNow = jOpenHours.optBoolean("open_now");
				mWeekDayTiming = jOpenHours.optJSONArray("weekday_text");
			}
			mTotalUserRating = jsonDetail.optString("user_ratings_total"); 
			type = jsonDetail.optJSONArray("types");

			JSONArray jsonReviews = jsonDetail.optJSONArray("reviews");
			if(jsonReviews != null)
				for(int i =0;i<jsonReviews.length();i++) {
					PlaceReview review = new PlaceReview(jsonReviews.getJSONObject(i));
					mReviews.add(review);
				}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public PlaceDetails(Boolean isBussiness, JSONObject jsonDetail) throws JSONException {
		this.isBussiness = isBussiness;
		businessName = jsonDetail.optString("businessName");
		mPhoneNumber = jsonDetail.optString("contactNumber");
		mWebsite = jsonDetail.optString("locationURL");
		mAddressFull = jsonDetail.optString("location_address");
		mIcon = 	jsonDetail.optString("image");			
		//mTotalUserRating = jsonDetail.optString("user_ratings_total"); 
		//type = jsonDetail.optJSONArray("businesstype");
	}

	public static PlaceDetails getEmpty() {
		return new PlaceDetails();
	}

	public String getPhoneNumber() {
		return mPhoneNumber;
	}

	public boolean phoneNumerIsValid() {
		return mPhoneNumber.matches("^\\(?([0-9]{3})\\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$");
	}

	public String getWebsite() {
		return mWebsite;
	}

	public boolean websiteIsValid() {
		return (mWebsite != "");
	}

	public ArrayList<PlaceReview> getReviews() {
		return mReviews;
	}

	public String getmPhoneNumber() {
		return mPhoneNumber;
	}

	public String getAddressFull() {
		return mAddressFull;
	}

	public String getIcon() {
		return mIcon;
	}

	/*	public boolean isOpenNow() {
		return isOpenNow;
	}
	 */
	public JSONArray getWeekDayTiming() {
		return mWeekDayTiming;
	}

	public String getTotalUserRating() {
		return mTotalUserRating;
	}

	public boolean hasReviews() {
		return (mReviews.size() > 0);
	}

	public int describeContents() {
		return 0;
	}

	public void writeToParcel(Parcel out, int flags) {
		out.writeString(mPhoneNumber);
		out.writeString(mWebsite);
		out.writeTypedList(mReviews);
	}

	public JSONArray getType() {
		return type;
	}

	public void setType(JSONArray type) {
		this.type = type;
	}

	public static final Parcelable.Creator<PlaceDetails> CREATOR = new Parcelable.Creator<PlaceDetails>() {

		public PlaceDetails createFromParcel(Parcel in) {
			return new PlaceDetails(in);
		}

		public PlaceDetails[] newArray(int size) {
			return new PlaceDetails[size];
		}
	};
}
