package com.utilities;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

public class MyLocation {
	Timer timer1;
	LocationManager lm;
	LocationResult locationResult;
	boolean gps_enabled=false;
	private Activity context;
	boolean network_enabled=false;

	public boolean getLocation(Activity context, LocationResult result, int tillTimeToWaitForLocation)
	{
		this.context = context;
		//I use LocationResult callback class to pass location value from MyLocation to user code.
		locationResult=result;
		
		timer1=new Timer();		

		if(lm==null)
			lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		
	

		//exceptions will be thrown if provider is not permitted.
		try{gps_enabled=lm.isProviderEnabled(LocationManager.GPS_PROVIDER);}catch(Exception ex){}
		try{network_enabled=lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);}catch(Exception ex){}

		if(gps_enabled)
			lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListenerGps);

		if(network_enabled)
			lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListenerNetwork);
		
		timer1.schedule(new GetLastLocation(), tillTimeToWaitForLocation*1000);
		return true;
	}

	public void removeUpdate(){
		try {
			lm.removeUpdates(locationListenerGps);
			lm.removeUpdates(locationListenerNetwork);
			if(timer1 != null){
				timer1.cancel();
				//timer1 = null;
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	LocationListener locationListenerGps = new LocationListener() 
	{
		public void onLocationChanged(Location location) 
		{			
			if(location != null){		
				timer1.cancel();
				lm.removeUpdates(locationListenerGps);
				lm.removeUpdates(this);

				locationResult.gotLocation(context,location);
			}
		}
		public void onProviderDisabled(String provider) {}
		public void onProviderEnabled(String provider) {}
		public void onStatusChanged(String provider, int status, Bundle extras) {}
	};

	LocationListener locationListenerNetwork = new LocationListener()
	{
		public void onLocationChanged(Location location) 
		{			
			if(location != null){				
				timer1.cancel();
				lm.removeUpdates(locationListenerNetwork);
				lm.removeUpdates(this);

				locationResult.gotLocation(context,location);
			}
		}
		public void onProviderDisabled(String provider) {}
		public void onProviderEnabled(String provider) {}
		public void onStatusChanged(String provider, int status, Bundle extras) {}
	};

	class GetLastLocation extends TimerTask {
		@Override
		public void run() {				
			Location  gps_loc=null;
			if(gps_enabled)
				gps_loc=lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			Location net_loc = null;
			if(network_enabled)
				net_loc=lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

			//if there are both values use the latest one
			try {
				if(gps_loc!=null){
					timer1.cancel();
					lm.removeUpdates(locationListenerGps);
					locationResult.gotLocation(context,gps_loc);
					return;
				}

				if(net_loc!=null){
					timer1.cancel();
					lm.removeUpdates(locationListenerNetwork);	
					locationResult.gotLocation(context,net_loc);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static abstract class LocationResult
	{
		public abstract void gotLocation(Context context,Location location);
	}
}


/*package com.mainPack.serviceClass;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

public class MyLocation {
	Timer timer1;
	LocationManager lm;
	LocationResult locationResult;
	boolean gps_enabled=false;
	boolean network_enabled=false;
	boolean passive_enabled = false;

	public boolean getLocation(Activity context, LocationResult result)
	{
		//I use LocationResult callback class to pass location value from MyLocation to user code.
		locationResult=result;
		if(lm==null)
			lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

		//exceptions will be thrown if provider is not permitted.
		try{gps_enabled=lm.isProviderEnabled(LocationManager.GPS_PROVIDER);}catch(Exception ex){}
		try{network_enabled=lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);}catch(Exception ex){}
		try{passive_enabled=lm.isProviderEnabled(LocationManager.PASSIVE_PROVIDER);}catch(Exception ex){}

		//don't start listeners if no provider is enabled
		if(!gps_enabled && !network_enabled && !passive_enabled)
			return false;

		context.runOnUiThread(new Runnable() 
		{			
			public void run() 
			{

				if(network_enabled)
				{
					System.out.println("Natwork enable");
					lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListenerNetwork);
				}
				else if(gps_enabled)
				{
					System.out.println("GPS enable");
					lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListenerGps);
				}
				else if(passive_enabled)
				{
					System.out.println("Passive enable");
					lm.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, 0, 0, locationListenerPassive);
				}
			}
		});

		timer1=new Timer();
		timer1.schedule(new GetLastLocation(), 20000);
		return true;
	}

	LocationListener locationListenerGps = new LocationListener() 
	{
		public void onLocationChanged(Location location) 
		{
			System.out.println("GPS listener");
			timer1.cancel();
			locationResult.gotLocation(location);
			lm.removeUpdates(this);
			lm.removeUpdates(locationListenerNetwork);
		}
		public void onProviderDisabled(String provider) {}
		public void onProviderEnabled(String provider) {}
		public void onStatusChanged(String provider, int status, Bundle extras) {}
	};

	LocationListener locationListenerNetwork = new LocationListener()
	{
		public void onLocationChanged(Location location) 
		{
			System.out.println("Natwork listener");
			timer1.cancel();
			locationResult.gotLocation(location);
			lm.removeUpdates(this);
			lm.removeUpdates(locationListenerGps);
		}
		public void onProviderDisabled(String provider) {}
		public void onProviderEnabled(String provider) {}
		public void onStatusChanged(String provider, int status, Bundle extras) {}
	};

	LocationListener locationListenerPassive = new LocationListener()
	{
		public void onLocationChanged(Location location) 
		{
			System.out.println("Passive listener");
			timer1.cancel();
			locationResult.gotLocation(location);
			lm.removeUpdates(this);
			lm.removeUpdates(locationListenerPassive);
		}
		public void onProviderDisabled(String provider) {}
		public void onProviderEnabled(String provider) {}
		public void onStatusChanged(String provider, int status, Bundle extras) {}
	};

	class GetLastLocation extends TimerTask {
		@Override
		public void run() 
		{
			lm.removeUpdates(locationListenerGps);
			lm.removeUpdates(locationListenerNetwork);
			lm.removeUpdates(locationListenerPassive);

			Location net_loc=null, gps_loc=null , passive_loc=null;
			if(gps_enabled)
			{
				System.out.println("GPS last Known location");
				gps_loc=lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			}
			else if(network_enabled)
			{
				System.out.println("Natwork last Known location");
				net_loc=lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
			}
			else if(passive_enabled)
			{
				System.out.println("Passive last Known location");
				passive_loc = lm.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
			}


			//if there are both values use the latest one
			if(gps_loc!=null && net_loc!=null)
			{
				if(gps_loc.getTime()>net_loc.getTime())
				{
					System.out.println("GPS last known Location is latest");
					locationResult.gotLocation(gps_loc);
				}
				else if(net_loc.getTime() > passive_loc.getTime())
				{
					System.out.println("Natwork last known Location is latest");
					locationResult.gotLocation(net_loc);
				}
				else
				{
					System.out.println("Natwork last known Location is latest");
					locationResult.gotLocation(passive_loc);
				}
				return;
			}

			if(gps_loc!=null)
			{
				System.out.println("GPS not null");
				locationResult.gotLocation(gps_loc);
				return;
			}
			else if(net_loc!=null)
			{
				System.out.println("Natwork not null");
				locationResult.gotLocation(net_loc);
				return;
			}
			else if(passive_loc!= null)
			{
				System.out.println("Passive not null");
				locationResult.gotLocation(passive_loc);
				return;
			}

			try 
			{
				locationResult.gotLocation(null);
			} 
			catch (Exception e) 
			{			
				e.printStackTrace();
			}
		}
	}

	public static abstract class LocationResult
	{
		public abstract void gotLocation(Location location);
	}
}
 */