package com.networkTask;

public interface URLsClass {
	public static final String GOOGLE_API_KEY = "AIzaSyCRLa4LQZWNQBcjCYcIVYA45i9i8zfClqc";
	public final String reverseGeoCodingApi = "http://maps.googleapis.com/maps/api/geocode/json";
	public final String address = "address";
	public final String sensor= "sensor";
	public final static String categories = "Airport|Art gallery|Bakery|Beauty salon|Book store|us station|Bank|Bars|Cafe|Car repair|Car wash|Casino|Clothing store|Convenience store|Gas station|Grocery or Supermarket|Gym|Hair care|Hardware store|Hospital|Hotels|Library|Liquor Store|Movie Theater|Museum|Night Club|Park|Pharmacy|Post office|Restaurant|School|Shoe Store|Shopping Mall|Store|University";
	
	//public final static String baseUrl = "http://192.168.1.53/suresh/rigges/webservices/";
	//public final static String baseUrl = "http://rigges.com/temp1/webservices/";
	public final static String baseUrl = "http://rigges.com/temp2/webservices/";
	public final static int login_type_normal = 1;
	public final static int login_type_fb = 2;
	
	// Service Type
	public final static String service_type_get_categories = baseUrl+"getCategory";
	public final static String service_type_getLocationDevice = baseUrl+"getLocationDevice";
	public final static String service_type_getBusinessHits = baseUrl+"businessHits";
	public final static String service_type_report = baseUrl+"submitReport";
	public final static String service_type_feedback = baseUrl+"submitFeedback";
	
	
	//Device Type
	public final static String DEVICE_TYPE = "Android";
	  
	// parameters
	public final static String p_api_key = "apiKey";
	
	
	public final static String p_device_brand = "deviceBrand";
	public final static String p_device_type = "deviceType";
	public final static String p_device_os = "deviceOs";
	
	public final static String p_user_name = "userName";
	public final static String p_first_name = "first_name";
	public final static String p_last_name = "last_name";
	
	public final static String p_fb_id = "fb_id";
	public final static String p_password = "password";
	public final static String p_type = "type";
	public final static String p_dob = "dob";
	public final static String p_email = "email";
	public final static String p_gender = "gender";
	public final static String p_country_code = "country_code";
	public final static String p_mobile_no = "mobileNo";
	
	public final static String p_user = "user";
	public final static String p_userid = "userId";
	public final static String p_list = "list";
	public final static String p_categories = "categories";
	public final static String p_message = "message";
	public final static String p_app_list = "appList";
	public final static String p_appName = "appName";
	public final static String p_appPack = "appPack";
	public final static String p_app_url = "app_url";
	
	public final static String p_friendName = "friendName";
	public final static String p_friendId = "friendId";
	public final static String p_status = "status";
	public final static String p_channel_id = "channel_Id";
	public final static String p_contact_list = "contactList";
	
	public final static String p_name = "name";
	public final static String p_description = "description";
	public final static String p_channel_type = "channel_type";
	public final static String p_channel_mode = "channel_mode";
	public final static String p_fileToUpload = "fileToUpload";
	
	public final static String p_show_subscribe_feeds = "show_subscribe_feeds";
	public final static String p_allow_post = "allow_post";
	public final static String p_allow_search = "allow_search";
	public final static String p_allow_feeds = "allow_feeds";
	
	public final static String p_receiver_id = "receiver_id";
	public final static String  p_postId = "postId";
	
	
	public final String and = "&";	
	
	public final static int POST = 1;
	public final static int GET = 2;
	
	public final static String status = "status";
	public final static String results = "response";
	public final static int OK = 1;
	public final static int NOTOK = 0;
	
	public final int fromAddressToLatLng = 101;
}
