package com.networkTask;

import java.util.HashMap;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;


public class MyAsyncTask extends AsyncTask<String, Void, ApiResponse> implements URLsClass
{	
	private Context context = null;
	private String progressMsg = null;
	private ProgressDialog progressDialog;
	private CallBackListener callBackListener = null;
	private ParsingClass parsingClass= null;
	//private int parsingType = 1;
	public boolean IsProgressBarShow = true;
	private boolean isPost = false;
	private HashMap<String, Object> params;
	private Boolean isFileUploading = false;

	public CallBackListener getCallBackListener() 
	{
		return callBackListener;
	}

	public void setCallBackListener(CallBackListener callBackListener) 
	{
		this.callBackListener = callBackListener;
	}

	public MyAsyncTask(Context context , HashMap<String, Object> params, String progressMsg, Boolean isPost)
	{
		this.context  = context;
		this.progressMsg = progressMsg;
		//this.parsingType = parsingType;
		parsingClass =  ParsingClass.getInstance(context);
		this.isPost  = isPost;
		this.params = params;
	}

	public MyAsyncTask(Context context , HashMap<String, Object> params, String progressMsg, Boolean isPost, Boolean isFileUploading)
	{
		this.context  = context;
		this.progressMsg = progressMsg;
		//this.parsingType = parsingType;
		parsingClass =  ParsingClass.getInstance(context);
		this.isPost  = isPost;
		this.isFileUploading  = isFileUploading;
		this.params = params;
	}

	@Override
	protected void onPreExecute() 
	{
		super.onPreExecute();
		if(IsProgressBarShow)
		{
			try 
			{

				progressDialog = new ProgressDialog(context);
				progressDialog.setMessage(progressMsg);
				progressDialog.setCancelable(false);
				if(!progressDialog.isShowing())
					progressDialog.show();
			} 
			catch (Exception e) 
			{			
				e.printStackTrace();
			}
			catch (Throwable e)
			{
				e.printStackTrace();
			}
		}
	}

	@Override
	protected ApiResponse doInBackground(String... arg0) {
		ApiResponse apiResponse = new ApiResponse();
	//	apiResponse = MyApplication.getApplication().getApisResponce();
		for(String string : arg0) {
			apiResponse = parsingClass.startUpConfigrationOfParsingPost(string, params, apiResponse, isPost);
		}
		return apiResponse;
	}

	@Override
	protected void onPostExecute(ApiResponse result) 
	{
		super.onPostExecute(result);
		if(progressDialog != null)
		{			
			try 
			{
				progressDialog.dismiss();
			}
			catch (Throwable e) 
			{			
				e.printStackTrace();
			}
		}

		if(callBackListener != null)
			callBackListener.callback(result);		
	}

	public interface CallBackListener
	{
		public void callback(Object object);
	}	
}

/*package com.jaipurnews.dis.networkTask;

import java.util.HashMap;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;

import com.jaipurnews.dis.MyApplication;

public class MyAsyncTask extends AsyncTask<String, Void, ApiResponse> implements URLsClass
{	
	private Context context = null;
	private String progressMsg = null;
	private ProgressDialog progressDialog;
	private CallBackListener callBackListener = null;
	private ParsingClass parsingClass= null;
	//private int parsingType = 1;
	public boolean IsProgressBarShow = true;
	private boolean isPost = false;
	private HashMap<String, Object> params;

	public CallBackListener getCallBackListener() 
	{
		return callBackListener;
	}

	public void setCallBackListener(CallBackListener callBackListener) 
	{
		this.callBackListener = callBackListener;
	}


	public MyAsyncTask(Context context , HashMap<String, Object> params, String progressMsg, Boolean isPost)
	{
		this.context  = context;
		this.progressMsg = progressMsg;
		//this.parsingType = parsingType;
		parsingClass =  ParsingClass.getInstance(context);
		this.isPost  = isPost;
		this.params = params;
	}

	@Override
	protected void onPreExecute() 
	{
		super.onPreExecute();
		if(IsProgressBarShow)
		{
			try 
			{
				if(context instanceof FragmentActivity){
					progressDialog = new ProgressDialog((FragmentActivity)context);
				}else{
					progressDialog = new ProgressDialog((Activity)context);
				}
				progressDialog.setMessage(progressMsg);
				progressDialog.setCancelable(false);
				if(!progressDialog.isShowing())
					progressDialog.show();
			} 
			catch (Exception e) 
			{			
				e.printStackTrace();
			}
			catch (Throwable e)
			{
				e.printStackTrace();
			}
		}
	}

	@Override
	protected ApiResponse doInBackground(String... arg0) {
		ApiResponse apiResponse = null;

		apiResponse = MyApplication.getApplication().getApisResponce();
		for(String string : arg0) {
			apiResponse = parsingClass.startUpConfigrationOfParsingPost(string, params, apiResponse, isPost);
		}
		return apiResponse;
	}

	@Override
	protected void onPostExecute(ApiResponse result) 
	{
		super.onPostExecute(result);
		if(progressDialog != null)
		{			
			try 
			{
				progressDialog.dismiss();
			}
			catch (Throwable e) 
			{			
				e.printStackTrace();
			}
		}

		if(callBackListener != null)
			callBackListener.callback(result);		
	}

	public interface CallBackListener
	{
		public void callback(Object object);
	}	
}
 */