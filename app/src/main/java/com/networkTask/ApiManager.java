
package com.networkTask;

import java.util.HashMap;

import com.baseClasses.BaseFragment;
import com.baseClasses.BaseFragmentActivity;
import com.baseClasses.Constants;
import com.preference.MySharedPreferences;
import com.utilities.DeviceInfomation;
import com.utilities.UtilsClass;

public class ApiManager implements URLsClass {

	private static ApiManager apiManager = null;
	public static ApiManager getInstance(){
		if(apiManager == null) 
			apiManager = new ApiManager();

		return apiManager;
	}

	public void getCategories(BaseFragmentActivity context){
		if(Is_Internet_Available_Class.internetIsAvailable(context)){	
			String urlsArr[] = {URLsClass.service_type_get_categories};
			//HashMap<String, Object> params = new HashMap<String, Object>();
			//params.put(Constants.USER_ID, MySharedPreferences.getInstance().getString(fragment, Constants.USER_ID, ""));
			//params.put(Constants.API_KEY, Constants.API_KEY_VALUE);
			context.serviceCaller(context, urlsArr, null, true, false, Constants.getCat, false);
		}else {
			UtilsClass.plsStartInternet(context);			
		}	
	}
	
	public void getLocations(BaseFragmentActivity context, double lat, double lng, String ssid, String catId, String locationName,double radius){
		if(Is_Internet_Available_Class.internetIsAvailable(context)){	
			String urlsArr[] = {URLsClass.service_type_getLocationDevice};
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put(Constants.deviceId, DeviceInfomation.getAndroid_device_id(context));
			params.put(Constants.lat, lat);
			params.put(Constants.lng, lng);
			params.put(Constants.locationName, locationName);
			params.put(Constants.ssid, ssid);
			params.put(Constants.catID, catId);
			params.put(Constants.radius, radius)	;	
			//context.serviceCaller(context, urlsArr, params, true, false, Constants.getLocationDevice, true);
		}else {
			UtilsClass.plsStartInternet(context);			
		}
		
	}
}
