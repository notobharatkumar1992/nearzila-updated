package com.nearzilla;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.baseClasses.BaseFragmentActivity;
import com.baseClasses.Constants;
import com.nearzilla.fragments.FavoritesFragment;
import com.nearzilla.fragments.HistoryFragment;
import com.utilities.UtilsClass;

import org.gmarz.googleplaces.models.Place;

import java.util.ArrayList;

//This is history activity where the all wifi devices history is managing.
public class HistoryActivity extends BaseFragmentActivity implements
		OnClickListener, OnRefreshListener {
	private Button FavButton, HisButton;
	ImageView BacButton;
	int where;
	ArrayList<Place> dbArray;
	RelativeLayout show_Linear;
	int flag = 0;
	Button delete, deleteAll;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_history);
		initUi();
		setListner();
		Bundle bundle = getIntent().getExtras();
		if (bundle != null) {
			where = bundle.getInt(Constants.type);
			System.out.println("wheretype-----" + where);
		}
		enableBackButton();
		ActionBar actionBar = getSupportActionBar();
		LayoutInflater mInflater = LayoutInflater.from(this);
		View mCustomView = mInflater.inflate(
				R.layout.actionbar_custom_view_home, null);
		actionBar.setCustomView(mCustomView);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setDisplayShowTitleEnabled(false);
		// BacButton = (ImageView) findViewById(R.id.imvBack);
		// delete_record = (ImageView) findViewById(R.id.delete_record);
		// delete_record.setOnClickListener(this);

		if (where == 0) {
			getSupportActionBar().setTitle("History");
			replaceFragement(HistoryFragment.getInstance(null),
					HistoryFragment.TAG);
			HisButton.setBackgroundResource(R.drawable.btn_selectright);
			// HisButton.setBackgroundColor(Color.parseColor("#000000"));
			FavButton.setBackgroundResource(R.drawable.btn_unselect);
			findViewById(R.id.view_his).setVisibility(View.VISIBLE);
			findViewById(R.id.view_fav).setVisibility(View.GONE);
		} else {
			getSupportActionBar().setTitle("Favorites");
			replaceFragement(FavoritesFragment.getInstance(null),
					FavoritesFragment.TAG);
			FavButton.setBackgroundResource(R.drawable.btn_select);
			HisButton.setBackgroundResource(R.drawable.btn_unselect);
			findViewById(R.id.view_fav).setVisibility(View.VISIBLE);
			findViewById(R.id.view_his).setVisibility(View.GONE);
		}
		//

	}

	private void setListner() {
		// TODO Auto-generated method stub
		FavButton.setOnClickListener(this);
		HisButton.setOnClickListener(this);
		show_Linear.setOnClickListener(this);
		delete.setOnClickListener(this);
		deleteAll.setOnClickListener(this);
	}

	private void initUi() {
		// TODO Auto-generated method stub
		FavButton = (Button) findViewById(R.id.fav_but);
		FavButton.setTypeface(AppDelegate.proxima_bold);
		HisButton = (Button) findViewById(R.id.his_but);
		HisButton.setTypeface(AppDelegate.proxima_bold);
		show_Linear = (RelativeLayout) findViewById(R.id.lin_his);
		delete = (Button) findViewById(R.id.delete_animate);
		deleteAll = (Button) findViewById(R.id.deleteall_animate);

	}

	// back button click.
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.fav_but:
			findViewById(R.id.view_fav).setVisibility(View.VISIBLE);
			findViewById(R.id.view_his).setVisibility(View.GONE);
			where = 1;
			getSupportActionBar().setTitle("Favorites");
			HisButton.setBackgroundResource(R.drawable.btn_unselect);
			FavButton.setBackgroundResource(R.drawable.btn_select);
			replaceFragement(FavoritesFragment.getInstance(null),
					FavoritesFragment.TAG);
			break;
		case R.id.his_but:
			findViewById(R.id.view_his).setVisibility(View.VISIBLE);
			findViewById(R.id.view_fav).setVisibility(View.GONE);
			where = 0;
			getSupportActionBar().setTitle("History");
			FavButton.setBackgroundResource(R.drawable.btn_unselect);
			HisButton.setBackgroundResource(R.drawable.btn_selectright);
			replaceFragement(HistoryFragment.getInstance(null),
					HistoryFragment.TAG);
			break;
		case R.id.delete_animate:
			if (where == 0) {
				HistoryFragment fragment = (HistoryFragment) getFragmentByTag(HistoryFragment.TAG);
				fragment.delete();
			} else {
				FavoritesFragment fragment = (FavoritesFragment) getFragmentByTag(FavoritesFragment.TAG);
				fragment.delete();
			}
			break;
		case R.id.deleteall_animate:
			if (where == 0) {
				HistoryFragment fragment = (HistoryFragment) getFragmentByTag(HistoryFragment.TAG);
				fragment.deleteAll();
			} else {
				FavoritesFragment fragment = (FavoritesFragment) getFragmentByTag(FavoritesFragment.TAG);
				fragment.deleteAll();
				;
			}
			break;
		default:
			break;
		}
	}

	@Override
	protected void initControls(Bundle savedInstanceState) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void setValueOnUI() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		// getMenuInflater().inflate(R.menu.menu_history, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case R.id.action_history:
			if (flag == 0) {
				delete.setVisibility(View.VISIBLE);
				deleteAll.setVisibility(View.VISIBLE);

				View[] view = { show_Linear };
				UtilsClass.animationSlide_in_down(this, view, 1000, 0);
				delete.setOnClickListener(this);
				deleteAll.setOnClickListener(this);
				flag = 1;
			} else {
				/*
				 * delete.setVisibility(View.GONE);
				 * deleteAll.setVisibility(View.GONE);
				 */
				View[] view = { show_Linear, delete, deleteAll };
				UtilsClass.animationSlide_out_up1(this, view, 1000, 0, delete,
						deleteAll);
				delete.setOnClickListener(null);
				deleteAll.setOnClickListener(null);
				FavButton.setOnClickListener(this);
				HisButton.setOnClickListener(this);
				flag = 0;
			}
			break;
		case android.R.id.home:
			finish();
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
}
