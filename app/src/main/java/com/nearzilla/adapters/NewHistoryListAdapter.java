package com.nearzilla.adapters;

import android.app.Activity;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.baseClasses.Constants;
import com.fortysevendeg.swipelistview.SwipeListView;
import com.nearzilla.AppDelegate;
import com.nearzilla.R;
import com.nearzilla.fragments.CommonRedirect;
import com.nearzilla.fragments.SettingFragment;
import com.nearzilla.interfaces.OnListItemClickListener;
import com.squareup.picasso.Picasso;

import org.gmarz.googleplaces.models.Place;
import org.gmarz.googleplaces.models.PlaceDetails;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import carbon.widget.RelativeLayout;
import carbon.widget.TextView;

public class NewHistoryListAdapter extends BaseAdapter {

    private List<Place> list;
    private Activity context;
    private LayoutInflater inflator;
    private ViewHolder holder;
    PlaceDetails details;
    List<Place> subList;
    List<Place> secList;
    SwipeListView listView;
    int i;
    private String disType;
    private OnListItemClickListener itemClickListener;

    public final static int ADAPTER_RECENT = 0, ADAPTER_FAV = 1, ADAPTER_HIS = 2;
    public int type = ADAPTER_RECENT;

    public NewHistoryListAdapter(Activity fragment, List<Place> list2, OnListItemClickListener itemClickListener, int type) {
        this.context = fragment;
        this.list = list2;
        inflator = LayoutInflater.from(fragment);
        SharedPreferences sharedpreferences = fragment.getSharedPreferences(Constants.MyPREFERENCES, 0);
        disType = sharedpreferences.getString(Constants.disType, SettingFragment.miles);
        this.itemClickListener = itemClickListener;
        this.type = type;
    }

    public void addPlaces(List<Place> list2, int i, SwipeListView listView) {
        System.out.println("indexadapter" + i);
        this.i = i;
        this.listView = listView;
        this.list.addAll(list2);
        ArrayList<Place> temp = (ArrayList<Place>) ((ArrayList<Place>) this.list)
                .clone();
        try {
            subList = temp.subList(0, i);
            secList = temp.subList(i, list.size());

            System.out.println("Before sort==" + secList);

            this.list.clear();
            this.list.addAll(subList);
            Collections.sort(secList, new Comparator<Place>() {
                @Override
                public int compare(Place lhs, Place rhs) {
                    // TODO Auto-generated method stub
                    if (lhs.getDistance() < rhs.getDistance())
                        return -1;
                    if (lhs.getDistance() > rhs.getDistance())
                        return 1;
                    return 0;
                }
            });
            this.list.addAll(secList);
            System.out.println("Aftersort==" + secList);
            System.out.println("hole list==" + list.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (i > 0)
            listView.setTillToSwipe(i);
        listView.getSelectedItemPosition();
        notifyDataSetChanged();

        int selectionPos = getCount() - list2.size();
        listView.setSelection(selectionPos - 2);
    }

    public List<Place> getList() {
        return list;
    }

    @Override
    public int getCount() {
        return this.list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        if (view == null) {
            view = inflator.inflate(R.layout.new_custom_raw_demo, null);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        final Place item = list.get(position);
        holder.tv_c_title.setTag(position);
        view.setTag(R.id.title, list.get(position));

//        holder.tv_c_title.setTypeface(AppDelegate.proxima_bold);
        holder.tv_c_url.setTypeface(AppDelegate.proxima_regular);
        holder.tv_c_description.setTypeface(AppDelegate.proxima_semibold);

        holder.tv_c_title.setText(/*item.mCatId + " " + */item.getName());
        holder.tv_c_url.setText(item.getUrl());
        holder.tv_c_url.setVisibility(View.GONE);
        holder.tv_c_description.setText(item.getAddress());

        item.getLongitude();
        AppDelegate.LogT("history adapter => " + item.getIcon() + ", " + item.getIconGrey());
        if (!list.get(position).getIconGrey().isEmpty()) {
            Picasso.with(context).load(item.getIconGrey()).into(holder.imgV);
        } else {
            Picasso.with(context)
                    .load("https://cdn2.iconfinder.com/data/icons/windows-8-metro-style/48/wifi.png")
                    .into(holder.imgV);
        }

        holder.rl_c_front.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
                Log.d("test", "holder.rl_c_front clicked => " + item.getUrl());
                try {
                    if (type != ADAPTER_HIS)
                        AppDelegate.LogT("data saved => " + CommonRedirect.saveToDB(context, item));
                    AppDelegate.openURL(context, item.getUrl());
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
            }
        });

        holder.img_c_dot.setSelected(item.getIsFavorites().equalsIgnoreCase("true"));
        holder.img_c_dot.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
                try {
//                    AppDelegate.LogT("date saved => " + CommonRedirect.saveToDB(context, item));
                    if (itemClickListener != null) {
                        itemClickListener.setOnListItemClickListener("place", position);
                    }
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
            }
        });
        return view;
    }

    private static class ViewHolder {
        // public ImageView imgV_arrow;
        private TextView tv_c_title = null, tv_c_url, tv_c_description;
        public ImageView imgV;
        public carbon.widget.ImageView img_c_dot;
        RelativeLayout rl_c_front;

        // private TextView tv_price;

        // public ViewGroup deleteHolder;
        // public Button addFavButton;

        ViewHolder(View view) {
            imgV = (ImageView) view.findViewById(R.id.imgV);
            img_c_dot = (carbon.widget.ImageView) view.findViewById(R.id.img_c_dot);
            // imgV_arrow = (ImageView) view.findViewById(R.id.imgV_arrow);
            tv_c_title = (TextView) view.findViewById(R.id.tv_c_title);
            tv_c_url = (TextView) view.findViewById(R.id.tv_c_url);
            tv_c_description = (TextView) view.findViewById(R.id.tv_c_description);
            // tv_price = (TextView) view.findViewById(R.id.expand_text);
            // deleteHolder = (ViewGroup) view.findViewById(R.id.holder);
            // addFavButton = (Button) view.findViewById(R.id.favadd_buton);

            rl_c_front = (RelativeLayout) view.findViewById(R.id.rl_c_front);
        }
    }
}
