package com.nearzilla.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nearzilla.AppDelegate;
import com.nearzilla.R;
import com.nearzilla.interfaces.OnListItemClickListener;
import com.squareup.picasso.Picasso;

import org.gmarz.googleplaces.models.Place;

import java.util.List;

public class NewRecentListAdapter extends RecyclerView.Adapter<RecentListViewHoldersDemo> {

    private List<Place> list;
    private Activity mContext;
    private OnListItemClickListener itemClickListener;

    public NewRecentListAdapter(Activity mContext, List<Place> list, OnListItemClickListener itemClickListener) {
        this.list = list;
        this.mContext = mContext;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public RecentListViewHoldersDemo onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.new_custom_raw_demo, null);
        RecentListViewHoldersDemo rcv = new RecentListViewHoldersDemo(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final RecentListViewHoldersDemo holder, final int position) {
        try {
            final Place item = list.get(position);
            holder.tv_c_title.setTag(position);
            holder.tv_c_title.setText(/*item.mCatId + " " + */item.getName());
            holder.tv_c_url.setText(item.getUrl());
            holder.tv_c_url.setVisibility(View.GONE);
            holder.tv_c_description.setText(item.getAddress());

            item.getLongitude();
            AppDelegate.LogT("Recent adapter => " + item.getIcon() + ", " + item.getIconGrey());
            if (!list.get(position).getIcon().isEmpty()) {
                Picasso.with(mContext).load(item.getIcon()).into(holder.imgV);
            } else {
                Picasso.with(mContext)
                        .load("https://cdn2.iconfinder.com/data/icons/windows-8-metro-style/48/wifi.png")
                        .into(holder.imgV);
            }

            holder.rl_c_front.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    Log.d("test", "holder.rl_c_front clicked => " + item.getUrl());
                    try {

                        AppDelegate.openURL(mContext, item.getUrl());
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            });

            holder.img_c_dot.setSelected(item.getIsFavorites().equalsIgnoreCase("true"));
            holder.img_c_dot.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    try {
//                    AppDelegate.LogT("date saved => " + CommonRedirect.saveToDB(mContext, item));
                        if (itemClickListener != null) {
                            itemClickListener.setOnListItemClickListener("place", position);
                        }
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            });
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public int getItemCount() {
        return this.list.size();
    }
}
