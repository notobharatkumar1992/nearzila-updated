package com.nearzilla.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.baseClasses.Constants;
import com.nearzilla.AppDelegate;
import com.nearzilla.R;
import com.nearzilla.db.DBHelper;
import com.nearzilla.fragments.SettingFragment;
import com.squareup.picasso.Picasso;
import com.utilities.UtilsClass;

import org.gmarz.googleplaces.models.Place;

import java.util.ArrayList;

//This is simple adapter to show data in listview with multiple values.

public class WifiListHistoryAdapterNew extends BaseAdapter implements
		Filterable {

	private ArrayList<Place> list;
	private Context _context;
	private boolean isHiostory;
	private boolean isLocation = true;
	public ArrayList<Place> orig;
	private LayoutInflater inflator;
	// private ViewHolder holder;
	private int flag;
	boolean[] checkBoxState;
	private String disType;
	static boolean chkorNot = false;

	public WifiListHistoryAdapterNew(Context context, int layoutID,
			ArrayList<Place> data, boolean isHistory, int flag) {
		_context = context;
		this.list = data;
		this.isHiostory = isHistory;
		inflator = LayoutInflater.from(context);
		this.flag = flag;
		checkBoxState = new boolean[list.size()];
		SharedPreferences sharedpreferences = context.getSharedPreferences(
				Constants.MyPREFERENCES, 0);
		disType = sharedpreferences.getString(Constants.disType,
				SettingFragment.miles);
	}

	/*
	 * public WifiListHistoryAdapterNew(HistoryActivity historyActivity, int
	 * rowWifilist) { // TODO Auto-generated constructor stub }
	 */

	@Override
	public View getView(final int position, View view, ViewGroup parent) {

		ViewHolder holder = null;
		if (view == null) {
			view = (View) inflator.inflate(R.layout.item_location, null);
			holder = new ViewHolder(view);
			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}
		if (flag == 1) {
			holder.delete_Row.setVisibility(View.GONE);
			holder.fav_Row.setVisibility(View.VISIBLE);
		} else {
			holder.delete_Row.setVisibility(View.VISIBLE);
			holder.fav_Row.setVisibility(View.GONE);
		}
		final Place item = list.get(position);
		Log.d("history",
				"his adapter => " + item.isBussiness + ", " + item.businesstype
						+ ", " + item.mIsOpenNow + ", "
						+ item.getDBisBussiness());
		if (flag == 1) {

			if (item.getIsFavorites().equals("true")) {
				Picasso.with(_context).load(R.drawable.star_yellow)
						.into(holder.fav_Row);
			} else {
				holder.fav_Row.setImageResource(R.drawable.star_gray);
			}
		}
		// holder.favBut.setVisibility(View.GONE);
		holder.front.setTag(R.id.front, item);
		holder.tv_title.setText(item.getName());
		holder.tv_title
				.setTypeface(AppDelegate.proxima_bold);
		holder.tv_subTitle.setText(item.getAddress());
		holder.tv_subTitle
				.setTypeface(AppDelegate.proxima_regular);
		holder.tv_dis.setText(UtilsClass.getDisWithType(disType,
				item.getDistance()));
		holder.tv_dis
				.setTypeface(AppDelegate.proxima_semibold);
		holder.delete_Row.setTag(position);
		holder.fav_Row.setTag(position);
		holder.fav_Row.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				// chkorNot=true;
				final int getPosition = (Integer) arg0.getTag();
				Place itemRow = list.get(getPosition);
				DBHelper dbHelper = new DBHelper(_context);
				SQLiteDatabase database = dbHelper.getWritableDatabase();

				/*
				 * dbHelper.setHistoryrow(database,
				 * list.get(getPosition).isFavorites, list
				 * .get(getPosition).getPlaceId());
				 */

				if (itemRow.isFavorites.equalsIgnoreCase("false")) {
					itemRow.isFavorites = "true";
					dbHelper.setHistoryrow(database, itemRow.isFavorites, list
							.get(getPosition).getPlaceId());
					boolean returnVal = dbHelper.insertIntoDatabasefav(
							database, list.get(getPosition));

					notifyDataSetChanged();
				} else {
					itemRow.isFavorites = "false";
					dbHelper.setHistoryrow(database, itemRow.isFavorites, list
							.get(getPosition).getPlaceId());
					dbHelper.deleteRecordSingle(itemRow);

					notifyDataSetChanged();
				}
			}
		});
		// holder.check.setChecked(list.get(position).isValuetest());
		holder.delete_Row.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				final int getPosition = (Integer) v.getTag();
				System.out.println("indeletepos====" + getPosition);
				AlertDialog.Builder builder = new AlertDialog.Builder(_context);
				builder.setTitle("Alert");
				builder.setCancelable(false);
				builder.setMessage("Are you sure you want to delete this listing from your favorites?");
				builder.setNegativeButton("Cancel", null);
				builder.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface arg0, int arg1) {
								DBHelper dbHelper = new DBHelper(_context);
								SQLiteDatabase database = dbHelper
										.getWritableDatabase();
								dbHelper.deleteRecordSingle(list
										.get(getPosition));
								list.remove(getPosition);
								System.out.println("indeletepos===="
										+ getPosition);
								// list.get(getPosition).setIsFavorites("false");
								dbHelper.setHistoryrow(database, "false",
										item.getPlaceId());
								notifyDataSetChanged();

							}
						});

				AlertDialog dialog = builder.create();
				dialog.show();

				System.out.println("clickpos===" + getPosition);

			}
		});

		holder.front.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Place place = (Place) v.getTag(R.id.front);
				if (place.getIsFavorites().equals("true")) {
					if (isValidString(place.mIsOpenNow)) {
						place.isBussiness = true;
						place.businesstype = place.mIsOpenNow;
					} else {
						place.isBussiness = false;
					}
				} else {
					if (place.getDBisBussiness().equalsIgnoreCase("1")) {
						place.isBussiness = true;
						place.businesstype = place.mIsOpenNow;
					} else {
						place.isBussiness = false;
					}
				}
//				CommonRedirect.goToDetail((BaseFragmentActivity) _context,
//						place);
			}
		});

		if (!list.get(position).getIcon().isEmpty()) {
			Picasso.with(_context).load(item.getIcon()).into(holder.imgV);
		} else {

			Picasso.with(_context)
					.load("https://cdn2.iconfinder.com/data/icons/windows-8-metro-style/48/wifi.png")
					.into(holder.imgV);
		}
		// holder.check.setChecked(list.get(position).isValuetest());
		return view;
	}

	public boolean isValidString(String string) {
		if (string != null && !string.equalsIgnoreCase("null")
				&& !string.equalsIgnoreCase("")) {
			return true;
		} else {
			return false;
		}
	}

	public void setList(ArrayList<Place> data) {
		this.list = data;
	}

	public ArrayList<Place> getList() {
		return this.list;
	}

	private static class ViewHolder {
		public TextView tv_category;
		public TextView tv_dis;
		public TextView tv_subTitle;
		// public ImageView imgV_arrow;
		public ImageView imgV;
		private TextView tv_title = null;
		// public ViewGroup deleteHolder;
		public CheckBox check = null;
		public ImageView delete_Row;
		public ImageView fav_Row;
		private View front;

		// public Button favBut;

		ViewHolder(View view) {
			front = view.findViewById(R.id.front);
			imgV = (ImageView) view.findViewById(R.id.imgV);
			// imgV_arrow = (ImageView) view.findViewById(R.id.imgV_arrow);
			tv_title = (TextView) view.findViewById(R.id.tv_title);
			tv_category = (TextView) view.findViewById(R.id.tv_category);
			tv_subTitle = (TextView) view.findViewById(R.id.tv_subTitle);
			tv_dis = (TextView) view.findViewById(R.id.tv_dis);
			// deleteHolder = (ViewGroup) view.findViewById(R.id.holder);
			// check = (CheckBox) view.findViewById(R.id.chk);
			// favBut = (Button) view.findViewById(R.id.favadd_buton);
			tv_category.setVisibility(View.GONE);
			delete_Row = (ImageView) view.findViewById(R.id.delete_fv);
			fav_Row = (ImageView) view.findViewById(R.id.fav_fv);

		}
	}

	/*
	 * static class ViewHolder { TextView txtTitle; ImageView imgVItem;
	 * //ImageView btn_send; ImageView imvDelete; ImageView txtDateTime;
	 * ImageView share; TextView distance; TextView short_dess; }
	 */

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return list.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	@Override
	public Filter getFilter() {
		// TODO Auto-generated method stub
		return new Filter() {

			@Override
			protected void publishResults(CharSequence constraint,
					FilterResults results) {
				// TODO Auto-generated method stub
				list = (ArrayList<Place>) results.values;
				notifyDataSetChanged();
			}

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				// TODO Auto-generated method stub

				final FilterResults oReturn = new FilterResults();
				final ArrayList<Place> results = new ArrayList<Place>();
				if (orig == null)
					orig = list;
				if (constraint != null) {
					if (orig != null && orig.size() > 0) {
						for (final Place g : orig) {
							if (g.getName().toLowerCase()
									.contains(constraint.toString()))
								results.add(g);
						}
					}
					oReturn.values = results;
				}
				return oReturn;
			}
		};
	}

	/*
	 * public void notifyDataSetChanged() { super.notifyDataSetChanged(); }
	 */
}
/*
 * package com.nearzilla.adapters;
 * 
 * import java.util.ArrayList;
 * 
 * import org.gmarz.googleplaces.models.Place;
 * 
 * import android.app.AlertDialog; import android.content.Context; import
 * android.content.DialogInterface; import
 * android.database.sqlite.SQLiteDatabase; import android.graphics.Typeface;
 * import android.view.LayoutInflater; import android.view.View; import
 * android.view.View.OnClickListener; import android.view.ViewGroup; import
 * android.widget.BaseAdapter; import android.widget.CheckBox; import
 * android.widget.Filter; import android.widget.Filterable; import
 * android.widget.ImageView; import android.widget.TextView;
 * 
 * import com.nearzilla.HistoryActivity; import com.nearzilla.R; import
 * com.nearzilla.db.DBHelper; import com.squareup.picasso.Picasso;
 * 
 * //This is simple adapter to show data in listview with multiple values.
 * 
 * public class WifiListHistoryAdapterNew extends BaseAdapter implements
 * Filterable {
 * 
 * private ArrayList<Place> list; private Context _context; private boolean
 * isHiostory; private boolean isLocation = true; public ArrayList<Place> orig;
 * private LayoutInflater inflator; // private ViewHolder holder; private int
 * flag; boolean[] checkBoxState; static boolean chkorNot = false;
 * 
 * public WifiListHistoryAdapterNew(Context context, int layoutID,
 * ArrayList<Place> data, boolean isHistory, int flag) { _context = context;
 * this.list = data; this.isHiostory = isHistory; inflator =
 * LayoutInflater.from(context); this.flag = flag; checkBoxState = new
 * boolean[list.size()]; }
 * 
 * public WifiListHistoryAdapterNew(HistoryActivity historyActivity, int
 * rowWifilist) { // TODO Auto-generated constructor stub }
 * 
 * @Override public View getView(final int position, View view, ViewGroup
 * parent) {
 * 
 * ViewHolder holder = null; if (view == null) { view = (View)
 * inflator.inflate(R.layout.item_location, null); holder = new
 * ViewHolder(view); view.setTag(holder); } else { holder = (ViewHolder)
 * view.getTag(); } if (flag == 1) { holder.delete_Row.setVisibility(View.GONE);
 * holder.fav_Row.setVisibility(View.VISIBLE); } else {
 * holder.delete_Row.setVisibility(View.VISIBLE);
 * holder.fav_Row.setVisibility(View.GONE); } final Place item =
 * list.get(position); if (flag == 1) { if
 * (item.getIsFavorites().equals("true")) {
 * Picasso.with(_context).load(R.drawable.star_yellow) .into(holder.fav_Row); }
 * else { holder.fav_Row.setImageResource(R.drawable.star_gray); } } //
 * holder.favBut.setVisibility(View.GONE);
 * 
 * holder.tv_title.setText(item.getName());
 * holder.tv_title.setTypeface(Typeface.createFromAsset( _context.getAssets(),
 * "proxima-nova-bold.ttf")); holder.tv_subTitle.setText(item.getAddress());
 * holder.tv_subTitle.setTypeface(Typeface.createFromAsset(
 * _context.getAssets(), "proxima-nova-regular.ttf"));
 * holder.tv_dis.setText(item.getDistance() + " km");
 * holder.tv_dis.setTypeface(Typeface.createFromAsset( _context.getAssets(),
 * "proxima-nova-semibold.ttf")); holder.delete_Row.setTag(position);
 * holder.fav_Row.setTag(position); holder.fav_Row.setOnClickListener(new
 * OnClickListener() {
 * 
 * @Override public void onClick(View arg0) { // TODO Auto-generated method stub
 * 
 * // chkorNot=true; final int getPosition = (Integer) arg0.getTag();
 * list.get(getPosition).setIsFavorites("true"); DBHelper dbHelper = new
 * DBHelper(_context); SQLiteDatabase database = dbHelper.getWritableDatabase();
 * boolean returnVal = dbHelper.insertIntoDatabasefav(database,
 * list.get(getPosition));
 * 
 * dbHelper.setHistoryrow(database, list.get(getPosition).isFavorites, list
 * .get(getPosition).getPlaceId());
 * 
 * 
 * if (!returnVal) { dbHelper.setHistoryrow(database,
 * list.get(getPosition).isFavorites, list.get(getPosition).getPlaceId());
 * 
 * notifyDataSetChanged(); } else { dbHelper.setHistoryrow(database, "false",
 * list.get(getPosition).getPlaceId()); dbHelper.deleteRecordSingle(list
 * .get(getPosition)); notifyDataSetChanged(); } } }); //
 * holder.check.setChecked(list.get(position).isValuetest());
 * holder.delete_Row.setOnClickListener(new OnClickListener() {
 * 
 * @Override public void onClick(View v) { // TODO Auto-generated method stub
 * final int getPosition = (Integer) v.getTag();
 * System.out.println("indeletepos====" + getPosition); AlertDialog.Builder
 * builder = new AlertDialog.Builder(_context); builder.setTitle("Alert");
 * builder.setCancelable(false); builder.setMessage(
 * "Are you sure you want to delete this listing from your favorites?");
 * builder.setNegativeButton("Cancel", null); builder.setPositiveButton("Yes",
 * new DialogInterface.OnClickListener() {
 * 
 * @Override public void onClick(DialogInterface arg0, int arg1) { DBHelper
 * dbHelper = new DBHelper(_context); SQLiteDatabase database = dbHelper
 * .getWritableDatabase(); dbHelper.deleteRecordSingle(list .get(getPosition));
 * list.remove(getPosition); System.out.println("indeletepos====" +
 * getPosition); // list.get(getPosition).setIsFavorites("false");
 * dbHelper.setHistoryrow(database, "false", item.getPlaceId());
 * notifyDataSetChanged();
 * 
 * } });
 * 
 * AlertDialog dialog = builder.create(); dialog.show();
 * 
 * System.out.println("clickpos===" + getPosition);
 * 
 * } });
 * 
 * if (!list.get(position).getIcon().isEmpty()) {
 * Picasso.with(_context).load(item.getIcon()).into(holder.imgV); } else {
 * 
 * Picasso.with(_context)
 * .load("https://cdn2.iconfinder.com/data/icons/windows-8-metro-style/48/wifi.png"
 * ) .into(holder.imgV); } //
 * holder.check.setChecked(list.get(position).isValuetest()); return view; }
 * 
 * public void setList(ArrayList<Place> data) { this.list = data; }
 * 
 * public ArrayList<Place> getList() { return this.list; }
 * 
 * private static class ViewHolder { public TextView tv_category; public
 * TextView tv_dis; public TextView tv_subTitle; // public ImageView imgV_arrow;
 * public ImageView imgV; private TextView tv_title = null; // public ViewGroup
 * deleteHolder; public CheckBox check = null; public ImageView delete_Row;
 * public ImageView fav_Row;
 * 
 * // public Button favBut;
 * 
 * ViewHolder(View view) { imgV = (ImageView) view.findViewById(R.id.imgV); //
 * imgV_arrow = (ImageView) view.findViewById(R.id.imgV_arrow); tv_title =
 * (TextView) view.findViewById(R.id.tv_title); tv_category = (TextView)
 * view.findViewById(R.id.tv_category); tv_subTitle = (TextView)
 * view.findViewById(R.id.tv_subTitle); tv_dis = (TextView)
 * view.findViewById(R.id.tv_dis); // deleteHolder = (ViewGroup)
 * view.findViewById(R.id.holder); // check = (CheckBox)
 * view.findViewById(R.id.chk); // favBut = (Button)
 * view.findViewById(R.id.favadd_buton); tv_category.setVisibility(View.GONE);
 * delete_Row = (ImageView) view.findViewById(R.id.delete_fv); fav_Row =
 * (ImageView) view.findViewById(R.id.fav_fv);
 * 
 * } }
 * 
 * 
 * static class ViewHolder { TextView txtTitle; ImageView imgVItem; //ImageView
 * btn_send; ImageView imvDelete; ImageView txtDateTime; ImageView share;
 * TextView distance; TextView short_dess; }
 * 
 * 
 * @Override public int getCount() { // TODO Auto-generated method stub return
 * list.size(); }
 * 
 * @Override public Object getItem(int arg0) { // TODO Auto-generated method
 * stub return list.get(arg0); }
 * 
 * @Override public long getItemId(int arg0) { return 0; }
 * 
 * @Override public Filter getFilter() { // TODO Auto-generated method stub
 * return new Filter() {
 * 
 * @Override protected void publishResults(CharSequence constraint,
 * FilterResults results) { // TODO Auto-generated method stub list =
 * (ArrayList<Place>) results.values; notifyDataSetChanged(); }
 * 
 * @Override protected FilterResults performFiltering(CharSequence constraint) {
 * // TODO Auto-generated method stub
 * 
 * final FilterResults oReturn = new FilterResults(); final ArrayList<Place>
 * results = new ArrayList<Place>(); if (orig == null) orig = list; if
 * (constraint != null) { if (orig != null && orig.size() > 0) { for (final
 * Place g : orig) { if (g.getName().toLowerCase()
 * .contains(constraint.toString())) results.add(g); } } oReturn.values =
 * results; } return oReturn; } }; }
 * 
 * 
 * public void notifyDataSetChanged() { super.notifyDataSetChanged(); }
 * 
 * }
 */