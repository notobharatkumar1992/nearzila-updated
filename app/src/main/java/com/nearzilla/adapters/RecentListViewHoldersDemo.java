package com.nearzilla.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.nearzilla.R;

import carbon.widget.RelativeLayout;
import carbon.widget.TextView;

public class RecentListViewHoldersDemo extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView tv_c_title = null, tv_c_url, tv_c_description;
    public ImageView imgV;
    public carbon.widget.ImageView img_c_dot;
    public RelativeLayout rl_c_front;

    public RecentListViewHoldersDemo(View view) {
        super(view);
//        view.setOnClickListener(this);

        imgV = (ImageView) view.findViewById(R.id.imgV);
        img_c_dot = (carbon.widget.ImageView) view.findViewById(R.id.img_c_dot);
        tv_c_title = (TextView) view.findViewById(R.id.tv_c_title);
        tv_c_url = (TextView) view.findViewById(R.id.tv_c_url);
        tv_c_description = (TextView) view.findViewById(R.id.tv_c_description);

        rl_c_front = (RelativeLayout) view.findViewById(R.id.rl_c_front);
    }

    @Override
    public void onClick(View view) {
        Toast.makeText(view.getContext(), "Clicked Position = " + getPosition(), Toast.LENGTH_SHORT).show();
    }
}
