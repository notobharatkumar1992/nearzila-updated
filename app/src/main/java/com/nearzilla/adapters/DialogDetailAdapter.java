package com.nearzilla.adapters;

import java.util.ArrayList;

import org.gmarz.googleplaces.models.Place;
import org.json.JSONArray;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baseClasses.Constants;
import com.nearzilla.HistoryActivity;
import com.nearzilla.R;
import com.squareup.picasso.Picasso;

//This is simple adapter to show data in listview with multiple values.

public class DialogDetailAdapter extends BaseAdapter implements
Filterable {

	private JSONArray list;
	private Context _context;
	private boolean isHiostory;
	private boolean isLocation = true;
	public ArrayList<Place> orig;
	private LayoutInflater inflator;
	// private ViewHolder holder;
	private int flag;
	boolean[] checkBoxState;

	public DialogDetailAdapter(Context context, int layoutID,
			JSONArray data) {
		_context = context;
		this.list = data;
		inflator = LayoutInflater.from(context);
	
	}

	public DialogDetailAdapter(HistoryActivity historyActivity,
			int rowWifilist) {
		// TODO Auto-generated constructor stub
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.length();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(int arg0, View convertView, ViewGroup arg2) {
		// TODO Auto-generated method stub
		convertView = (View) inflator.inflate(R.layout.item_dialog, null);
		TextView tvname = (TextView) convertView.findViewById(R.id.day_dailog);
		TextView image = (TextView) convertView.findViewById(R.id.timing_dialog);
		tvname.setTypeface(Typeface.createFromAsset(_context.getAssets(),"proxima-nova-regular.ttf"));
		image.setTypeface(Typeface.createFromAsset(_context.getAssets(),"proxima-nova-bold.ttf"));
		
		String[] timeSpl = list.optString(arg0).split("day:");
		System.out.println("arrayofday====="+timeSpl);
		if (timeSpl.length > 1) {
			tvname.setText(timeSpl[0].trim()+"day");
			image.setText(timeSpl[1].trim());
		}
	//	tvname.setText(list.optString(arg0));
		// convertView.setTag(names.get(position).getId());
		return convertView;
	}
	static class Holder {
		public RelativeLayout root;
		ImageView imgV = null;
		TextView name = null;
		RelativeLayout rel_append = null;
	}
	@Override
	public Filter getFilter() {
		// TODO Auto-generated method stub
		return null;
	}

	
}