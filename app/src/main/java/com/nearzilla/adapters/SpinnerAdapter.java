package com.nearzilla.adapters;

import java.text.BreakIterator;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.baseClasses.Constants;
import com.nearzilla.R;
import com.squareup.picasso.Picasso;

public class SpinnerAdapter extends BaseAdapter {
	private JSONArray names = null;
	private LayoutInflater inflator;
	private Holder holder;
	Context context;
	// String apend = "false";
	View convertView;
	public int send = 0;
	public boolean isExpend = false;

	public SpinnerAdapter(Context context, JSONArray names) {
		this.names = names;
		inflator = LayoutInflater.from(context);
		this.context = context;
	}

	@Override
	public int getCount() {
		send = 0;
		try {
			/*if (names.length() < 7) {
				send = names.length();
			} else if ((names.length() > 6) && !isExpend) {
				send = 7;
			} else {*/
				send = names.length();
		//	}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return send;
	}

	@Override
	public Object getItem(int position) {

		try {
			return names.getJSONObject(position);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public long getItemId(int position) {

		return position;
	}

	@Override
	public View getView(int position, View convertView1, ViewGroup parent) {

		this.convertView = convertView1;
		convertView = (View) inflator.inflate(R.layout.item_spinner, null);
		TextView tvname = (TextView) convertView.findViewById(R.id.name);
		tvname.setTypeface(Typeface.createFromAsset(context.getAssets(),"proxima-nova-bold.ttf"));
		ImageView image = (ImageView) convertView.findViewById(R.id.image);
		/*if (send == 7 && position == 6) {
			convertView.findViewById(R.id.vie_botom).setVisibility(View.VISIBLE);
			convertView.findViewById(R.id.vie_botom).setOnClickListener(
					new OnClickListener() {

						@Override
						public void onClick(View arg0) {
							isExpend = true;
							notifyDataSetChanged();
						}
					});
		} else {
			convertView.findViewById(R.id.vie_botom).setVisibility(View.GONE);

		}*/
		convertView.findViewById(R.id.vie_botom).setVisibility(View.GONE);
		/*if (position == 0) {
			tvname.setText("Select a categories ");

		} else {*/
			tvname.setText(names.optJSONObject(position).optString(Constants.category));
	//	}
		/*if (position == 0) {

		} else {*/
			Picasso.with(context)
					.load(names.optJSONObject(position).optString("image"))
					.placeholder(R.drawable.default_image).into(image);
	//	}
		// convertView.setTag(names.get(position).getId());
		return convertView;
	}

	static class Holder {
		public RelativeLayout root;
		ImageView imgV = null;
		TextView name = null;
		RelativeLayout rel_append = null;
	}
}
