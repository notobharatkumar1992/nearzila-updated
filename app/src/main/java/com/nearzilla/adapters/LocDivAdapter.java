package com.nearzilla.adapters;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.baseClasses.Constants;
import com.fortysevendeg.swipelistview.SwipeListView;
import com.nearzilla.AppDelegate;
import com.nearzilla.R;
import com.nearzilla.db.DBHelper;
import com.nearzilla.fragments.SettingFragment;
import com.squareup.picasso.Picasso;
import com.utilities.UtilsClass;

import org.gmarz.googleplaces.models.Place;
import org.gmarz.googleplaces.models.PlaceDetails;
import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class LocDivAdapter extends BaseAdapter {
    private List<org.gmarz.googleplaces.models.Place> list;
    private Activity context;
    private LayoutInflater inflator;
    private ViewHolder holder;
    PlaceDetails details;
    List<Place> subList;
    List<Place> secList;
    SwipeListView listView;
    int i;
    private String disType;

    public LocDivAdapter(Activity fragment,
                         List<org.gmarz.googleplaces.models.Place> list2) {
        this.context = fragment;
        this.list = list2;
        inflator = LayoutInflater.from(fragment);
        SharedPreferences sharedpreferences = fragment.getSharedPreferences(
                Constants.MyPREFERENCES, 0);
        disType = sharedpreferences.getString(Constants.disType,
                SettingFragment.miles);
    }

    public void addPlaces(List<org.gmarz.googleplaces.models.Place> list2,
                          int i, SwipeListView listView) {
        System.out.println("indexadapter" + i);
        this.i = i;
        this.listView = listView;
        this.list.addAll(list2);
        ArrayList<Place> temp = (ArrayList<Place>) ((ArrayList<Place>) this.list)
                .clone();
        try {
            subList = temp.subList(0, i);
            secList = temp.subList(i, list.size());

            System.out.println("Before sort==" + secList);

            this.list.clear();
            this.list.addAll(subList);
            Collections.sort(secList, new Comparator<Place>() {
                @Override
                public int compare(Place lhs, Place rhs) {
                    // TODO Auto-generated method stub
                    if (lhs.getDistance() < rhs.getDistance())
                        return -1;
                    if (lhs.getDistance() > rhs.getDistance())
                        return 1;
                    return 0;
                }
            });
            this.list.addAll(secList);
            System.out.println("Aftersort==" + secList);
            System.out.println("hole list==" + list.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (i > 0)
            listView.setTillToSwipe(i);
        listView.getSelectedItemPosition();
        notifyDataSetChanged();

        int selectionPos = getCount() - list2.size();
        listView.setSelection(selectionPos - 2);
    }

    public List<org.gmarz.googleplaces.models.Place> getList() {
        return list;
    }

    @Override
    public int getCount() {
        return this.list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        if (view == null) {
            view = inflator.inflate(R.layout.custom_row, null);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        final Place item = list.get(position);
        holder.front.setTag(R.id.title, item);
        holder.call.setTag(item.getPhoneNo());
        holder.share.setTag(position);
        view.setTag(R.id.title, list.get(position));
        holder.fav.setTag(position);

        holder.tv_title.setTypeface(AppDelegate.proxima_bold);
        holder.tv_subTitle.setTypeface(AppDelegate.proxima_regular);
        holder.tv_dis.setTypeface(AppDelegate.proxima_semibold);

        holder.tv_title.setText(item.getName());
        holder.tv_subTitle.setText(item.getAddress());
        item.getLongitude();
        // if (item.getDistance()==0.0) {
        // holder.tv_dis.setVisibility(View.GONE);
        // } else {
        holder.tv_dis.setText(UtilsClass.getDisWithType(disType, item.getDistance()));
        // }
        if (!list.get(position).getIcon().isEmpty()) {
            // if (!item.isLocation) {
            Picasso.with(context).load(item.getIcon()).into(holder.imgV);
        } else {
            // Picasso.with(context.getActivity()).load(item.getIcon()).placeholder(R.drawable.wifi_default).into(holder.imgV);
            Picasso.with(context)
                    .load("https://cdn2.iconfinder.com/data/icons/windows-8-metro-style/48/wifi.png")
                    .into(holder.imgV);
        }

        if (item.isBussiness) {
            holder.imgBus.setVisibility(View.VISIBLE);
            holder.imgBus_google.setVisibility(View.GONE);
//            holder.imgBus.setImageDrawable(context.getResources().getDrawable(R.drawable.bussiness));
            holder.lin_price.setVisibility(View.VISIBLE);
            setPriceAdapter(item.getOffersList());
        } else {
            holder.imgBus.setVisibility(View.GONE);
            holder.imgBus_google.setVisibility(View.VISIBLE);
//            holder.imgBus.setImageDrawable(context.getResources().getDrawable(R.drawable.google));
            holder.lin_price.setVisibility(View.GONE);
        }

        holder.front.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
                Log.d("test", "holder.front clicked => " + item.getUrl());
                try {
                    AppDelegate.openURL(context, item.getUrl());
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }

//                Place place = (Place) view.getTag(R.id.title);
//                CommonRedirect.goToDetail((BaseFragmentActivity) context, place);
//                ((LocDivFragment)((LocationsActivity)context).getFragmentByTag(LocDivFragment.TAG)).goToDetail(place);
            }
        });

        holder.call.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d("test", "holder.call clicked");

                String phoneNo = (String) v.getTag();
                System.out.println("clickbuttonindex" + phoneNo);

                if (!phoneNo.isEmpty()) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + phoneNo));
                    context.startActivity(callIntent);
                }
            }
        });

        holder.share.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d("test", "holder.share clicked");

                int pos = (int) v.getTag();
                System.out.println("clickbuttonindex" + pos);
                Place place = list.get(pos);
                System.out.println("url======" + place.getUrl());
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, place.getName() + "\n"
                        + place.getAddress());
                sendIntent.setType("text/plain");
                context.startActivity(sendIntent);
            }
        });

        holder.fav.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = (int) v.getTag();
                list.get(pos).setIsFavorites("true");
                System.out.println("clickbuttonindex" + pos);
                Place place = list.get(pos);
                DBHelper dbHelper = new DBHelper(context);
                SQLiteDatabase database = dbHelper.getWritableDatabase();
                boolean returnVal = dbHelper.insertIntoDatabasefav(database,
                        place);
                dbHelper.setHistoryrow(database, list.get(pos).isFavorites,
                        list.get(pos).getPlaceId());
            }
        });
        return view;
    }

    public void setPriceAdapter(JSONArray jArr) {
        if (jArr != null && jArr.length() > 0) {
            PriceSpinnerAdapter adapter = new PriceSpinnerAdapter(context, jArr);
            holder.lin_price.setAdapter(adapter);
            holder.lin_price
                    .setOnItemSelectedListener(new OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> arg0,
                                                   View arg1, int arg2, long arg3) {
                            // TODO Auto-generated method stub

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> arg0) {
                            // TODO Auto-generated method stub

                        }
                    });
        } else {
            holder.lin_price.setVisibility(View.GONE);
        }
    }

    private static class ViewHolder {
        public TextView tv_category;
        public TextView tv_dis;
        public TextView tv_subTitle;
        // public ImageView imgV_arrow;
        public ImageView imgV;
        private TextView tv_title = null;
        private Button fav, share, call;
        private RelativeLayout front;
        private ImageView imgBus, imgBus_google;
        private Spinner lin_price;

        // private TextView tv_price;

        // public ViewGroup deleteHolder;
        // public Button addFavButton;

        ViewHolder(View view) {
            front = (RelativeLayout) view.findViewById(R.id.front);
            imgV = (ImageView) view.findViewById(R.id.imgV);
            // imgV_arrow = (ImageView) view.findViewById(R.id.imgV_arrow);
            tv_title = (TextView) view.findViewById(R.id.tv_title);
            tv_category = (TextView) view.findViewById(R.id.tv_category);
            tv_subTitle = (TextView) view.findViewById(R.id.tv_subTitle);
            tv_dis = (TextView) view.findViewById(R.id.tv_dis);
            fav = (Button) view.findViewById(R.id.swipe_button1);
            share = (Button) view.findViewById(R.id.swipe_button2);
            call = (Button) view.findViewById(R.id.swipe_button3);
            imgBus = (ImageView) view.findViewById(R.id.imgBus);
            imgBus_google = (ImageView) view.findViewById(R.id.imgBus_google);
            lin_price = (Spinner) view.findViewById(R.id.lin_price);
            // tv_price = (TextView) view.findViewById(R.id.expand_text);
            // deleteHolder = (ViewGroup) view.findViewById(R.id.holder);
            // addFavButton = (Button) view.findViewById(R.id.favadd_buton);
            tv_category.setVisibility(View.VISIBLE);
            imgBus.setVisibility(View.GONE);
            imgBus_google.setVisibility(View.GONE);
        }
    }
}
