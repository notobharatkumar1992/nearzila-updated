package com.nearzilla.adapters;

import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nearzilla.R;
import com.nearzilla.beans.ModelDrawer;

public class DrawerAdapter extends BaseAdapter {
	
	private Context _context;
	private List<ModelDrawer> listmodel;
	
	private Holder holder;
	
	
	public DrawerAdapter() {
		// TODO Auto-generated constructor stub
	}
	
	public DrawerAdapter(Context context, List<ModelDrawer>list) {
		// TODO Auto-generated constructor stub
		this._context=context;
		this.listmodel=list;
	}
	

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return listmodel.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		// TODO Auto-generated method stub
		holder=new Holder();
		//ModelDrawer drawer= (ModelDrawer)get
		
		//ModelDrawer drawer =(ModelDrawer)getView(position, view, parent);
		if (view==null) {
			
			LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = infalInflater.inflate(R.layout.item_drawer_list, null);
			
		}
		
		holder.icon=(ImageView) view.findViewById(R.id.icon);
		holder.title=(TextView) view.findViewById(R.id.title);
		holder.iconSpecific=(ImageView) view.findViewById(R.id.icon_specific);
//		holder.drawer_list_route=(RelativeLayout) view.findViewById(R.id.drawer_list_route);
		view.setTag(holder);
		holder=(Holder)view.getTag();
		
		holder.iconSpecific.setImageResource(listmodel.get(position).getIconSelect());
		holder.title.setText(listmodel.get(position).getTitle());
		if(position<=3){
			holder.title.setTextSize(21);
			holder.title.setTypeface(null, Typeface.BOLD);
			holder.title.setTextColor(_context.getResources().getColor(android.R.color.white));
		}else{
			holder.title.setTextSize(18);
			holder.title.setTypeface(null, Typeface.NORMAL);
			holder.title.setTextColor(_context.getResources().getColor(android.R.color.background_light));
		}
		if(position==3){
			view.findViewById(R.id.vie_botom).setVisibility(View.VISIBLE);;
		}
		else {
			view.findViewById(R.id.vie_botom).setVisibility(View.GONE);;
		}
		
		
		/* if (drawer.isChecked()) {
			 holder.icon.setImageResource(drawer.getIconSelect());
	        	 holder.drawer_list_route.setBackgroundColor(_context.getResources().getColor(R.color.white));
	        	 holder.title.setTextColor(_context.getResources().getColor(R.color.black));
	        	 
			}else {
				holder.icon.setImageResource(drawer.getIconUnselect());
				holder.drawer_list_route.setBackgroundColor(_context.getResources().getColor(R.color.color_background));
				holder.title.setTextColor(_context.getResources().getColor(R.color.white));
			}
		 holder.drawer_list_route.setTag(drawer);
		 holder.title.setText(drawer.getTitle());*/
		
		
		
		return view;
	}

	
	
	
	public void changeLoginStatus(String status, int groupPosition){
    	ModelDrawer drawer = listmodel.get(groupPosition);
    	drawer.setTitle(status);
    	listmodel.set(groupPosition, drawer);
    	notifyDataSetChanged();
    }
 
    @Override
    public boolean hasStableIds() {
        return false;
    }
 
    
    
    public void reloadDrawerList(List<ModelDrawer> list){
    	listmodel = list;
    	 
    	notifyDataSetChanged();
    }
    
    static class HolderChild {
		CheckedTextView child_text = null;
		
	}
	
	
	
	
	
	
	static class Holder{
		private ImageView iconSpecific=null;
		private ImageView icon=null;
		private TextView title=null;
		private RelativeLayout drawer_list_route=null;
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
