package com.nearzilla.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nearzilla.AppDelegate;
import com.nearzilla.R;

import org.json.JSONArray;
import org.json.JSONException;

public class PriceSpinnerAdapter extends BaseAdapter {
	private JSONArray names = null;
	private LayoutInflater inflator;
	Context context;

	public PriceSpinnerAdapter(Context context, JSONArray names) {
		this.names = names;
		inflator = LayoutInflater.from(context);
		this.context = context;
	}

	@Override
	public int getCount() {
		return names.length();
	}

	@Override
	public Object getItem(int position) {

		try {
			return names.getJSONObject(position);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public long getItemId(int position) {

		return position;
	}

	@Override
	public View getView(int position, View convertView1, ViewGroup parent) {
		//this.convertView = convertView1;
		convertView1 = (View) inflator.inflate(R.layout.item_price_spinner, null);
		TextView tvname = (TextView) convertView1.findViewById(R.id.name);
		tvname.setTypeface(AppDelegate.proxima_semibold);

		String price = names.optJSONObject(position).optJSONObject("business_offers").optString("text");
		tvname.setText(price);
		return convertView1;
	}

	static class Holder {
		public RelativeLayout root;
		ImageView imgV = null;
		TextView name = null;
		RelativeLayout rel_append = null;
	}
}
