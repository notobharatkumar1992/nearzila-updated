package com.nearzilla.adapters;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.nearzilla.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;



public class AdapterReqSubCategory extends BaseAdapter implements Filterable {

	private LayoutInflater infalter;
	private Context activity;
	private Holder holder;
	private ArrayList<Address> arrlist;
	protected ArrayList<Address> data = new ArrayList<Address>();
	private String countryCode;
	double latitude, longitude;


	public AdapterReqSubCategory() {
		// TODO Auto-generated constructor stub
	}

	public AdapterReqSubCategory(Context activity,
			ArrayList<Address> arrlist,double lat,double lon) {

		this.activity = activity;
		this.arrlist = arrlist;
		this.latitude=lat;
		this.longitude=lon;
		infalter = infalter.from(activity);
		countryCode=getCountryName(activity, latitude, longitude);
	}

	public void setList(ArrayList<Address> arrSearched) {
		this.arrlist = arrSearched;
	}

	public ArrayList<Address> getList() {
		return this.arrlist;
	}



	@Override
	public int getCount() {
		if(this.data != null)
			return this.data.size();
		else
			return 0;
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		// TODO Auto-generated method stub


		if (view == null) {
			holder = new Holder();
			view = (View) infalter.inflate(R.layout.item_drawer_list, null);

			//holder.img_icon = (ImageView) view.findViewById(R.id.img_icon);
			view.findViewById(R.id.icon).setVisibility(View.GONE);
			holder.text_data = (TextView) view.findViewById(R.id.title);

			view.setTag(holder);

		} else {
			holder = (Holder) view.getTag();
		}

		//holder.img_icon.setTag(position);
		//holder.text_data.setTag(position);
		view.setTag(R.id.title,data.get(position));
		holder.text_data.setText(data.get(position).getAddressLine(0)+ " "+data.get(position).getAddressLine(1));
		//Picasso.with(activity).load(arrlist.get(position).getImage()).placeholder(activity.getResources().getDrawable(R.drawable.ic_launcher)).into(holder.img_icon);

		return view;
	}

	static class Holder {

		//ImageView img_icon = null;
		TextView text_data = null;

	}

	@Override
	public Filter getFilter() {
		// TODO Auto-generated method stub
		return new Filter() {

			@Override
			protected void publishResults(CharSequence constraint, FilterResults result) {
				// TODO Auto-generated method stub

				data= (ArrayList<Address>)result.values;
				/*if (arrlist==null) {
					arrlist=new ArrayList<ModelTrySearch>();
				}*/
				notifyDataSetChanged();
				arrlist.clear();
			}

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				// TODO Auto-generated method stub
				final FilterResults oReturn = new FilterResults();
				final ArrayList<Address> results = new ArrayList<Address>();

				if (constraint != null) {
					if (arrlist != null && arrlist.size() > 0) {
						for (final Address g : arrlist) {
							String code = g.getCountryCode();
							if(code != null){
								Log.e("code", code+"");
								if(code.equalsIgnoreCase(countryCode)){									
									results.add(g);
									Log.i("code", code+"");
								}
							}
						}

					}


					if(results.size() == 0){
						String location = constraint.toString();
						if(location.length() > 2){
							Geocoder gc = new Geocoder(activity);
							List<Address> list;
							try {
								list = gc.getFromLocationName(location, 10);

								Address add = list.get(0);
								String locality = add.getAddressLine(0) + " "+add.getAddressLine(1);
								//Toast.makeText(activity, locality, Toast.LENGTH_LONG).show();

								double lat = add.getLatitude();
								double lng = add.getLongitude();

								arrlist.addAll(list);
								results.addAll(list);					
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
					}
					oReturn.values = results;
					//UtilsClass.hideKeyBoard(activity, holder.text_data);
				}
				return oReturn;
			}
		};
	}

	public void notifyDataSetChanged() {
		super.notifyDataSetChanged();
	}

	public static String getCountryName(Context context, double latitude, double longitude) {
		Geocoder geocoder = new Geocoder(context, Locale.getDefault());
		List<Address> addresses = null;
		try {
			addresses = geocoder.getFromLocation(latitude, longitude, 1);
			Address result;

			if (addresses != null && !addresses.isEmpty()) {
				return addresses.get(0).getCountryCode();
			}
			return "";
		} catch (IOException ignored) {
			//do something
		}
		return "";

	}
}
