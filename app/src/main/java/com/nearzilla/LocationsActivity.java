package com.nearzilla;

import android.app.Activity;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.baseClasses.BaseFragmentActivity;
import com.nearzilla.fragments.FragmentMainSetting;
import com.nearzilla.fragments.LocDivFragment;
import com.nearzilla.fragments.MainHistoryFragment;
import com.nearzilla.fragments.NavigationDrawerFragment;
import com.nearzilla.fragments.SearchFragment;
import com.nearzilla.interfaces.OnReciveServerResponse;


public class LocationsActivity extends BaseFragmentActivity implements OnClickListener, OnReciveServerResponse {
    private boolean isButton = false;
    // private SlideView mLastSlideViewWithStatusOn;
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private CharSequence mTitle;

    // ArrayList<DeviceModel> arraylist;

    public LinearLayout layout_bottom;
    public RelativeLayout lin_setting, lin_search, lin_home, lin_history;
    public View view_home, view_search, view_history, view_setting;
    public ImageView img_linref, img_setting, img_linsearch, img_linhome,
            img_linhis, close;

    public static OnReciveServerResponse serverResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container);
        System.out.println("location activity");
        restoreActionBar();
        serverResponse = this;

        initView();
        WifiManager wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        Log.d("wifiInfo", wifiInfo.toString());
        Log.d("SSID", wifiInfo.getSSID());
        replaceFragement(LocDivFragment.getInstance(null), LocDivFragment.TAG);
        // callServiceforPlaces();
        // arraylist = new ArrayList<DeviceModel>();
    }

    private void initView() {
        layout_bottom = (LinearLayout) findViewById(R.id.layout_bottom);

        lin_setting = (RelativeLayout) findViewById(R.id.lin_setting);
        lin_setting.setOnClickListener(this);
        lin_search = (RelativeLayout) findViewById(R.id.lin_search);
        lin_search.setOnClickListener(this);
        lin_home = (RelativeLayout) findViewById(R.id.lin_home);
        lin_home.setOnClickListener(this);
        lin_history = (RelativeLayout) findViewById(R.id.lin_history);
        lin_history.setOnClickListener(this);

        view_home = (View) findViewById(R.id.view_home);
        view_search = (View) findViewById(R.id.view_search);
        view_history = (View) findViewById(R.id.view_history);
        view_setting = (View) findViewById(R.id.view_setting);

        img_linref = (ImageView) findViewById(R.id.img_linref);
        img_setting = (ImageView) findViewById(R.id.img_setting);
        img_linsearch = (ImageView) findViewById(R.id.img_linsearch);
        img_linhome = (ImageView) findViewById(R.id.img_linhome);
        img_linhis = (ImageView) findViewById(R.id.img_linhis);

        updateTabView(0);
    }

    public void updateTabView(int position) {
        img_linhome.setImageResource(R.drawable.sl_tab_home_deselected);
        img_linsearch.setImageResource(R.drawable.sl_tab_search_deselected);
        img_linhis.setImageResource(R.drawable.sl_tab_history_deselected);
        img_setting.setImageResource(R.drawable.sl_tab_more_deselected);

        view_home.setVisibility(View.GONE);
        view_search.setVisibility(View.GONE);
        view_history.setVisibility(View.GONE);
        view_setting.setVisibility(View.GONE);

        switch (position) {
            case 0:
                img_linhome.setImageResource(R.drawable.sl_tab_home_selected);
                view_home.setVisibility(View.VISIBLE);
                break;
            case 1:
                img_linsearch.setImageResource(R.drawable.sl_tab_search_selected);
                view_search.setVisibility(View.VISIBLE);
                break;
            case 2:
                img_linhis.setImageResource(R.drawable.sl_tab_history_selected);
                view_history.setVisibility(View.VISIBLE);
                break;
            case 3:
                img_setting.setImageResource(R.drawable.sl_tab_more_selected);
                view_setting.setVisibility(View.VISIBLE);
                break;

            default:
                break;
        }
    }

    boolean isDelayed = false;

    private void setDelayed() {
        isDelayed = true;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                isDelayed = false;
            }
        }, 1000);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lin_setting:
                restoreActionBar(false);
                updateTabView(3);

                replaceFragement(FragmentMainSetting.getInstance(null),
                        LocDivFragment.TAG);
                break;

            case R.id.lin_home:
                if (!isDelayed) {
                    restoreActionBar(true);
                    updateTabView(0);

                    replaceFragement(LocDivFragment.getInstance(null),
                            LocDivFragment.TAG);
                    setDelayed();
                }
                break;

            case R.id.lin_history:
                // restoreActionBar(false);
                // replaceFragement(FragmentMainSetting.getInstance(null));
                updateTabView(2);

                replaceFragement(MainHistoryFragment.getInstance(null),
                        MainHistoryFragment.TAG);

                // startActivity(new Intent(this, HistoryActivity.class));
                break;

            // case R.id.lin_refresh:
            // showLayout(false);
            // img_linref.setImageResource(R.drawable.refresh_hover);
            // img_linsearch.setImageResource(R.drawable.searchicon);
            // img_linhome.setImageResource(R.drawable.homegray);
            // img_linhis.setImageResource(R.drawable.history);
            // refresh();
            // break;

            case R.id.lin_search:
                restoreActionBar(false);
                replaceFragement(SearchFragment.getInstance(null));
                updateTabView(1);

                // showLayout(true);

                break;
            // case R.id.lin_home:
            // /*
            // * catId=0; showLayout(false);
            // * replaceFragment(LocDivFragment.getInstance(null),
            // * LocDivFragment.TAG);
            // * img_linref.setImageResource(R.drawable.refresh_botom);
            // * img_linsearch.setImageResource(R.drawable.searchicon);
            // * img_linhome.setImageResource(R.drawable.homegray1);
            // * img_linhis.setImageResource(R.drawable.history);
            // */
            // showLayout(false);
            // img_linref.setImageResource(R.drawable.refresh_botom);
            // img_linsearch.setImageResource(R.drawable.searchicon);
            // img_linhome.setImageResource(R.drawable.homegray);
            // img_linhis.setImageResource(R.drawable.history_hover);
            // Bundle b = new Bundle();
            //
            // Intent mIntent1 = new Intent(getActivity(), HistoryActivity.class);
            // b.putInt(Constants.type, 0);
            // mIntent1.putExtras(b);
            // startActivity(mIntent1);
            // break;
            //
            default:
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        serverResponse = null;
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        if (apiName.equalsIgnoreCase("refresh")) {
            if (lin_home != null)
                lin_home.performClick();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // mNavigationDrawerFragment.selectItem(0);
    }

    /**/
    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = "HISTORY";
                break;
            case 2:
                mTitle = "FAVORITES";
                break;
            case 3:
                mTitle = "SETTINGS";
                break;
            case 4:
                mTitle = "ABOUT";
                break;
            case 5:
                mTitle = "TERMS OF SERVICE";
                break;
            case 6:
                mTitle = "PRIVACY POLICY";
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        /*
         * if (!mNavigationDrawerFragment.isDrawerOpen()) { // Only show items
		 * in the action bar relevant to this screen // if the drawer is not
		 * showing. Otherwise, let the drawer // decide what to show in the
		 * action bar. // getMenuInflater().inflate(R.menu.menu_screen, menu);
		 * restoreActionBar(); return true; }
		 */
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (item.getItemId()) {
            case R.id.action_search:
                System.out.println("image click");
                LocDivFragment fragment = (LocDivFragment) getFragmentByTag(LocDivFragment.TAG);
                fragment.showLayout(true);
                return true;

            case R.id.action_refresh:
                restoreActionBar(true);
                updateTabView(0);

                replaceFragement(LocDivFragment.getInstance(null),
                        LocDivFragment.TAG);
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void initControls(Bundle savedInstanceState) {

    }

    @Override
    protected void setValueOnUI() {

    }

    @Override
    protected Boolean callBackFromApi(Object object, Activity act,
                                      int requstCode) {
        if (super.callBackFromApi(object, act, requstCode)) {
            switch (requstCode) {/*
                                 * case Constants.getLocationDevice: if (object
								 * != null) { try { JSONObject jsonObject =
								 * (JSONObject) object;
								 * System.out.println("incallback====" +
								 * jsonObject); JSONObject json_query;
								 * 
								 * json_query =
								 * jsonObject.getJSONObject("response");
								 * 
								 * JSONArray jArr =
								 * json_query.getJSONArray("devices"); JSONArray
								 * jArr1 = json_query.getJSONArray("business");
								 * DeviceModel deviceModel = new DeviceModel();
								 * 
								 * for (int i = 0; i < jArr.length(); i++) {
								 * 
								 * JSONObject json_json_result =
								 * jArr.getJSONObject(i);
								 * deviceModel.setHeadline
								 * (json_json_result.getString("headline"));
								 * deviceModel
								 * .setId(json_json_result.getString("id"));
								 * deviceModel
								 * .setSsid(json_json_result.getString("ssid"));
								 * deviceModel
								 * .setCategory(json_json_result.getString
								 * ("category"));
								 * deviceModel.setSuffix(json_json_result
								 * .getString("suffix"));
								 * deviceModel.setUrl(json_json_result
								 * .getString("url"));
								 * deviceModel.setDescription
								 * (json_json_result.getString("description"));
								 * arraylist.add(deviceModel); Place place = new
								 * Place(json_json_result,0); }
								 * 
								 * for (int i = 0; i < jArr1.length(); i++) {
								 * 
								 * JSONObject json_json_result =
								 * jArr1.getJSONObject(i);
								 * deviceModel.setHeadline
								 * (json_json_result.getString("headline"));
								 * deviceModel
								 * .setBusinessName(json_json_result.getString
								 * ("businessName"));
								 * deviceModel.setPlace_id(json_json_result
								 * .getString("place_id"));
								 * deviceModel.setDescription
								 * (json_json_result.getString("description"));
								 * deviceModel
								 * .setCategory_id(json_json_result.getString
								 * ("category_id"));
								 * deviceModel.setLocationURL(json_json_result
								 * .getString("locationURL"));
								 * deviceModel.setLng
								 * (json_json_result.getString("lng"));
								 * deviceModel
								 * .setLat(json_json_result.getString("lat"));
								 * arraylist.add(deviceModel); Place place = new
								 * Place(json_json_result,0); } } catch
								 * (JSONException e) { // TODO Auto-generated
								 * catch block e.printStackTrace(); }
								 * System.out.
								 * println("inlocationarraylistlocation"
								 * +arraylist.size()); } break;
								 */
            }

        }

        return super.callBackFromApi(object, act, requstCode);
    }

}
