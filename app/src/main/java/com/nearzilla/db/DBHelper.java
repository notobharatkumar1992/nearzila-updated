package com.nearzilla.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import org.gmarz.googleplaces.models.Place;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//This is place where database handling is going on.
public class DBHelper extends SQLiteOpenHelper {

    private final static String DATABASE_NAME = "NearZilla.sqlite";
    private final static String TABLE_NAME = "WIFI_LOC_LIST";
    private final static int DATABASE_VERSION = 1;
    public final static String TABLE_NAME_FAV = "FAV_LIST";

    private String ssid = "SSID";
    private String dateTime = "DATE_TIME";
    private String name = "NAME";
    private String suffix = "SUFFIX";
    private String url = "URL";
    private String level = "LEVEL";
    private String id = "ID";
    private String current_date = "C_DATE";
    private String lat = "LAT";
    private String lng = "LNG";
    private String image = "IMAGE";
    private String image_grey = "IMAGE_GREY";
    private String addr = "ADDR";
    private String distance = "DISTANCE";
    public String isBussiness = "ISBUSSINESS";
    public String isfavroites = "ISFAVORITES";
    public String placeId = "PLACEID";
    private String mIsOpenNow = "MISOPENNOW";
    private String category = "category";

    Context context;

    // private SQLiteDatabase database;
    // private ArrayList<Place> dbList;
    private String address = "ADDRESS";
    boolean isNot = false;
    private boolean sendfav;

    public DBHelper(Context context) {
        // super(context, "/sdcard/"+DATABASE_NAME, null, DATABASE_VERSION);
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table if not exists "
                + TABLE_NAME
                + " ( "
                + id
                + " varchar,"
                // + ssid + " varchar primary key, "
                + ssid + " varchar , " + name + " varchar, " + suffix
                + " varchar, " + address + " varchar, " + url + " varchar, "
                + level + " varchar, " + current_date + " varchar, " + dateTime
                + " varchar, " + lat + " double, " + lng + " double, "
                + distance + " varchar, " + addr + " varchar, " + mIsOpenNow
                + " varchar, " + isfavroites + " varchar, " + isBussiness + " varchar, " + placeId
                + " varchar, " + image + " varchar, " + image_grey + " varchar, " + category + " varchar) ");
        db.execSQL("create table if not exists "
                + TABLE_NAME_FAV
                + " ( "
                + id
                + " varchar,"
                // + ssid + " varchar primary key, "
                + ssid + " varchar , " + name + " varchar, " + suffix
                + " varchar, " + address + " varchar, " + url + " varchar, "
                + level + " varchar, " + current_date + " varchar, " + dateTime
                + " varchar, " + lat + " double, " + lng + " double, "
                + distance + " varchar, " + addr + " varchar, "
                + isfavroites + " varchar, "
                + mIsOpenNow
                + " varchar, " + isBussiness + " varchar, " + placeId + " varchar, " + image + " varchar, " + image_grey + " varchar, " + category + " varchar) ");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public void insertIntoDatabase(SQLiteDatabase db, List<Place> list) {
        if (db == null)
            db = getWritableDatabase();
        try {
            if (list != null && list.size() > 0) {
                for (int i = 0; i < list.size(); i++) {
                    ContentValues values = getContentValue(list.get(i));
                    String ref = list.get(i).getPlaceId();

                    long ret;
                    if (!isUpdateNeed(db, TABLE_NAME, placeId, ref)) {
                        System.out.println("in ifhis==");
                        ret = db.insert(TABLE_NAME, null, values);
                    } else {
                        System.out.println("in elsehis==");
                        String selection = placeId + "=?";
                        String selectionArgs[] = {ref};
                        ret = db.updateWithOnConflict(TABLE_NAME, values,
                                selection, selectionArgs,
                                SQLiteDatabase.CONFLICT_REPLACE);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insertIntoDatabase(SQLiteDatabase db, Place list) {
        if (db == null)
            db = getWritableDatabase();
        try {
            ContentValues values = getContentValue(list);
            String ref = list.getPlaceId();

            long ret;
            if (!isUpdateNeed(db, TABLE_NAME, placeId, ref)) {
                System.out.println("in ifhis==");
                ret = db.insert(TABLE_NAME, null, values);
            } else {
                System.out.println("in elsehis==");
                String selection = placeId + "=?";
                String selectionArgs[] = {ref};
                ret = db.updateWithOnConflict(TABLE_NAME, values,
                        selection, selectionArgs,
                        SQLiteDatabase.CONFLICT_REPLACE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean islike(Context context, String id) {
        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        return dbHelper.isUpdateNeed(database, DBHelper.TABLE_NAME_FAV, this.placeId, id);
    }

    public boolean insertIntoDatabasefav(SQLiteDatabase db, Place list) {
        long ret = -1;
        System.out.println("in fAV DATABASE");

        ContentValues values = getContentValue(list);
        String refer = list.getPlaceId();
        if (!isUpdateNeed(db, TABLE_NAME_FAV, placeId, refer)) {
            sendfav = false;
            ret = db.insert(TABLE_NAME_FAV, null, values);
            Toast.makeText(context, "Item added to favorites list", Toast.LENGTH_LONG).show();
        } else {
            sendfav = true;
            System.out.println("on else");
            String selection = placeId + "=?";
            String selectionArgs[] = {refer};
            ret = db.updateWithOnConflict(TABLE_NAME_FAV, values, selection, selectionArgs, SQLiteDatabase.CONFLICT_REPLACE);
            Toast.makeText(context, "Item succesfuly removed from favorites list", Toast.LENGTH_LONG).show();
        }

        return sendfav;
    }

    public ContentValues getContentValue(Place place) {
        ContentValues values = new ContentValues();
        values.put(id, place.getReference());
        values.put(name, place.getName());
        values.put(ssid, place.getSsid());
        values.put(suffix, place.getSuffix());
        values.put(url, place.getUrl());
        values.put(addr, place.getAddress());
        values.put(address, place.getAddress());
        values.put(placeId, place.getPlaceId());
        //	System.out.println("datetime++++++++" + place.isBussiness);
        values.put(isBussiness, place.isBussiness);
        values.put(isfavroites, place.isFavorites);

        values.put(mIsOpenNow, place.isIsOpenNow());
        //		System.out.println("datetime++++++++" + place.getDateTime());

        String[] separated = place.getDateTime().split(",");
        //	System.out.println("splitdate" + separated[0]);
        //	System.out.println("byconvertstring tomili=="
        //			+ UtilsClass.getMilisecondsFromDate(separated[0]));
        //	System.out.println("byconvertmili tostring=="
        //			+ UtilsClass.getDate(UtilsClass
        //					.getMilisecondsFromDate(separated[0])));

        values.put(dateTime, place.getDateTime());
        String cDate = getCurrentDateTime();
        values.put(current_date, cDate);
        values.put(lat, place.getLatitude());
        values.put(lng, place.getLongitude());
        values.put(image, place.getIcon());
        values.put(image_grey, place.getIconGrey());
        values.put(distance, place.getDistance());
        values.put(category, place.mCatId);
        return values;
    }

    public ArrayList<Place> readFromDatabase(SQLiteDatabase db,
                                             boolean isHisotry) {
        ArrayList<Place> arrayList = new ArrayList<Place>();
        Cursor cursor;
        if (isHisotry) {
            // cursor = db.rawQuery("Select * from " + TABLE_NAME, null);
            cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME + " ORDER BY "
                    + distance + " ASC LIMIT  40", null);

        } else {
            cursor = db.rawQuery("Select * from " + TABLE_NAME_FAV + " ORDER BY " + name + "  ASC LIMIT  40 ", null);
        }
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                Place info = new Place();
                info.setReference(cursor.getString(cursor.getColumnIndex(id)));
                info.setSsid(cursor.getString(cursor.getColumnIndex(ssid)));
                info.setName(cursor.getString(cursor.getColumnIndex(name)));
                info.setIsOpenNow(cursor.getString(cursor
                        .getColumnIndex(mIsOpenNow)));
                //	System.out.println("incursor====="
                //					+ cursor.getString(cursor.getColumnIndex(isBussiness)));
                info.setPlaceId(cursor.getString(cursor.getColumnIndex(placeId)));

                info.setDBisBussiness(cursor.getString(cursor
                        .getColumnIndex(isBussiness)));
                info.setIsFavorites(cursor.getString(cursor
                        .getColumnIndex(isfavroites)));
                info.setAddress(cursor.getString(cursor.getColumnIndex(addr)));
                info.setDistance(Double.parseDouble(cursor.getString(cursor
                        .getColumnIndex(distance))));
                String suffixstr = cursor.getString(cursor
                        .getColumnIndex(suffix));
                if (suffixstr == null || suffixstr.length() == 0) {
                    info.setLocation(true);
                    info.setIcon(cursor.getString(cursor.getColumnIndex(image)));
                    info.setIconGrey(cursor.getString(cursor.getColumnIndex(image_grey)));
                    info.setAddress(cursor.getString(cursor
                            .getColumnIndex(address)));
                    info.setDateTime(cursor.getString(cursor
                            .getColumnIndex(dateTime)));
                    info.setLatitude(cursor.getDouble(cursor
                            .getColumnIndex(lat)));
                    info.setLongitude(cursor.getDouble(cursor
                            .getColumnIndex(lng)));
                } else {
                    // info.setImage(R.drawable.wifi);
                    info.setIcon(cursor.getString(cursor.getColumnIndex(image)));
                    info.setIconGrey(cursor.getString(cursor.getColumnIndex(image_grey)));
                    info.setDateTime(cursor.getString(cursor
                            .getColumnIndex(dateTime)));
                }
                info.setSuffix(suffixstr);
                // info.setLevel(Integer.parseInt(cursor.getString(cursor.getColumnIndex(level))));
                info.setUrl(cursor.getString(cursor.getColumnIndex(url)));
                info.mCatId = cursor.getString(cursor.getColumnIndex(category));

                // info.setDateTime(cursor.getString(cursor.getColumnIndex(current_date)));
                arrayList.add(info);
            } while (cursor.moveToNext());
        }
        return arrayList;
    }

    private String getCurrentDateTime() {
        Date now = new Date();
        String nowAsString = new SimpleDateFormat("dd-MM-yyyy").format(now);
        return nowAsString;
    }

    /*
     * public void deleteRecord(SQLiteDatabase db, Place info) {
     * System.out.println("in databsedelete========="+info.getName()); String
     * where = name + " = ? "; String[] whereArgs = new String[] {
     * info.getName()}; db.delete(TABLE_NAME, where, whereArgs); }
     */
    public void deleteRecordSingle(Place info) {
        SQLiteDatabase database = this.getWritableDatabase();
        String where = placeId + "=?";
        String[] whereArgs = new String[]{info.getPlaceId()};
        database.delete(TABLE_NAME_FAV, where, whereArgs);
        Toast.makeText(context, "Item removed from favorites list", Toast.LENGTH_LONG).show();
    }

    public void deleteRecord(SQLiteDatabase db, ArrayList<String> info) {
        System.out.println("in database delete=========" + info.size());
        for (int i = 0; i < info.size(); i++) {
            String where = name + " = ? ";
            // Place place=info.get(i);
            String[] whereArgs = new String[]{info.get(i)};
            db.delete(TABLE_NAME, where, whereArgs);
        }
    }

    public void deleteRecordFav(SQLiteDatabase db, ArrayList<String> info) {
        System.out.println("in databsedelete=========" + info.size());
        for (int i = 0; i < info.size(); i++) {
            String where = name + " = ? ";
            // Place place=info.get(i);
            String[] whereArgs = new String[]{info.get(i)};
            db.delete(TABLE_NAME_FAV, where, whereArgs);
        }
    }

    public void deleteAllRecord(Context context, int type) {
        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        if (type == 0) {
            database.delete(TABLE_NAME, null, null);
        } else {
            database.delete(TABLE_NAME_FAV, null, null);
        }

    }

    public boolean isUpdateNeed(SQLiteDatabase db, String tblName, String columnName, String colValue) {
        Cursor cursor = null;
        try {
            // Select All Query
            String selectQuery = "SELECT * FROM " + tblName + " WHERE " + columnName + " = '" + colValue + "'";
            Log.i("Query", "=" + selectQuery);
            cursor = db.rawQuery(selectQuery, null);
            System.out.println("isupdated====" + cursor);
        } catch (SQLiteException e) {
            Log.i("update", "=" + e.getMessage());
            // return false;
        } catch (Exception e) {
            Log.i("update", "=" + e.getMessage());
            // return false;
        }
        if (cursor != null && cursor.getCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public void setHistoryrow(SQLiteDatabase db, String v1, String id) {
        ContentValues values = new ContentValues();
        values.put(isfavroites, v1);
        String whrArg[] = {id};
        int up = db.update(TABLE_NAME, values, placeId + "=?", whrArg);
        Log.d("updatequery", String.valueOf(up));
    }


    public ArrayList<Place> readFromDatabaseHistory(SQLiteDatabase db) {
        ArrayList<Place> arrayList = new ArrayList<Place>();
        Cursor cursor;

        // cursor = db.rawQuery("Select * from " + TABLE_NAME, null);
        cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME + " ORDER BY "
                + distance + " ASC LIMIT  40", null);


        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                Place info = new Place();
                info.setReference(cursor.getString(cursor.getColumnIndex(id)));
                info.setSsid(cursor.getString(cursor.getColumnIndex(ssid)));
                info.setName(cursor.getString(cursor.getColumnIndex(name)));
                info.setIsOpenNow(cursor.getString(cursor.getColumnIndex(mIsOpenNow)));
                //		System.out.println("incursor====="
                //				+ cursor.getString(cursor.getColumnIndex(isBussiness)));
                info.setPlaceId(cursor.getString(cursor.getColumnIndex(placeId)));

                info.setDBisBussiness(cursor.getString(cursor
                        .getColumnIndex(isBussiness)));

                info.setIsFavorites(cursor.getString(cursor
                        .getColumnIndex(isfavroites)));
                info.setAddress(cursor.getString(cursor.getColumnIndex(addr)));
                try {
                    String dis = cursor.getString(cursor.getColumnIndex(distance));
                    if (dis != null && !dis.isEmpty())
                        info.setDistance(Double.parseDouble(dis));
                } catch (NumberFormatException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String suffixstr = cursor.getString(cursor
                        .getColumnIndex(suffix));
                if (suffixstr == null || suffixstr.length() == 0) {
                    info.setLocation(true);
                    info.setIcon(cursor.getString(cursor.getColumnIndex(image)));
                    info.setIconGrey(cursor.getString(cursor.getColumnIndex(image_grey)));
                    info.setAddress(cursor.getString(cursor
                            .getColumnIndex(address)));
                    info.setDateTime(cursor.getString(cursor
                            .getColumnIndex(dateTime)));
                    info.setLatitude(cursor.getDouble(cursor
                            .getColumnIndex(lat)));
                    info.setLongitude(cursor.getDouble(cursor
                            .getColumnIndex(lng)));
                } else {
                    // info.setImage(R.drawable.wifi);
                    info.setIcon(cursor.getString(cursor.getColumnIndex(image)));
                    info.setIconGrey(cursor.getString(cursor.getColumnIndex(image_grey)));
                    info.setDateTime(cursor.getString(cursor
                            .getColumnIndex(dateTime)));
                }
                info.setSuffix(suffixstr);
                // info.setLevel(Integer.parseInt(cursor.getString(cursor.getColumnIndex(level))));
                info.setUrl(cursor.getString(cursor.getColumnIndex(url)));
                info.mCatId = cursor.getString(cursor.getColumnIndex(category));

                // info.setDateTime(cursor.getString(cursor.getColumnIndex(current_date)));
                arrayList.add(info);
            } while (cursor.moveToNext());
        }
        return arrayList;
    }


}
