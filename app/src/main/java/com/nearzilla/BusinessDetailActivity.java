package com.nearzilla;

import org.gmarz.googleplaces.models.Place;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.baseClasses.BaseFragmentActivity;
import com.nearzilla.fragments.NavigationDrawerFragment;

public class BusinessDetailActivity extends BaseFragmentActivity implements
		NavigationDrawerFragment.NavigationDrawerCallbacks {
	WebView webview;
	Place place;
	String where, name;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		enableBackButton();
		Bundle bundle = getIntent().getExtras();
		if (bundle != null) {
			where = bundle.getString("urlfordatabase");
			name = bundle.getString("namedetail");
			System.out.println("wheretype-----" + name);
		}
		getSupportActionBar().setTitle(name);
		WebView theWebPage = new WebView(this);
		theWebPage.setWebViewClient(new WebViewClient()); // the lines of code
		theWebPage.setWebChromeClient(new WebChromeClient()); // same as above
		setContentView(theWebPage);
		theWebPage.loadUrl(where);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == android.R.id.home) {
			finish();
			return true;
		}
		return true;
	}

	@Override
	protected void initControls(Bundle savedInstanceState) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void setValueOnUI() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onNavigationDrawerItemSelected(int position) {
		// TODO Auto-generated method stub

	}

	private void init() {
		// TODO Auto-generated method stub
		webview = (WebView) findViewById(R.id.detail_webview);
	}

}
