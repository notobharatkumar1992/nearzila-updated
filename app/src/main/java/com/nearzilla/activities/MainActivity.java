package com.nearzilla.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.nearzilla.R;
import com.nearzilla.adapters.SectionsPagerAdapter;
import com.nearzilla.fragments.NewFavouriteFragment;
import com.nearzilla.fragments.NewHistoryFragment;
import com.nearzilla.fragments.NewRecentListFragment;

import java.util.ArrayList;

import carbon.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ArrayList<Fragment> arrayFragment = new ArrayList<>();
    private ViewPager mViewPager;

    private TextView txt_c_recent, txt_c_fav, txt_c_history;
    private View view_recent, view_fav, view_history;

    public final static int ADAPTER_RECENT = 0, ADAPTER_FAV = 1, ADAPTER_HIS = 2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
    }

    private void initView() {
        findViewById(R.id.img_c_right).setOnClickListener(this);
        txt_c_recent = (TextView) findViewById(R.id.txt_c_recent);
        txt_c_fav = (TextView) findViewById(R.id.txt_c_fav);
        txt_c_history = (TextView) findViewById(R.id.txt_c_history);

        view_recent = (View) findViewById(R.id.view_recent);
        view_fav = (View) findViewById(R.id.view_fav);
        view_history = (View) findViewById(R.id.view_history);

        findViewById(R.id.rl_c_recent).setOnClickListener(this);
        findViewById(R.id.rl_c_fav).setOnClickListener(this);
        findViewById(R.id.rl_c_history).setOnClickListener(this);

        arrayFragment.add(new NewRecentListFragment());
        arrayFragment.add(new NewFavouriteFragment());
        arrayFragment.add(new NewHistoryFragment());
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), arrayFragment);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switchPage(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mViewPager.postDelayed(new Runnable() {
            @Override
            public void run() {
                mViewPager.setCurrentItem(0);
                switchPage(0);
            }
        }, 400);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_right:
                startActivity(new Intent(MainActivity.this, NewSettingActivity.class));
                break;
            case R.id.rl_c_recent:
                switchPage(0);
                mViewPager.setCurrentItem(0, true);
                break;

            case R.id.rl_c_fav:
                switchPage(1);
                mViewPager.setCurrentItem(1, true);
                break;

            case R.id.rl_c_history:
                switchPage(2);
                mViewPager.setCurrentItem(2, true);
                break;
        }
    }

    private void switchPage(int value) {
        txt_c_recent.setTextColor(getResources().getColor(R.color.text_shadow));
        txt_c_fav.setTextColor(getResources().getColor(R.color.text_shadow));
        txt_c_history.setTextColor(getResources().getColor(R.color.text_shadow));

        view_recent.setVisibility(View.GONE);
        view_fav.setVisibility(View.GONE);
        view_history.setVisibility(View.GONE);
        switch (value) {
            case 0:
                txt_c_recent.setTextColor(Color.BLACK);
                view_recent.setVisibility(View.VISIBLE);
                break;
            case 1:
                txt_c_fav.setTextColor(Color.BLACK);
                view_fav.setVisibility(View.VISIBLE);
                break;
            case 2:
                txt_c_history.setTextColor(Color.BLACK);
                view_history.setVisibility(View.VISIBLE);
                break;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


//    /**
//     * A placeholder fragment containing a simple view.
//     */
//    public static class PlaceholderFragment extends Fragment {
//        /**
//         * The fragment argument representing the section number for this
//         * fragment.
//         */
//        private static final String ARG_SECTION_NUMBER = "section_number";
//
//        public PlaceholderFragment() {
//        }
//
//        /**
//         * Returns a new instance of this fragment for the given section
//         * number.
//         */
//        public static PlaceholderFragment newInstance(int sectionNumber) {
//            PlaceholderFragment fragment = new PlaceholderFragment();
//            Bundle args = new Bundle();
//            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
//            fragment.setArguments(args);
//            return fragment;
//        }
//
//        @Override
//        public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                                 Bundle savedInstanceState) {
//            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
//            android.widget.TextView textView = (android.widget.TextView) rootView.findViewById(R.id.section_label);
//            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
//            return rootView;
//        }
//    }

//    /**
//     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
//     * one of the sections/tabs/pages.
//     */
//    public class SectionsPagerAdapter extends FragmentPagerAdapter {
//
//        public SectionsPagerAdapter(FragmentManager fm) {
//            super(fm);
//        }
//
//        @Override
//        public Fragment getItem(int position) {
//            // getItem is called to instantiate the fragment for the given page.
//            // Return a PlaceholderFragment (defined as a static inner class below).
//            return PlaceholderFragment.newInstance(position + 1);
//        }
//
//        @Override
//        public int getCount() {
//            // Show 3 total pages.
//            return 3;
//        }
//
//        @Override
//        public CharSequence getPageTitle(int position) {
//            switch (position) {
//                case 0:
//                    return "SECTION 1";
//                case 1:
//                    return "SECTION 2";
//                case 2:
//                    return "SECTION 3";
//            }
//            return null;
//        }
//    }

}
