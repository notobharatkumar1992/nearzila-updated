package com.nearzilla.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.baseClasses.Constants;
import com.nearzilla.R;

import java.util.ArrayList;

public class NewSettingActivity extends FragmentActivity implements OnClickListener {
    public static final String TAG = "SET";
    // private Button incresB, decresB, setradiosButton;
    ImageView kilom_Buton, mile_Buton;
    // LinearLayout incresBR, decresBR;
    // private TextView setradiosTextR;
    ArrayList<Integer> radious;
    ArrayList<String> type;
    // static int pos = 0, POSTYPE = 1;
    // String value = "KM";
    public static String miles = "Miles";
    public static String km = "KM";
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;
    double METERCHANGE = 1609.344;

    // SeekBar seekBar;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_activity);
        initUi();
        setListener();
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        getDistanceType();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.kilom_buton:
                selectKm();
                break;
            case R.id.mile_buton:
                selectMile();
                break;
            default:
                break;
        }

    }

    private void selectKm() {
        mile_Buton.setImageResource(R.drawable.mile_hover);
        kilom_Buton.setImageResource(R.drawable.kilomitter);
        setDistanceType(km);
    }

    private void selectMile() {
        mile_Buton.setImageResource(R.drawable.mile);
        kilom_Buton.setImageResource(R.drawable.kilomitter_hover);
        setDistanceType(miles);
    }

    private void setDistanceType(String disType) {
        editor = sharedpreferences.edit();
        editor.putString(Constants.disType, disType);
        editor.commit();
    }

    private void getDistanceType() {
        String disType = sharedpreferences.getString(Constants.disType, miles);
        if (disType.equalsIgnoreCase(km)) {
            selectKm();
        } else {
            selectMile();
        }
    }

    protected void initUi() {
        // TODO Auto-generated method stub
        kilom_Buton = (ImageView) findViewById(R.id.kilom_buton);
        mile_Buton = (ImageView) findViewById(R.id.mile_buton);
        radious = new ArrayList<Integer>();
        type = new ArrayList<String>();
        setvalue();

        sharedpreferences = getSharedPreferences(Constants.MyPREFERENCES, 0);
    }

    private void setvalue() {
        // TODO Auto-generated method stub
        radious.add(0);
        radious.add(1);
        radious.add(2);
        radious.add(3);
        radious.add(4);
        radious.add(5);
        radious.add(6);
        radious.add(7);
        radious.add(8);
        radious.add(9);
        radious.add(10);
        type.add("Miles");
        type.add("Meter");
        type.add("Kilometer");
        System.out.println("sizeofarray====" + radious.size());
    }

    protected void setListener() {
        // TODO Auto-generated method stub
        kilom_Buton.setOnClickListener(this);
        mile_Buton.setOnClickListener(this);
    }
}
