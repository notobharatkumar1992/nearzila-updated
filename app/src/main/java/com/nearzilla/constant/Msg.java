package com.nearzilla.constant;

/**
 * Created by Bharat on 09/16/2016.
 */
public class Msg {

    public static final int SHOW_PROGRESS = 10;
    public static final int HIDE_PROGRESS = 11;
    public static final int SHOW_PROGRESS_BAR = 12;
    public static final int HIDE_PROGRESS_BAR = 13;

    public static final int INVALIDATE_LIST_1 = 1;
    public static final int INVALIDATE_LIST_2 = 2;
    public static final int INVALIDATE_LIST_3 = 3;

}
