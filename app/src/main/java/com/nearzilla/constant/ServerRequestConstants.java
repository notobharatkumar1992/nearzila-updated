package com.nearzilla.constant;

public class ServerRequestConstants {
    /**
     * (All Constants that were used in server connection are declared)
     */

	/*
     * Post Parameter Type Constants
	 */
    public static final String CODE_200 = "200";
    public static final String CODE_400 = "400";
    public static final String CODE_401 = "401";
    public static final String CODE_402 = "402";
    public static final String CODE_403 = "403";
    public static final String CODE_404 = "404";
    public static final String Key_PostStrValue = "String";
    public static final String Key_PostDoubleValue = "double";
    public static final String Key_PostintValue = "int";
    public static final String Key_PostbyteValue = "byte";
    public static final String Key_PostFileValue = "File";
    public static final String Key_PostStringArrayValue = "String[]";
    public static final String Key_PostStringArrayValue1 = "String[]";

    //    public static final String CHAT_SERVER_URL = "http://192.168.100.34:3000/";
    public static final String CHAT_SERVER_URL = "http://notosolutions.net:3002/";
    public static final String BASE_URL = "http://notosolutions.net/fitplus/webServices/";
    //   public static final String BASE_URL = "http://192.168.100.53/fitplus/webServices/";
    //  public static final String BASE_URL_DRIVER = "http://192.168.100.81/short/webServicesDriver/";

    public static final String getLocationsFromServer = "http://rigges.com/temp2/webservices/getLocationDevice";
    // String getLocationsFromServer =
    // "http://192.168.1.14/riggies/webservices/getLocationDevice";

}

