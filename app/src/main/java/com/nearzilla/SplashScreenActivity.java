package com.nearzilla;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;

import com.baseClasses.BaseFragmentActivity;
import com.baseClasses.Constants;
import com.crashlytics.android.Crashlytics;
import com.nearzilla.activities.MainActivity;
import com.networkTask.ApiManager;
import com.preference.MySharedPreferences;
import com.utilities.MyLocation;
import com.utilities.MyLocation.LocationResult;
import com.utilities.UtilsClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import io.fabric.sdk.android.Fabric;

public class SplashScreenActivity extends BaseFragmentActivity {

    private static final long SPLASH_DURATION = 1000;
    private boolean isWifiScanningRunning = true;
    private boolean isLocation = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.bg_white);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash_screen);
        ActionBar actionBar = getSupportActionBar();
        // actionBar.setIcon(R.drawable.email_icon);
        actionBar.hide();


        final WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        if (wifi.isWifiEnabled() == false) {
//            Toast.makeText(getApplicationContext(), "Enabling Wi-Fi",
//                    Toast.LENGTH_LONG).show();
            wifi.setWifiEnabled(true);
        } else {
            WifiManager wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            Log.d("wifiInfo", wifiInfo.toString());
            Log.d("SSID", wifiInfo.getSSID());
            MySharedPreferences.getInstance().putStringKeyValue(this, Constants.getSSID, wifiInfo.getSSID());
//            MySharedPreferences.getInstance().putStringKeyValue(this, Constants.getSSID, "");
        }
        task();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.splash_screen, menu);

        return true;
    }

    private void setHeightWidth() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int widthPixel = displayMetrics.widthPixels;
        int heightPixel = displayMetrics.heightPixels;
        // System.out.println("In Pixels Width : "+widthPixel+" Height : "+heightPixel);

        AppDelegate.setWidthPixel(widthPixel);
        AppDelegate.setHeightPixel(heightPixel);
    }

    private void task() {
        setHeightWidth();
        // createDatabase();
        AppDelegate.listofValidwifi.clear();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getLocation();
        wifiScaning();
    }

    // wifi scanning process is starting from here while checking wifi is enable
    // or not.
    private void wifiScaning() {
        final WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        if (wifi.isWifiEnabled() == false) {
//            Toast.makeText(getApplicationContext(), "Enabling Wi-Fi",
//                    Toast.LENGTH_LONG).show();
            wifi.setWifiEnabled(true);
        }

        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context c, Intent intent) {
                List<ScanResult> results = wifi.getScanResults();
                if (results == null) {
                    return;
                }

                for (int i = 0; i < results.size(); i++) {
                    if (WifiManager.calculateSignalLevel(results.get(i).level,
                            5) >= 0) {
                        AppDelegate.listofValidwifi.add(results
                                .get(i));
                    }
                }

                try {
                    SplashScreenActivity.this.unregisterReceiver(this);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                isWifiScanningRunning = false;

                if (!isLocation) {
                    getCategory();
                    // getLocationdetail();

                }
            }
        }, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        wifi.startScan();
    }

	/*
     * private void getLocation(){ final MyLocation myLocation = new
	 * MyLocation(); myLocation.getLocation(SplashScreenActivity.this, new
	 * LocationResult() {
	 * 
	 * @Override public void gotLocation(Context context, Location location) {
	 * if(location != null){ myLocation.removeUpdate(); MyApplication myApp =
	 * MyApplication.getApplication();
	 * myApp.setCurrentLatitude(location.getLatitude());
	 * myApp.setCurrentLongitude(location.getLongitude());
	 * 
	 * isLocation = false;
	 * 
	 * if(!isWifiScanningRunning){ getCategory(); } } } }, 5); }
	 */

    public void getLocation() {
        if (UtilsClass.checkGPS(this)) {
            final MyLocation mylocation = new MyLocation();
            LocationResult result = new LocationResult() {

                @Override
                public void gotLocation(Context context, final Location location) {
                    System.out.println("Location :" + location);
                    if (location != null) {
                        ((Activity) context).runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                try {
                                    mylocation.removeUpdate();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                AppDelegate.setCurrentLatitude(location.getLatitude());
                                AppDelegate.setCurrentLongitude(location.getLongitude());
//								myApp.setCurrentLatitude(Double
//										.parseDouble("56.1304"));
//								myApp.setCurrentLongitude(Double
//										.parseDouble("106.3468"));

                                isLocation = false;

                                if (!isWifiScanningRunning) {
                                    getCategory();
                                    // getLocationdetail();
                                }
                            }
                        });
                    }
                }
            };
            mylocation.getLocation(this, result, 20);
        }
    }

    private void getCategory() {
        ApiManager.getInstance().getCategories(this);
    }

    private void getLocationdetail() {
        ApiManager.getInstance().getLocations(this, 26.023, 74.202, "SSID1",
                "30", "Headline", 2000);
    }

    @Override
    protected Boolean callBackFromApi(Object object, Activity act, int requstCode) {
        if (super.callBackFromApi(object, act, requstCode)) {
            JSONObject jObj = (JSONObject) object;
            JSONArray jArr = jObj.optJSONArray(Constants.response);
            if (jArr != null) {
                Log.i(TAG, jArr.toString());
                AppDelegate.setCategories(jArr);
            }
//            startActivity(new Intent(SplashScreenActivity.this, LocationsActivity.class));
            startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
            SplashScreenActivity.this.finish();
        }
        return true;
    }

    @Override
    protected void initControls(Bundle savedInstanceState) {
    }

    @Override
    protected void setValueOnUI() {
    }
}
