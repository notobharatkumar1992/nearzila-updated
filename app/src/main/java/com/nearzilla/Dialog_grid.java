package com.nearzilla;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.widget.ListView;

import com.nearzilla.adapters.DialogDetailAdapter;

public class Dialog_grid extends Activity {
	ListView detailList;
	DialogDetailAdapter adapter;
	JSONArray array;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		 requestWindowFeature(Window.FEATURE_NO_TITLE);
		 
		setContentView(R.layout.activity_dialog_grid);
		detailList = (ListView) findViewById(R.id.dilog_list);
		Intent intent = getIntent();
		String jsonArray = intent.getStringExtra("Weekday");

		try {
			 array = new JSONArray(jsonArray);
			System.out.println(array.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}

		adapter = new DialogDetailAdapter(this, R.layout.row_wifilist_his,
				array);
		detailList.setAdapter(adapter);
		detailList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

	}

}
