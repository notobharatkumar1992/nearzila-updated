package com.nearzilla;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.baseClasses.BaseFragmentActivity;
import com.nearzilla.fragments.SettingFragment;

public class SettingActivity extends BaseFragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container);
        //	getSupportActionBar().setTitle("Settings");
        restoreActionBar();
        replaceFragement(SettingFragment.getInstance(null));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return true;
    }

    @Override
    protected void initControls(Bundle savedInstanceState) {
        // TODO Auto-generated method stub

    }

    @Override
    protected void setValueOnUI() {
        // TODO Auto-generated method stub

    }


}
