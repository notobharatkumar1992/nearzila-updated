package com.nearzilla;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.baseClasses.BaseFragmentActivity;
import com.nearzilla.fragments.NavigationDrawerFragment;

public class InAppBrowser extends BaseFragmentActivity implements
		NavigationDrawerFragment.NavigationDrawerCallbacks {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// enableBackButton();
		ActionBar actionBar = getSupportActionBar();
		LayoutInflater mInflater = LayoutInflater.from(this);
		View mCustomView = mInflater.inflate(
				R.layout.actionbar_custom_view_home, null);

		img_back = (ImageView) mCustomView.findViewById(R.id.img_back);
		img_back.setVisibility(View.VISIBLE);
		img_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
		actionBar.setCustomView(mCustomView);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setDisplayShowTitleEnabled(false);
		WebView theWebPage = new WebView(this);
		theWebPage.setWebViewClient(new WebViewClient()); // the lines of code
		theWebPage.setWebChromeClient(new WebChromeClient()); // same as above
		setContentView(theWebPage);
		theWebPage.loadUrl("http://rigges.com/temp2/pages/termAndCondition");
	}

	@Override
	protected void initControls(Bundle savedInstanceState) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void setValueOnUI() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onNavigationDrawerItemSelected(int position) {
		// TODO Auto-generated method stub

	}

}
