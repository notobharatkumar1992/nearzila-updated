package com.nearzilla;

import android.os.Bundle;
import android.view.MenuItem;

import com.baseClasses.BaseFragmentActivity;
import com.nearzilla.fragments.LocationDetailFragment;
import com.nearzilla.fragments.NavigationDrawerFragment;

public class LocationDetailActivity extends BaseFragmentActivity implements
		NavigationDrawerFragment.NavigationDrawerCallbacks {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_container_drawer);
		enableBackButton();
		
		replaceFragement(LocationDetailFragment.getInstance(getBundle()), LocationDetailFragment.TAG);
	}

	@Override
	protected void initControls(Bundle savedInstanceState) {

	}

	@Override
	protected void setValueOnUI() {

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();
		if (id == android.R.id.home) {
			this.finish();
			return true;
		}
		return true;
	}

	@Override
	public void onNavigationDrawerItemSelected(int position) {
		// TODO Auto-generated method stub

	}

}
