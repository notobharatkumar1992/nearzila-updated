package com.nearzilla.beans;

public class ModelDrawer {

	private static final long serialVersionUID = 111111L;
	private String title = "";
	private int iconSelect = 0;
	private int iconUnselect = 0;
	private int color = 0;
	private boolean isChecked = false;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getIconSelect() {
		return iconSelect;
	}

	public void setIconSelect(int iconSelect) {
		this.iconSelect = iconSelect;
	}

	public int getIconUnselect() {
		return iconUnselect;
	}

	public void setIconUnselect(int iconUnselect) {
		this.iconUnselect = iconUnselect;
	}

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}

	public boolean isChecked() {
		return isChecked;
	}

	public void setChecked(boolean isChecked) {
		this.isChecked = isChecked;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "ModelDrawer [title=" + title + ", iconSelect=" + iconSelect
				+ ", iconUnselect=" + iconUnselect + ", color=" + color
				+ ", isChecked=" + isChecked + "]";
	}

}
