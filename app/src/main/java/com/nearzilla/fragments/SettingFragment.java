package com.nearzilla.fragments;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.baseClasses.BaseFragment;
import com.baseClasses.Constants;
import com.nearzilla.LocationsActivity;
import com.nearzilla.R;

import java.util.ArrayList;

public class SettingFragment extends BaseFragment implements OnClickListener {
	public static final String TAG = "SET";
	// private Button incresB, decresB, setradiosButton;
	ImageView kilom_Buton, mile_Buton;
	// LinearLayout incresBR, decresBR;
	// private TextView setradiosTextR;
	ArrayList<Integer> radious;
	ArrayList<String> type;
	// static int pos = 0, POSTYPE = 1;
	// String value = "KM";
	public static String miles = "Miles";
	public static String km = "KM";
	SharedPreferences sharedpreferences;
	SharedPreferences.Editor editor;
	double METERCHANGE = 1609.344;

	// SeekBar seekBar;

	public static SettingFragment getInstance(Bundle bundle) {
		SettingFragment fragment = new SettingFragment();
		if (bundle != null) {
			fragment.setArguments(bundle);
		}
		return fragment;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		((LocationsActivity) getActivity()).showBackButton(true);
		((LocationsActivity) getActivity()).layout_bottom
				.setVisibility(View.GONE);
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		((LocationsActivity) getActivity()).showBackButton(false);
		((LocationsActivity) getActivity()).layout_bottom
				.setVisibility(View.VISIBLE);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_setting, null);
		initUi(view);
		setListener();

		/*
		 * seekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
		 * 
		 * @Override public void onStopTrackingTouch(SeekBar arg0) { // TODO
		 * Auto-generated method stub
		 * 
		 * }
		 * 
		 * @Override public void onStartTrackingTouch(SeekBar arg0) { // TODO
		 * Auto-generated method stub
		 * 
		 * }
		 * 
		 * @Override public void onProgressChanged(SeekBar arg0, int arg1,
		 * boolean arg2) { // TODO Auto-generated method stub
		 * System.out.println("prog===" + arg1); switch (arg1) { case 1:
		 * setradiosTextR.setText("1"); break; case 2:
		 * setradiosTextR.setText("2"); break; case 3:
		 * setradiosTextR.setText("3"); break; case 4:
		 * setradiosTextR.setText("4"); break; case 5:
		 * setradiosTextR.setText("5"); break; case 6:
		 * setradiosTextR.setText("6"); break; case 7:
		 * setradiosTextR.setText("7"); break; case 8:
		 * setradiosTextR.setText("8"); break; case 9:
		 * setradiosTextR.setText("9"); break; case 10:
		 * setradiosTextR.setText("10"); break; default: break; } } });
		 */
		return view;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		// pos = 1;
		// POSTYPE = 1;
		super.onResume();
		getDistanceType();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		/*
		 * case R.id.incresR_buton: decresBR.setVisibility(View.VISIBLE); pos =
		 * pos + 1; seekBar.setProgress(pos); if (pos == 10) {
		 * incresBR.setVisibility(View.INVISIBLE); }
		 * System.out.println("curindexvalue====" + radious.get(pos));
		 * //setradiosTextR.setText(radious.get(pos).toString()); break; case
		 * R.id.decresR_buton: //incresBR.setVisibility(View.VISIBLE); pos = pos
		 * - 1; seekBar.setProgress(pos); if (pos == 1) {
		 * decresBR.setVisibility(View.INVISIBLE); }
		 * System.out.println("curindexvalue====" + radious.get(pos));
		 * setradiosTextR.setText(radious.get(pos).toString()); break;
		 */
		case R.id.kilom_buton:
			selectKm();
			break;
		case R.id.mile_buton:
			selectMile();
			break;
		/*
		 * case R.id.setradious_buton:
		 * 
		 * calculation(); startActivity(new Intent(getActivity(),
		 * LocationsActivity.class)); getActivity().finish(); break;
		 */
		default:
			break;
		}

	}

	/*
	 * private void calculation() { // TODO Auto-generated method stub if
	 * (value.equals("Miles")) { // confusen //int send_radious = (int)
	 * (Integer.parseInt(setradiosTextR.getText().toString()) * METERCHANGE);
	 * int send_radious = (int)
	 * (Integer.parseInt(setradiosTextR.getText().toString()));
	 * editor.putString(Constants.radious, String.valueOf(send_radious)); //
	 * Toast.makeText(getActivity(), // "miles==" +
	 * String.valueOf(send_radious), // Toast.LENGTH_SHORT).show();// radious to
	 * store editor.commit();
	 * 
	 * } else if (value.equals("Kilometer")) { //int sendkm =
	 * Integer.parseInt(setradiosTextR.getText().toString()) * 1000; int sendkm
	 * = (int) (Integer.parseInt(setradiosTextR.getText().toString()) * 0.62);
	 * // Toast.makeText(getActivity(), "kilometer" + // String.valueOf(sendkm),
	 * // Toast.LENGTH_SHORT).show(); editor.putString(Constants.radious,
	 * String.valueOf(sendkm)); // radious // to // store editor.commit(); } }
	 */

	private void selectKm() {
		mile_Buton.setImageResource(R.drawable.mile_hover);
		kilom_Buton.setImageResource(R.drawable.kilomitter);
		setDistanceType(km);
	}

	private void selectMile() {
		mile_Buton.setImageResource(R.drawable.mile);
		kilom_Buton.setImageResource(R.drawable.kilomitter_hover);
		setDistanceType(miles);
	}

	private void setDistanceType(String disType) {
		editor = sharedpreferences.edit();
		editor.putString(Constants.disType, disType);
		editor.commit();
	}

	private void getDistanceType() {
		String disType = sharedpreferences.getString(Constants.disType, miles);
		if (disType.equalsIgnoreCase(km)) {
			selectKm();
		} else {
			selectMile();
			;
		}
	}

	@Override
	protected void initUi(View view) {
		// TODO Auto-generated method stub
		// seekBar = (SeekBar) view.findViewById(R.id.seek_bar);
		getActionBar().setTitle("Settings");
		// setradiosButton = (Button) view.findViewById(R.id.setradious_buton);
		kilom_Buton = (ImageView) view.findViewById(R.id.kilom_buton);
		mile_Buton = (ImageView) view.findViewById(R.id.mile_buton);
		// setradiosText = (TextView) view.findViewById(R.id.setradious_text);
		// incresBR = (LinearLayout) view.findViewById(R.id.incresR_buton);
		// decresBR = (LinearLayout) view.findViewById(R.id.decresR_buton);
		// setradiosTextR = (TextView) view.findViewById(R.id.setradiousR_text);
		radious = new ArrayList<Integer>();
		type = new ArrayList<String>();
		setvalue();
		// decresBR.setVisibility(View.INVISIBLE);
		// decresB.setVisibility(View.INVISIBLE);

		sharedpreferences = getActivity().getSharedPreferences(
				Constants.MyPREFERENCES, 0);
	}

	private void setvalue() {
		// TODO Auto-generated method stub
		radious.add(0);
		radious.add(1);
		radious.add(2);
		radious.add(3);
		radious.add(4);
		radious.add(5);
		radious.add(6);
		radious.add(7);
		radious.add(8);
		radious.add(9);
		radious.add(10);
		type.add("Miles");
		type.add("Meter");
		type.add("Kilometer");
		System.out.println("sizeofarray====" + radious.size());
	}

	@Override
	protected void setValueOnUi() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void setListener() {
		// TODO Auto-generated method stub
		// setradiosButton.setOnClickListener(this);
		// incresBR.setOnClickListener(this);
		// decresBR.setOnClickListener(this);
		kilom_Buton.setOnClickListener(this);
		mile_Buton.setOnClickListener(this);
		// setradiosButton.setOnClickListener(this);
	}

	@Override
	public boolean onBackPressedListener() {
		// TODO Auto-generated method stub
		return false;
	}

}
