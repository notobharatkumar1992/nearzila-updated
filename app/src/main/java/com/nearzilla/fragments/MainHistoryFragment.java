package com.nearzilla.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.baseClasses.BaseFragment;
import com.nearzilla.AppDelegate;
import com.nearzilla.LocationsActivity;
import com.nearzilla.R;

import org.gmarz.googleplaces.models.Place;

import java.util.ArrayList;

public class MainHistoryFragment extends BaseFragment {

	public View view;
	private Button FavButton, HisButton;
	ImageView BacButton;
	int where = 0;
	ArrayList<Place> dbArray;
	RelativeLayout show_Linear;
	int flag = 0;
	Button delete, deleteAll;

	public static String TAG = "MAIN_HIS";

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.activity_history, null);
		initUi();
		setListner();
		loadFragment();
		return view;
	}

	private void loadFragment() {
		if (where == 0) {
			((LocationsActivity) getActivity()).getSupportActionBar().setTitle(
					"History");
			// replaceFragement(HistoryFragment.getInstance(null),
			// HistoryFragment.TAG);

			try {
				getChildFragmentManager().beginTransaction()
						.replace(R.id.container, new HistoryFragment())
						.addToBackStack(null).commitAllowingStateLoss();
			} catch (Exception e) {
				e.printStackTrace();
			}

			HisButton.setBackgroundResource(R.drawable.btn_selectright);
			// HisButton.setBackgroundColor(Color.parseColor("#000000"));
			FavButton.setBackgroundResource(R.drawable.btn_unselect);
			view.findViewById(R.id.view_his).setVisibility(View.VISIBLE);
			view.findViewById(R.id.view_fav).setVisibility(View.GONE);
		} else {
			// ((LocationsActivity)
			// getActivity()).getSupportActionBar().setTitle(
			// "Favorites");
			// replaceFragement(FavoritesFragment.getInstance(null),
			// FavoritesFragment.TAG);

			try {
				getChildFragmentManager().beginTransaction()
						.replace(R.id.container, new FavoritesFragment())
						.addToBackStack(null).commitAllowingStateLoss();
			} catch (Exception e) {
				e.printStackTrace();
			}

			FavButton.setBackgroundResource(R.drawable.btn_select);
			HisButton.setBackgroundResource(R.drawable.btn_unselect);
			view.findViewById(R.id.view_fav).setVisibility(View.VISIBLE);
			view.findViewById(R.id.view_his).setVisibility(View.GONE);
		}
	}

	private void setListner() {
		// TODO Auto-generated method stub
		FavButton.setOnClickListener(this);
		HisButton.setOnClickListener(this);
		show_Linear.setOnClickListener(this);
		delete.setOnClickListener(this);
		deleteAll.setOnClickListener(this);
	}

	private void initUi() {
		// TODO Auto-generated method stub
		FavButton = (Button) view.findViewById(R.id.fav_but);
		FavButton.setTypeface(AppDelegate.proxima_bold);
		HisButton = (Button) view.findViewById(R.id.his_but);
		HisButton.setTypeface(AppDelegate.proxima_bold);
		show_Linear = (RelativeLayout) view.findViewById(R.id.lin_his);
		delete = (Button) view.findViewById(R.id.delete_animate);
		deleteAll = (Button) view.findViewById(R.id.deleteall_animate);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.fav_but:
			where = 1;
			loadFragment();
			break;
		case R.id.his_but:
			where = 0;
			loadFragment();
			break;
		// case R.id.delete_animate:
		// if (where == 0) {
		// HistoryFragment fragment = (HistoryFragment) getActivity()
		// .getFragmentByTag(HistoryFragment.TAG);
		// fragment.delete();
		// } else {
		// FavoritesFragment fragment = (FavoritesFragment)
		// getFragmentByTag(FavoritesFragment.TAG);
		// fragment.delete();
		// }
		// break;
		// case R.id.deleteall_animate:
		// if (where == 0) {
		// HistoryFragment fragment = (HistoryFragment)
		// getFragmentByTag(HistoryFragment.TAG);
		// fragment.deleteAll();
		// } else {
		// FavoritesFragment fragment = (FavoritesFragment)
		// getFragmentByTag(FavoritesFragment.TAG);
		// fragment.deleteAll();
		// ;
		// }
		// break;
		default:
			break;
		}
	}

	@Override
	protected void initUi(View view) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void setValueOnUi() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void setListener() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onBackPressedListener() {
		// TODO Auto-generated method stub
		return false;
	}

	public static Fragment getInstance(Object object) {
		MainHistoryFragment fragment = new MainHistoryFragment();
		return fragment;
	}

}
