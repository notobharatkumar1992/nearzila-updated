package com.nearzilla.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.baseClasses.BaseFragment;
import com.baseClasses.Constants;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nearzilla.AppDelegate;
import com.nearzilla.Dialog_grid;
import com.nearzilla.R;
import com.nearzilla.adapters.PriceSpinnerAdapter;
import com.nearzilla.db.DBHelper;
import com.networkTask.ApiResponse;
import com.networkTask.ParsingClass;
import com.networkTask.URLsClass;
import com.squareup.picasso.Picasso;
import com.utilities.DeviceInfomation;
import com.utilities.ToastCustomClass;
import com.utilities.UtilsClass;

import org.apache.http.client.ClientProtocolException;
import org.gmarz.googleplaces.models.DetailsResult;
import org.gmarz.googleplaces.models.Place;
import org.gmarz.googleplaces.models.PlaceDetails;
import org.gmarz.googleplaces.models.Result.StatusCode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class LocationDetailFragment extends BaseFragment {

    public static String TAG = "LOC_DETAIL";
    // KEY Strings
    public static String KEY_REFERENCE = "reference"; // id of the place
    public static String KEY_NAME = "name"; // name of the place
    public static String KEY_VICINITY = "vicinity"; // Place area namedd
    // ListItems data
    ArrayList<Place> placesListItems = new ArrayList<Place>();
    private ImageView imgV_1;
    private TextView tv_locName;
    //private TextView tv_locCat;
    private TextView tv_address;
    private TextView tv_link;
    private ImageView tv_phoneNo;
    private TextView tv_address1;
    private TextView tv_dis;
    private Button btnPrice;
    private LinearLayout img_calender;
    private TextView tv_open;
    private TextView tv_openHour;
    private ImageView img_back;
    private ImageView img_like;
    //private TextView tv_review;
    //private TextView tv_rating;
    public Place place;
    private TextView feedback;
    private LinearLayout layout;
    String phone, website;
    GoogleMap googleMap;
    View view;
    MapView mMapView;
    double latitude, longitude;
    //String name;
    private PlaceDetails placeDetails;
    LinearLayout hour_lin;
    private TextView tv_reportLocation;
    private View linfeedback;
    private DBHelper dbHelper;
    private Spinner lin_price;

    public LocationDetailFragment() {
    }

    public static LocationDetailFragment getInstance(Bundle bundle) {
        LocationDetailFragment fragment = new LocationDetailFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
        Bundle bundle = getArguments();
        if (bundle != null) {
            place = (Place) getArguments().getParcelable(KEY_REFERENCE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_location_detail, null);
        initUi(view);
        setListener();
        mMapView = (MapView) view.findViewById(R.id.imgV_1);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume();// needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        googleMap = mMapView.getMap();
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_phoneNo:
                if (phone != null) {
                    makeCall(phone.trim());
                } else {
                    Toast.makeText(getActivity(), "NO number enter",
                            Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.img_back:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, place.getName() + "\n"
                        + place.getAddress());
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                break;
            case R.id.img_like:
                like();
                break;
            case R.id.detail_feedback:
                // startMyActivity(FeedbackDialog.class, null);
                break;
            case R.id.img_calender:
                if (placeDetails.getWeekDayTiming() != null) {
                    Bundle bundle = new Bundle();
                    bundle.putString("Weekday", placeDetails.getWeekDayTiming()
                            .toString());
                    Intent intent = new Intent(getActivity(), Dialog_grid.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                } else {
                    Toast.makeText(getActivity(), "No timing is given",
                            Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.tv_link:
                try {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse(website));
                    startActivity(browserIntent);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                break;
            case R.id.tv_address1:
                String uri = "http://maps.google.com/maps?f=d&hl=en&saddr="
                        + AppDelegate.getCurrentLatitude() + ","
                        + AppDelegate.getCurrentLongitude()
                        + "&daddr=" + latitude + "," + longitude;
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse(uri));
                startActivity(Intent.createChooser(intent, "Select an application"));
                break;
            default:
                break;
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setValueOnUi();
    }

    private void like() {

        SQLiteDatabase database = dbHelper.getWritableDatabase();
        if (dbHelper.islike(getActivity(), place.getPlaceId())) {
            dbHelper.deleteRecordSingle(place);
            dbHelper.setHistoryrow(database, "false", place.getPlaceId());
            Picasso.with(getActivity()).load(R.drawable.star_gray).into(img_like);
        } else {
            boolean returnVal = dbHelper.insertIntoDatabasefav(database, place);
            dbHelper.setHistoryrow(database, "true", place.getPlaceId());
            Picasso.with(getActivity()).load(R.drawable.star_yellow).into(img_like);
        }
    }

    @Override
    protected void initUi(View view) {
        linfeedback = view.findViewById(R.id.linfeedback);
        hour_lin = (LinearLayout) view.findViewById(R.id.lin_hoursToday);
        tv_address = (TextView) view.findViewById(R.id.tv_address);
        feedback = (TextView) view.findViewById(R.id.detail_feedback);
        feedback.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "proxima-nova-regular.ttf"));
        layout = (LinearLayout) view.findViewById(R.id.rel2);
        tv_locName = (TextView) view.findViewById(R.id.tv_locName);
        tv_locName.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "proxima-nova-bold.ttf"));
        img_like = (ImageView) view.findViewById(R.id.img_like);
        img_back = (ImageView) view.findViewById(R.id.img_back);
        tv_openHour = (TextView) view.findViewById(R.id.tv_openHour);
        lin_price = (Spinner) view.findViewById(R.id.lin_price);
        tv_openHour.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "proxima-nova-regular.ttf"));
        tv_open = (TextView) view.findViewById(R.id.tv_open);
        tv_open.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "proxima-nova-semibold.ttf"));
        img_calender = (LinearLayout) view.findViewById(R.id.img_calender);
        btnPrice = (Button) view.findViewById(R.id.btnPrice);
        tv_dis = (TextView) view.findViewById(R.id.tv_dis);
        //tv_dis.setTypeface(Typeface.createFromAsset(getActivity().getAssets(),"proxima-nova-bold.ttf"));
        tv_address1 = (TextView) view.findViewById(R.id.tv_address1);
        tv_address1.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "proxima-nova-semibold.ttf"));
        tv_phoneNo = (ImageView) view.findViewById(R.id.tv_phoneNo);
        tv_link = (TextView) view.findViewById(R.id.tv_link);
        tv_reportLocation = (TextView) view.findViewById(R.id.tv_reportLocation);
        tv_link.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "proxima-nova-semibold.ttf"));
        dbHelper = new DBHelper(getActivity());
        if (dbHelper.islike(getActivity(), place.getPlaceId())) {
            img_like.setImageResource(R.drawable.star_yellow);
        } else
            img_like.setImageResource(R.drawable.star_gray);
    }

    @Override
    protected void setValueOnUi() {
        new LoadSinglePlaceDetails().execute();
    }

    @Override
    protected void setListener() {
        feedback.setOnClickListener(this);
        tv_phoneNo.setOnClickListener(this);
        img_back.setOnClickListener(this);
        img_like.setOnClickListener(this);
        img_calender.setOnClickListener(this);
        tv_link.setOnClickListener(this);
        tv_address1.setOnClickListener(this);
    }

    @Override
    public boolean onBackPressedListener() {
        // TODO Auto-generated method stub
        return false;
    }

    /**
     * Background Async Task to Load Google places
     */
    class LoadSinglePlaceDetails extends AsyncTask<String, String, Place> {

        private ProgressDialog pDialog;

        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading profile ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * getting Profile JSON
         */
        protected Place doInBackground(String... args) {
            // creating Places class object
            DetailsResult detail;
            try {
                ApiResponse apiResponce = new ApiResponse();
                if (place.isBussiness) {
                    String url = URLsClass.service_type_getBusinessHits;
                    HashMap<String, Object> params = new HashMap<String, Object>();
                    params.put(Constants.businessId, place.getPlaceId());
                    params.put(Constants.deviceID, DeviceInfomation.getAndroid_device_id(getActivity()));
                    params.put(Constants.type, "2");
                    params.put(Constants.businessType, place.businesstype);

                    apiResponce = ParsingClass.getInstance(getActivity()).startUpConfigrationOfParsingPost(url, params, apiResponce, true);
                    String res = apiResponce.getResponceMap().get(url);
                    JSONObject jsonObject = new JSONObject(res);
                    PlaceDetails mDetails = new PlaceDetails(true, jsonObject.optJSONObject("data").optJSONObject("business"));
                    //PlaceDetails mDetails = detail.getDetails();
                    place.setDetail(mDetails);
                } else {
                    detail = Place.gPlaces.getPlaceDetails(place.getReference());
                    if (detail.getStatusCode() == StatusCode.OK) {
                        PlaceDetails mDetails = detail.getDetails();
                        place.setDetail(mDetails);
                    }
                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return place;
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        protected void onPostExecute(final Place place) {
            // dismiss the dialog after getting all products
            pDialog.dismiss();
            // updating UI from Background Thread
            /**
             * Updating parsed Places into LISTVIEW
             * */
            if (place != null) {
                String name = place.getName();
                getActionBar().setTitle(name);
                String address = place.getAddress();
                latitude = place.getLatitude();
                longitude = place.getLongitude();

                setLatLongomMap();

                String latitude = Double.toString(place.getLatitude());
                String longitude = Double.toString(place.getLongitude());
                String isOpenNow = place.isIsOpenNow();
                String rating = place.getmRating();

                // Price price = placeDetails.getPrice();
                // Hours hours = placeDetails.getHours();
                // JSONObject jObj = placeDetails.getJson();

                // Scope scope = placeDetails.getScope();
                // se.walkercrou.places.Status status =
                // placeDetails.getStatus();
                // List<String> types = placeDetails.getTypes();

                // Displaying all the details in the view
                // single_place.xml
                /*
                 * TextView lbl_name = (TextView) findViewById(R.id.name);
				 * TextView lbl_address = (TextView) findViewById(R.id.address);
				 * TextView lbl_phone = (TextView) findViewById(R.id.phone);
				 * TextView lbl_location = (TextView)
				 * findViewById(R.id.location);
				 */
                // Check for null data from google
                // Sometimes place details might missing
                name = name == null ? "Not present" : name; // if name is null
                // display as
                // "Not present"
                address = address == null ? "Not present" : address;
                latitude = latitude == null ? "Not present" : latitude;
                longitude = longitude == null ? "Not present" : longitude;

                tv_locName.setText(name);

                if (isOpenNow != null)
                    tv_open.setText(isOpenNow.equalsIgnoreCase("true") ? "Open" : "Closed");
                else {
                    tv_open.setVisibility(View.INVISIBLE);
                }
                /*try {
					tv_rating.setText(Double.parseDouble(rating) + "/5");
				} catch (NumberFormatException e) {					
					e.printStackTrace();
					tv_rating.setText("0/5");
				}*/

                if (place.getDistance() != null) {
                    tv_dis.setText(UtilsClass.getDisWithType(getActivity().getSharedPreferences(
                            Constants.MyPREFERENCES, 0).getString(Constants.disType,
                            SettingFragment.miles), place.getDistance()));
                } else {
                    tv_dis.setVisibility(View.GONE);
                }
                placeDetails = place.getDetail();
                if (placeDetails != null) {
                    if (placeDetails.getPhoneNumber() != null) {
                        phone = placeDetails.getPhoneNumber();
                    } else {
                        layout.setVisibility(View.GONE);
                    }
                    String phone = placeDetails.getPhoneNumber();
                    System.out.println("phonme==== " + phone);
                    website = placeDetails.getWebsite();
                    String iconCat = placeDetails.getIcon();

                    phone = phone == null ? "Not present" : phone;

                    if (iconCat != null && !iconCat.isEmpty())
                        if (phone.equals("") && website.equals("")) {
                            layout.setVisibility(View.GONE);
                        } else {
                            // tv_phoneNo.setText(Html.fromHtml(phone));
                        }

                    tv_link.setText(website);
                    tv_address1.setText(placeDetails.getAddressFull());

                    if (!place.isBussiness) {
                        // Linkify.addLinks(tv_link, Linkify.WEB_URLS);
                        int day = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
                        JSONArray a = placeDetails.getWeekDayTiming();
                        System.out.println("detailday+++++++" + placeDetails.getWeekDayTiming());

                        if (placeDetails.getWeekDayTiming() != null && placeDetails.getWeekDayTiming().optString(day) != null) {
                            String timeStr = placeDetails.getWeekDayTiming().optString((day > 0 ? (day - 1) : 6));

                            if (timeStr != null) {
                                String[] timeSpl = timeStr.split("day:");
                                System.out.println("arrayofday=====" + timeSpl);
                                if (timeSpl.length > 1) {
                                    tv_openHour.setText("Hours Today:\n"
                                            + timeSpl[1].trim());
                                } else {
                                    tv_openHour.setVisibility(View.INVISIBLE);
                                }
                            } else {
                                tv_openHour.setVisibility(View.INVISIBLE);
                            }
                        } else {

                            hour_lin.setVisibility(View.GONE);
                            btnPrice.setVisibility(View.GONE);
                        }
                    } else {
                        tv_locName.setText(placeDetails.businessName);
                        tv_address.setText(placeDetails.getAddressFull());
                        tv_address1.setText(placeDetails.getAddressFull());
                        tv_reportLocation.setText("Report a problem with this locaiton");
                        tv_reportLocation.setBackgroundResource(android.R.color.transparent);
                        setPriceAdapter(place.getOffersList());

                        tv_reportLocation.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                reportDialog(place.placeId);
                            }
                        });

                        linfeedback.setOnClickListener(new OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                reportTextDialog(true, place.placeId);
                            }
                        });

                        feedback.setOnClickListener(new OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                reportTextDialog(true, place.placeId);
                            }
                        });

                        hour_lin.setVisibility(View.GONE);
                        btnPrice.setVisibility(View.GONE);
                    }
                }
                // btnPrice.setText(price.name());
                // lbl_location.setText(Html.fromHtml("<b>Latitude:</b> " +
                // latitude + ", <b>Longitude:</b> " + longitude));
            }

        }
    }

    private void reportDialog(final String busId) {
        final Dialog dialog = new Dialog(getActivity());
        LayoutInflater inf = LayoutInflater.from(getActivity());
        View viewReport = inf.inflate(R.layout.dialog_report, null);
        Button btnNotExits = (Button) viewReport.findViewById(R.id.btn_notExits);
        Button btn_moved = (Button) viewReport.findViewById(R.id.btn_moved);
        Button btn_others = (Button) viewReport.findViewById(R.id.btn_others);
        btnNotExits.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                reportBusiness(busId, 1, "");
                dialog.dismiss();
            }
        });
        btn_moved.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                reportBusiness(busId, 2, "");
                dialog.dismiss();
            }
        });
        btn_others.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                reportTextDialog(false, busId);
                dialog.dismiss();
            }
        });

        LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);


        dialog.addContentView(viewReport, params);
        dialog.setTitle("Report Problem");
        dialog.show();
    }

    private void reportTextDialog(final Boolean isFeedback, final String busId) {
        final Dialog dialog = new Dialog(getActivity());
        LayoutInflater inf = LayoutInflater.from(getActivity());
        View viewReport = inf.inflate(R.layout.dialog_report_text, null);
        final EditText et_report = (EditText) viewReport.findViewById(R.id.et_report);
        Button btn_cancel = (Button) viewReport.findViewById(R.id.btn_cancel);
        Button btn_done = (Button) viewReport.findViewById(R.id.btn_done);
        btn_cancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btn_done.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (et_report.getText().toString().length() > 0) {
                    if (isFeedback) {
                        reportByFeeback(busId, et_report.getText().toString());
                    } else {
                        reportBusiness(busId, 3, et_report.getText().toString());
                    }
                    dialog.dismiss();
                } else
                    ToastCustomClass.showToast(getActivity(), "Please enter something");
            }
        });


        LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);


        dialog.addContentView(viewReport, params);
        if (isFeedback) {
            dialog.setTitle("Send Feedback");
            et_report.setHint("Enter feedback");
        } else {
            dialog.setTitle("Report Problem");
        }
        dialog.show();
    }

    private void reportBusiness(final String busId, final int tag, final String repText) {
        //business_id=180&type=1&device_id=223232323&report_type=1&text=
        AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {

            @Override
            protected String doInBackground(Void... args) {
                // TODO Auto-generated method stub
                String url = URLsClass.service_type_report;
                HashMap<String, Object> params = new HashMap<String, Object>();
                params.put(Constants.business_id, busId);
                params.put(Constants.type, "2");
                params.put("device_id", DeviceInfomation.getAndroid_device_id(getActivity()));
                params.put("report_type", tag);
                params.put("text", repText);
                ApiResponse apiResponce = new ApiResponse();
                apiResponce = ParsingClass.getInstance(getActivity()).startUpConfigrationOfParsingPost(url, params, apiResponce, true);
                String res = apiResponce.getResponceMap().get(url);

                try {
                    JSONObject jObj = new JSONObject(res);
                    String msg = jObj.optString(Constants.message);
                    return msg;
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                if (result != null) {
                    ToastCustomClass.showToast(getActivity(), result);
                }
            }

        };
        task.execute();
    }

    private void reportByFeeback(final String busId, final String repText) {
        //business_id=180&type=1&device_id=223232323&report_type=1&text=
        AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {

            @Override
            protected String doInBackground(Void... args) {
                String url = URLsClass.service_type_feedback;
                HashMap<String, Object> params = new HashMap<String, Object>();
                params.put(Constants.business_id, busId);
                params.put(Constants.type, "2");
                params.put("device_id", DeviceInfomation.getAndroid_device_id(getActivity()));
                params.put("feedback", repText);
                ApiResponse apiResponce = new ApiResponse();
                apiResponce = ParsingClass.getInstance(getActivity()).startUpConfigrationOfParsingPost(url, params, apiResponce, true);
                String res = apiResponce.getResponceMap().get(url);
                try {
                    JSONObject jObj = new JSONObject(res);
                    String msg = jObj.optString(Constants.message);
                    return msg;
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                if (result != null) {
                    ToastCustomClass.showToast(getActivity(), result);
                }

            }
        };
        task.execute();
    }

    private void makeCall(String no) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + no));
        startActivity(callIntent);
    }

    public boolean isDouble(String value) {
        try {
            Double.parseDouble(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public void setLatLongomMap() {
        MarkerOptions marker = new MarkerOptions().position(new LatLng(latitude, longitude)).title("");
        // Changing marker icon
        marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        // adding marker
        googleMap.addMarker(marker);
        CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(latitude, longitude)).zoom(12).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mMapView != null) {
            mMapView.onResume();
            googleMap = this.mMapView.getMap();
        }
    }

    public void setPriceAdapter(JSONArray jArr) {
        if (jArr != null && jArr.length() > 0) {
            lin_price.setVisibility(View.VISIBLE);
            PriceSpinnerAdapter adapter = new PriceSpinnerAdapter(getActivity(), jArr);
            lin_price.setAdapter(adapter);
            lin_price.setOnItemSelectedListener(new OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> arg0, View arg1,
                                           int arg2, long arg3) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onNothingSelected(AdapterView<?> arg0) {
                    // TODO Auto-generated method stub

                }
            });
        } else {
            lin_price.setVisibility(View.GONE);
        }
    }

}
