package com.nearzilla.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.location.Address;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.library21.custom.SwipeRefreshLayoutBottom;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.Spinner;

import com.baseClasses.BaseFragment;
import com.baseClasses.Constants;
import com.fortysevendeg.swipelistview.BaseSwipeListViewListener;
import com.fortysevendeg.swipelistview.SwipeListView;
import com.nearzilla.AppDelegate;
import com.nearzilla.HistoryActivity;
import com.nearzilla.MainSettingActivity;
import com.nearzilla.R;
import com.nearzilla.SearchActivity;
import com.nearzilla.adapters.AdapterReqSubCategory;
import com.nearzilla.adapters.LocDivAdapter;
import com.nearzilla.adapters.SpinnerAdapter;
import com.nearzilla.interfaces.OnReciveServerResponse;
import com.networkTask.ApiResponse;
import com.networkTask.Is_Internet_Available_Class;
import com.networkTask.ParsingClass;
import com.networkTask.URLsClass;
import com.preference.MySharedPreferences;
import com.utilities.ToastCustomClass;
import com.utilities.UtilsClass;

import org.gmarz.googleplaces.models.Place;
import org.gmarz.googleplaces.models.PlacesResult;
import org.gmarz.googleplaces.models.Result.StatusCode;
import org.gmarz.googleplaces.query.NearbySearchQuery.Ranking;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class LocDivFragment extends BaseFragment implements
        OnItemClickListener, OnClickListener, OnRefreshListener,
        SwipeRefreshLayoutBottom.OnRefreshListener, OnReciveServerResponse {

    String getLocationsFromServer = "http://rigges.com/temp2/webservices/getLocationDevice";
    // String getLocationsFromServer =
    // "http://192.168.1.14/riggies/webservices/getLocationDevice";
    public static String TAG = "LOC_DIV";
    // private com.fortysevendeg.swipelistview.SwipeListView listView;
    // private Spinner sp_find;
    // private AutoCompleteTextView et_near;
    private LocDivAdapter adapter;
    // KEY Strings
    public static String KEY_REFERENCE = "reference"; // id of the place
    public static String KEY_NAME = "name"; // name of the place
    public static String KEY_VICINITY = "vicinity"; // Place area name
    // Alert Dialog Manager
    // ListItems data
    ArrayList<Place> placesListItems = new ArrayList<Place>();
    // private boolean isButton;
    // private ImageView Search;
    // private RelativeLayout animate;
    LinearLayout gone;
    private String token = null;
    ProgressBar tokenProgres;
    // private SwipeRefreshLayoutBottom mSwipeRefreshLayout;
    LinearLayout refresh_Lin;
    double lat, lng;
    String cat = "";
    int catId = 0;
    // LinearLayout lin_setting, searchLin, homeLin, historyLin;
    private int pos_sublist;
    private SpinnerAdapter adapterSpin;
    ImageView ref_botomimg, serch_botomimg, home_botomimg, his_botomimg, close;
    String apend_distance;
    boolean buslen;
    private PlacesResult searchedPlace;
    boolean withRadius = true;
    private RelativeLayout root;
    private View searchFindView;
    private boolean isFirstTime = true;

    private SwipeRefreshLayoutBottom mSwipeRefreshLayout;
    private com.fortysevendeg.swipelistview.SwipeListView listView;

    public static OnReciveServerResponse serverResponse;

    String type = "atm%7cairport%7cart%20gallery%7cbakery%7cbeauty%20salon%7cbook%20store%7cbus%20station%7cbank%7cbars%7CCaf%C3%A9%7ccar%20repair%7ccar%20wash%7ccasino%7cclothing%20store%7cconvenience%20store%7cgas%20station%7cgrocery%20or%20supermarket%7cgym%7chair%20care%7chardware%20store%7chospital%7chotels%7clibrary%7cliquor%20store%7cmovie%20theater%7cmuseum%7cnight%20club%7cpark%7cpharmacy%7cpost%20office%7crestaurant%7cschool%7cshoe%20store%7cshopping%20mall%7cstore%7cuniversity";

    // public static char disType='M';

    public LocDivFragment() {
    }

    public static LocDivFragment getInstance(Bundle bundle) {
        LocDivFragment fragment = new LocDivFragment();
        fragment.catId = 0;
        fragment.cat = "";
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_locations, null);
//        ((LocationsActivity) getActivity()).restoreActionBar(true);
        serverResponse = this;
        initUi(view);
        setValueOnUi();
        setListener();
        addViewSearchFindLocations();
        showLayout(true);
        removeViewSearchFind();
        UtilsClass.hideKeyBoard(getActivity());
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lin_refresh:
                // showLayout(false);
                // ref_botomimg.setImageResource(R.drawable.refresh_hover);
                // serch_botomimg.setImageResource(R.drawable.searchicon);
                // home_botomimg.setImageResource(R.drawable.homegray);
                // his_botomimg.setImageResource(R.drawable.history);
                refresh();
                break;
            case R.id.lin_search:
                startMyActivity(SearchActivity.class, null);
            /*
             * ref_botomimg.setImageResource(R.drawable.refresh_botom);
			 * serch_botomimg.setImageResource(R.drawable.search_hover);
			 * home_botomimg.setImageResource(R.drawable.homegray);
			 * his_botomimg.setImageResource(R.drawable.history);
			 * showLayout(true);
			 */
                break;
            case R.id.lin_home:
            /*
             * catId=0; showLayout(false);
			 * replaceFragment(LocDivFragment.getInstance(null),
			 * LocDivFragment.TAG);
			 * ref_botomimg.setImageResource(R.drawable.refresh_botom);
			 * serch_botomimg.setImageResource(R.drawable.searchicon);
			 * home_botomimg.setImageResource(R.drawable.homegray1);
			 * his_botomimg.setImageResource(R.drawable.history);
			 */
                showLayout(true);
                ref_botomimg.setImageResource(R.drawable.refresh_botom);
                serch_botomimg.setImageResource(R.drawable.searchicon);
                home_botomimg.setImageResource(R.drawable.homegray);
                his_botomimg.setImageResource(R.drawable.history_hover);
                Bundle b = new Bundle();
                Intent mIntent1 = new Intent(getActivity(), HistoryActivity.class);
                b.putInt(Constants.type, 0);
                mIntent1.putExtras(b);
                startActivity(mIntent1);
                break;
            case R.id.lin_history:
                startMyActivity(MainSettingActivity.class, null);
                break;
            case R.id.sp_find:
                adapterSpin.isExpend = false;
                break;
            case R.id.lin_setting:
                startMyActivity(MainSettingActivity.class, null);
                break;

        }
    }

    public void refresh() {
        adapter.getList().clear();
        adapter.notifyDataSetChanged();
        MySharedPreferences.getInstance().putStringKeyValue(getActivity(),
                Constants.radious, 3100 + "");
        if (withRadius == true) {
            new LoadPlaces(lat, lng, "", false, true).execute();
        } else {
            new LoadPlaces(lat, lng, cat, false, true).execute();
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

	/*
     * private int radiusCalculation(int km){ return (int) (km * 1000 * 0.62); }
	 */

    @Override
    protected void initUi(View view) {
        root = (RelativeLayout) view.findViewById(R.id.root);
        close = (ImageView) view.findViewById(R.id.close);
        ref_botomimg = (ImageView) view.findViewById(R.id.img_linref);
        serch_botomimg = (ImageView) view.findViewById(R.id.img_linsearch);
        home_botomimg = (ImageView) view.findViewById(R.id.img_linhome);
        his_botomimg = (ImageView) view.findViewById(R.id.img_linhis);

        // lin_setting = (LinearLayout) view.findViewById(R.id.lin_setting);
        // searchLin = (LinearLayout) view.findViewById(R.id.lin_search);
        // homeLin = (LinearLayout) view.findViewById(R.id.lin_home);
        // historyLin = (LinearLayout) view.findViewById(R.id.lin_history);

        listView = (com.fortysevendeg.swipelistview.SwipeListView) view
                .findViewById(R.id.listView);
        listView.setSwipeMode(SwipeListView.SWIPE_MODE_RIGHT);
        listView.setAnimationTime(500);

        // Animation time
        // sp_find = (Spinner) view.findViewById(R.id.sp_find);
        // et_near = (AutoCompleteTextView) view.findViewById(R.id.et_near);
        // et_near.setTypeface(Typeface.createFromAsset(getActivity().getAssets(),"proxima-nova-regular.ttf"));
        // imgSearch = (ImageView) view.findViewById(R.id.imgSearch);
        // Search = (ImageView) view.findViewById(R.id.search);
        // animate = (RelativeLayout) view.findViewById(R.id.animate_rellay);
        // gone = (LinearLayout) view.findViewById(R.id.gone_lin);
        // imgRefresh = (ImageView) view.findViewById(R.id.imgREFRESH);

        mSwipeRefreshLayout = (SwipeRefreshLayoutBottom) view
                .findViewById(R.id.refresh_lin);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        mSwipeRefreshLayout.setColorSchemeResources(R.color.yellow,
                R.color.blue);

        adapter = new LocDivAdapter(getActivity(),
                new ArrayList<org.gmarz.googleplaces.models.Place>());
        listView.setAdapter(adapter);
        listView.setClickable(true);
        // listView.setVisibility(View.GONE);

        lat = AppDelegate.getCurrentLatitude();
        lng = AppDelegate.getCurrentLongitude();
        // Toast.makeText(getActivity(),
        // String.valueOf(lat),Toast.LENGTH_LONG).show();
        // Toast.makeText(getActivity(),
        // String.valueOf(lng),Toast.LENGTH_LONG).show();
    }

    boolean from_search = false;
    String search_name = "", url_query = "";

    @Override
    protected void setValueOnUi() {
        if (getArguments() != null) {
            try {
                if (getArguments().containsKey("category")) {
                    JSONObject object = new JSONObject(getArguments()
                            .getString("category"));
                    cat = object.getString("category");
                    catId = object.getInt("id");
                }

                search_name = getArguments().getString("name");
                url_query = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=AIzaSyAzQ6opqbcUbBXdjMJsSDRQh7Vw7LFikhc&location="
                        + lat
                        + ","
                        + lng
                        + "&radius=3100&rankBy=distance&sensor=true&keyword="
                        + cat + "&name=" + search_name;
                from_search = true;
            } catch (JSONException e) {
                e.printStackTrace();
                cat = URLsClass.categories;
            }
        } else {
            cat = URLsClass.categories;
        }

        if (Is_Internet_Available_Class.internetIsAvailable(getActivity())) {
            if (withRadius == true) {
                new LoadPlaces(lat, lng, cat, false, true).execute();
            } else {
                new LoadPlaces(lat, lng, cat, false, true).execute();
            }
        } else
            ToastCustomClass.showToast(getActivity(),
                    "Please start your internet");
    }

    @Override
    public void onResume() {
        super.onResume();
        if (adapterSpin != null)
            adapterSpin.isExpend = false;
    }

    @Override
    protected void setListener() {
        listView.setOnItemClickListener(this);
        listView.setSwipeMode(SwipeListView.SWIPE_MODE_NONE);
        listView.setSwipeListViewListener(new BaseSwipeListViewListener() {
            @Override
            public void onOpened(int position, boolean toRight) {
                Log.i("onOpened", "onOpened");
            }

            @Override
            public void onClosed(int position, boolean fromRight) {
                Log.i("onClosed", "onClosed");
            }

            @Override
            public void onListChanged() {
            }

            @Override
            public void onMove(int position, float x) {
                System.out.println("move====" + position);
                /*
                 * if(position > 5){ listView.resetScrolling(); }
				 */
            }

            @Override
            public void onStartOpen(int position, int action, boolean right) {
                Log.d("swipe", String.format("onStartOpen %d - action %d",
                        position, action));
                Log.i("onStartOpen", "onStartOpen");
            }

            @Override
            public void onStartClose(int position, boolean right) {
                Log.d("swipe", String.format("onStartClose %d", position));
                Log.i("onStartClose", "onStartClose");
            }

            @Override
            public void onClickFrontView(int position) {
                Log.i("onClickFrontView", "onClickFrontView");
                System.out.println("item click");

                Place place = adapter.getList().get(position);
//                CommonRedirect.goToDetail((BaseFragmentActivity) getActivity(),
//                        place);
                // swipelistview.openAnimate(position); //when you touch front
                // view it will open
            }

            @Override
            public void onDismiss(int[] reverseSortedPositions) {

            }

        });

    }

    @Override
    public boolean onBackPressedListener() {
        return false;
    }

    // private void loadGooglePlaces() {
    // creating GPS Class object
    // check if GPS location can get
    /*
     * if (gps.canGetLocation()) { Log.d("Your Location", "latitude:" +
	 * gps.getLatitude() + ", longitude: " + gps.getLongitude()); } else { //
	 * Can't get user's current location alert.showAlertDialog(getActivity(),
	 * "GPS Status", "Couldn't get location information. Please enable GPS",
	 * false); // stop executing code by return return; }
	 */

	/*
     * GooglePlaces googlePlaces = new GooglePlaces(URLsClass.GOOGLE_API_KEY);
	 * String types = "cafe|restaurant"; List<Place> places =
	 * googlePlaces.getPlacesByQuery(types, GooglePlaces.MAXIMUM_RESULTS);
	 * if(places != null && places.size() > 0){ PlacesList nearPlaces = new
	 * PlacesList(); nearPlaces.results = places; nearPlaces.status = "OK"; }
	 */
    // }

    /**
     * Before starting background thread Show Progress Dialog
     */
    class LoadPlaces extends AsyncTask<String, String, PlacesResult> {
        private ProgressDialog pDialog;
        private org.gmarz.googleplaces.GooglePlaces gPlaces;

        private double lat;
        private double lng;
        private String cat;
        // private String radius;
        private boolean addMore;
        private boolean showProgress;

        // private GooglePlaces googlePlaces;
        public LoadPlaces(double lat, double lng, String cat, boolean addMore,
                          boolean showProgress) {
            this.lat = lat;
            this.lng = lng;
            this.cat = cat;
            this.addMore = addMore;
            this.showProgress = showProgress;
            // this.radius="1000";
            System.out.println("catinservice====" + this.cat);
            /*
             * lat=26.894482; lng=75.834414;
			 */
            AppDelegate.setCustomLat(lat);
            AppDelegate.setCustomLng(lng);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (showProgress) {
                if (pDialog != null)
                    pDialog.dismiss();
                pDialog = null;
                pDialog = new ProgressDialog(getActivity());
                pDialog.setMessage(Html
                        .fromHtml("<b>Search</b><br/>Loading Places..."));
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(false);
                try {
                    pDialog.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        /**
         * getting Places JSON
         */
        protected PlacesResult doInBackground(String... args) {
            SharedPreferences preferences = getActivity().getSharedPreferences(
                    Constants.MyPREFERENCES, 0);
            int radius = Integer.parseInt(preferences.getString(
                    Constants.radious, "3100"));
            String catStr = "";
            if (catId == 0) {
                catStr = "";
            } else {
                catStr = catId + "";
            }
            // setCat();

            // disType = getDisType(getActivity());
            if (!addMore) {
                ApiResponse apiResponse = callServiceforPlaces(
                        lat,
                        lng,
                        catStr,
                        "0.3",
                        MySharedPreferences.getInstance().getString(
                                getActivity(), Constants.getSSID,
                                "36d9f2ss652c5s2s5s2s2"));
                HashMap<String, String> map = apiResponse.getResponceMap();
                String res = map.get(getLocationsFromServer);
                AppDelegate.LogUR(res);
                if (res != null) {
                    JSONObject object;
                    try {
                        object = new JSONObject(res);
                        if (object instanceof JSONObject) {
                            // jObRoot = jArr.getJSONObject(0);
                            JSONObject jOb = (JSONObject) object;
                            if (jOb != null
                                    && jOb.optInt(URLsClass.status,
                                    URLsClass.NOTOK) == URLsClass.OK) {
                                // ToastCustomClass.showToast(this, ":)");
                                parseData(jOb);
                            } else {
                                ToastCustomClass.showToast(getActivity(),
                                        jOb.optString(URLsClass.p_message));
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            // creating Places class object
            // googlePlaces = new GooglePlaces(URLsClass.GOOGLE_API_KEY);
            gPlaces = new org.gmarz.googleplaces.GooglePlaces(
                    URLsClass.GOOGLE_API_KEY);
            try {
                // String types = "restaurant"; // Listing places only cafes,
                // restaurants
                // Radius in meters - increase this value if you don't find any
                // places

                // System.out.println("bysetingradius=====" + radius);

                // int radius = 9000; // 1000 meters
                if (from_search) {

                    if (token != null && !token.equals("null") && !token.equals("")) {
                        if (withRadius == true) {
                            Log.d("test",
                                    "!token.equals() called withRadius == true => ");
                            searchedPlace = gPlaces.getPlaces(cat,
                                    googleRadius(radius), lat, lng, token,
                                    Ranking.prominence, true);
                        } else {
                            Log.d("test",
                                    "token.equals() called withRadius == true => ");
                            searchedPlace = gPlaces.getPlaces(cat,
                                    googleRadius(radius), lat, lng, token,
                                    Ranking.distance, false);
                        }
                        // token = null;

                    } else {
                        JSONObject response = gPlaces.executeRequest(url_query);
                        searchedPlace = new PlacesResult(response);
                    }
                } else {
                    if (token == null) {

                        if (withRadius == true) {
                            Log.d("test",
                                    "token == null called withRadius == true => ");
                            searchedPlace = gPlaces.getPlaces(type, 0, lat,
                                    lng, "", Ranking.distance, false);
                        } else {
                            Log.d("test",
                                    "token == null called withRadius != true=> ");
                            searchedPlace = gPlaces.getPlaces("",
                                    googleRadius(radius), lat, lng, "",
                                    Ranking.distance, false);
                        }
                        /*
                         * searchedPlace = gPlaces.getPlaces(cat, 5000, lat,
						 * lng, "", Ranking.distance);
						 */
                    } else if (!token.equals("")) {
                        if (withRadius == true) {
                            Log.d("test",
                                    "!token.equals() called withRadius == true => ");
                            searchedPlace = gPlaces.getPlaces(cat,
                                    googleRadius(radius), lat, lng, token,
                                    Ranking.prominence, true);
                        } else {
                            Log.d("test",
                                    "token.equals() called withRadius == true => ");
                            searchedPlace = gPlaces.getPlaces(cat,
                                    googleRadius(radius), lat, lng, token,
                                    Ranking.distance, false);
                        }
                        // token = null;

                    }
                }
                // sp_find.setSelection(0);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return searchedPlace;
        }

        /**
         * After completing background task Dismiss the progress dialog and show
         * the data in UI Always use runOnUiThread(new Runnable()) to update UI
         * from background thread, otherwise you will get error
         **/
        protected void onPostExecute(final PlacesResult nearPlaces) {
            // dismiss the dialog after getting all products
            if (pDialog != null)
                pDialog.dismiss();
            mSwipeRefreshLayout.setRefreshing(false);

            /**
             * Updating parsed Places into LISTVIEW
             * */

            if (nearPlaces != null) {
                // Check for all possible status
                if (nearPlaces.getStatusCode() == StatusCode.OK) {
                    // list adapter
                    System.out.println("placselist"
                            + nearPlaces.getPlaces().size());
//					insertIntoDatabase(nearPlaces.getPlaces());
                    // insertIntoDatabase(adapter.getList());
                    StringBuilder messages = new StringBuilder();
                    for (int i = 0; i < nearPlaces.getPlaces().size(); i++) {
                        messages.append(nearPlaces.getPlaces().get(i)
                                .getLatitude()
                                + ",");
                        messages.append(nearPlaces.getPlaces().get(i)
                                .getLongitude()
                                + "|");
                    }
                    apend_distance = messages.toString();

                    // System.out.println("apenddistance=======" +
                    // apend_distance);

                    adapter.addPlaces(nearPlaces.getPlaces(), pos_sublist,
                            listView);

                    if (isFirstTime && adapter.getCount() > 0) {
                        isFirstTime = false;
                        listView.setSelection(0);
                    }
                } else {
                    adapter.notifyDataSetChanged();
                    listView.invalidate();
                }
            } else {
                adapter.notifyDataSetChanged();
                listView.invalidate();
            }

        }
    }

	/*
     * private char getDisType(Context context){ SharedPreferences
	 * sharedpreferences = context.getSharedPreferences(Constants.MyPREFERENCES,
	 * 0); String disType = sharedpreferences.getString(Constants.disType,
	 * NewSettingActivity.miles); char type = 'M';
	 * if(disType.equalsIgnoreCase(NewSettingActivity.km)) type = 'K'; else type =
	 * 'M'; return type; }
	 */

    private int googleRadius(int radius) {
        return radius * 620;
    }

    private double bussnessRadius(int radius) {
        return (radius * 0.62);
    }

    private void parseData(JSONObject jsonObject) {
        JSONObject json_query = jsonObject.optJSONObject("response");
        JSONArray device = json_query.optJSONArray("devices");
        JSONArray buss = json_query.optJSONArray("business");
        JSONArray toplist = json_query.optJSONArray("toplisting");
        // DeviceModel deviceModel = new DeviceModel();

        List<Place> arraylist = adapter.getList();
        if (toplist != null)
            for (int i = 0; i < toplist.length(); i++) {
                Place place = new Place(toplist.optJSONObject(i), 0);
                // arraylist.add(place);
            }
        if (device != null)
            for (int i = 0; i < device.length(); i++) {
                Place place = new Place(device.optJSONObject(i), 0);
                // arraylist.add(place);
            }
        if (buss != null) {
            for (int i = 0; i < buss.length(); i++) {
                JSONObject jOb = buss.optJSONObject(i);
                Place place = new Place(jOb, 0);
                JSONArray jArr = jOb.optJSONArray(Constants.offers);
                if (jArr != null) {
                    place.addOfferList(jArr);
                }
                arraylist.add(place);
            }
        } else {
            buslen = true;
        }

        pos_sublist = arraylist.size();
//		insertIntoDatabase(arraylist);
    }

    private ApiResponse callServiceforPlaces(double lat, double lng,
                                             String catId, String radius, String ssid) {
        // TODO Auto-generated method stub
        System.out.println("calling setrvice" + radius);
        // String urlsArr[] = {URLsClass.service_type_getLocationDevice};

        HashMap<String, Object> params = new HashMap<String, Object>();
        // params.put(Constants.deviceId,
        // DeviceInfomation.getAndroid_device_id(getActivity()));
        params.put(Constants.lat, lat);
        params.put(Constants.lng, lng);
        // params.put(Constants.locationName, "Headline");
        params.put(Constants.radius, radius);
        params.put(Constants.ssid, ssid);
        params.put(Constants.catID, catId);
        ApiResponse apiResponse = new ApiResponse();
        apiResponse = ParsingClass.getInstance(getActivity())
                .startUpConfigrationOfParsingPost(getLocationsFromServer,
                        params, apiResponse, true);
        return apiResponse;
    }

//	private void insertIntoDatabase(List<Place> list) {
//		// Database creating process.
//		try {
//			DBHelper dbHelper = new DBHelper(getActivity());
//			dbHelper.insertIntoDatabase(null, list);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

    // current time and date.
    private String getCurrentDateTime() {
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy, HH:mm:ss");
        return df.format(Calendar.getInstance().getTime());
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
        Place place = (Place) view.getTag(R.id.title);
//        CommonRedirect.goToDetail( getActivity(), place);
    }

	/*
     * public void goToDetail(Place place){ Bundle bundle = new Bundle();
	 * bundle.putParcelable(KEY_REFERENCE, place); if (place.isBussiness) {
	 * if(place.businesstype.equalsIgnoreCase("1")){
	 * startMyActivity(LocationDetailActivity.class, bundle); }else{ //
	 * startMyActivity(BusinessDetailActivity.class, bundle); Bundle b = new
	 * Bundle(); String url = place.getUrl(); String name = place.getName();
	 * System.out.println("clickurlinlocdiv========" + name); Intent mIntent1 =
	 * new Intent(getActivity(), BusinessDetailActivity.class);
	 * b.putString("urlfordatabase", url); b.putString("namedetail",
	 * place.getName()); mIntent1.putExtras(b); startActivity(mIntent1); } }
	 * else { startMyActivity(LocationDetailActivity.class, bundle); } }
	 */

    @Override
    public void onRefresh() {
        if (searchedPlace != null)
            if (Is_Internet_Available_Class.internetIsAvailable(getActivity())) {
                token = searchedPlace.getToken();
                System.out.println("inloctokenvalue = " + token);
                if (token != null && !token.equals("null") && !token.equals("")) {
                    if (withRadius == true) {
                        new LoadPlaces(lat, lng, "", true, false).execute();
                    } else {
                        new LoadPlaces(lat, lng, cat, true, false).execute();
                    }
                } else {
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            } else {
                ToastCustomClass.showToast(getActivity(),
                        "Please start your internet");
            }
    }

	/*
     * public void showLayout(boolean set) { // TODO Auto-generated method stub
	 * System.out.println("image click"); if (set == true) { View[] view = {
	 * animate }; UtilsClass.animationSlide_in_down(getActivity(), view, 1000,
	 * 0); } else { View[] view1 = { animate }; boolean value =
	 * UtilsClass.animationSlide_out_up(getActivity(), view1, 1000, 0); } }
	 */

    private void addViewSearchFindLocations() {
        root.removeView(searchFindView);

        searchFindView = LayoutInflater.from(getActivity()).inflate(
                R.layout.layout_search_find, null);
        RelativeLayout.LayoutParams params = new LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        params.addRule(RelativeLayout.ALIGN_TOP, R.id.shaddow);
        ImageView close = (ImageView) searchFindView.findViewById(R.id.close);
        final Spinner sp_find = (Spinner) searchFindView
                .findViewById(R.id.sp_find);
        final AutoCompleteTextView et_near = (AutoCompleteTextView) searchFindView
                .findViewById(R.id.et_near);
        et_near.setTypeface(Typeface.createFromAsset(getActivity().getAssets(),
                "proxima-nova-regular.ttf"));
        // imgSearch = (ImageView) view.findViewById(R.id.imgSearch);
        ImageView Search = (ImageView) searchFindView.findViewById(R.id.search);
        // final RelativeLayout animate = (RelativeLayout)
        // searchFindView.findViewById(R.id.animate_rellay);
        searchFindView.setLayoutParams(params);
        searchFindView.setClickable(true);

        close.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                showLayout(false);
                et_near.setText("");
                sp_find.setSelection(0);
                listView.setClickable(true);
                // listView_1.setClickable(true);
            }
        });
        Search.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    JSONObject jsonObject = new JSONObject(sp_find
                            .getSelectedItem().toString());
                    cat = jsonObject.optString("category");
                    catId = jsonObject.optInt("id");
                    System.out.println("value=====" + cat);
                    adapter.getList().clear();
                    adapter.notifyDataSetChanged();
                    if (withRadius == true) {
                        new LoadPlaces(lat, lng, "", false, true).execute();
                        et_near.setText("");
                    } else {
                        new LoadPlaces(lat, lng, cat, false, true).execute();
                        et_near.setText("");
                    }

                    InputMethodManager inputManager = (InputMethodManager) v
                            .getContext().getSystemService(
                                    Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getActivity()
                                    .getCurrentFocus().getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                showLayout(false);
            }
        });

        sp_find.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                arg1.findViewById(R.id.vie_botom).setVisibility(View.GONE);
                System.out.println("click" + arg2);
                if (arg2 >= 0) {
                    withRadius = false;
                } else {
                    withRadius = true;
                }
                adapterSpin.isExpend = false;
                adapterSpin.send = AppDelegate
                        .getCategories().length();
                arg1.findViewById(R.id.vie_botom).setVisibility(View.GONE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                System.out.println("nothingclick");
                adapterSpin.isExpend = false;
                arg0.findViewById(R.id.vie_botom).setVisibility(View.GONE);
            }
        });

        adapterSpin = new com.nearzilla.adapters.SpinnerAdapter(getActivity(),
                AppDelegate.getCategories());
        // sp_find.setPrompt("Select your favorite Planet!");
        sp_find.setAdapter(adapterSpin);
        adapterSpin.getCount();

        JSONObject jsonObject;
        try {
            jsonObject = new JSONObject(sp_find.getSelectedItem().toString());
            cat = jsonObject.optString("category");
            catId = jsonObject.optInt("id");
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        final AdapterReqSubCategory adapter = new AdapterReqSubCategory(
                getActivity(), new ArrayList<Address>(),
                AppDelegate.getCurrentLatitude(),
                AppDelegate.getCurrentLongitude());
        et_near.setAdapter(adapter);
        et_near.setThreshold(1);
        et_near.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                Address add = ((Address) arg1.getTag(R.id.title));
                lat = add.getLatitude();
                lng = add.getLongitude();
                et_near.setText(add.getAddressLine(0) + " "
                        + add.getAddressLine(1));
            }
        });

        root.addView(searchFindView, params);
    }

    public void showLayout(boolean set) {
        System.out.println("image click");
        if (set) {
            addViewSearchFindLocations();
            if (searchFindView != null) {
                View[] view = {searchFindView};
                UtilsClass.animationSlide_in_down(getActivity(), view, 300, 0);
            }

        } else {
            if (searchFindView != null) {
                View[] view1 = {searchFindView};
                UtilsClass.animationSlide_out_up(getActivity(), view1, 300, 0);
            }
            removeViewSearchFind();
        }
    }

    public void removeViewSearchFind() {
        root.removeView(searchFindView);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        serverResponse = null;
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        if (apiName.equalsIgnoreCase("refresh")) {
            // showLayout(false);
            // ref_botomimg.setImageResource(R.drawable.refresh_hover);
            // serch_botomimg.setImageResource(R.drawable.searchicon);
            // home_botomimg.setImageResource(R.drawable.homegray);
            // his_botomimg.setImageResource(R.drawable.history);
            refresh();
        }
    }

}
