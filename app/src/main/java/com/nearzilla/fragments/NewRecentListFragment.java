package com.nearzilla.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.baseClasses.Constants;
import com.nearzilla.AppDelegate;
import com.nearzilla.Async.PostAsync;
import com.nearzilla.Model.PostAysnc_Model;
import com.nearzilla.R;
import com.nearzilla.adapters.NewRecentListAdapter;
import com.nearzilla.constant.Msg;
import com.nearzilla.constant.ServerRequestConstants;
import com.nearzilla.constant.Tags;
import com.nearzilla.db.DBHelper;
import com.nearzilla.interfaces.OnListItemClickListener;
import com.nearzilla.interfaces.OnReciveServerResponse;
import com.networkTask.Is_Internet_Available_Class;
import com.networkTask.URLsClass;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.preference.MySharedPreferences;
import com.utilities.GifMovieView;
import com.utilities.GifMovieViewScale2;

import org.gmarz.googleplaces.models.Place;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import carbon.widget.TextView;

/**
 * Created by Bharat on 09/16/2016.
 */
public class NewRecentListFragment extends Fragment implements AdapterView.OnItemClickListener, OnReciveServerResponse, OnListItemClickListener {

    String type = "atm%7cairport%7cart%20gallery%7cbakery%7cbeauty%20salon%7cbook%20store%7cbus%20station%7cbank%7cbars%7CCaf%C3%A9%7ccar%20repair%7ccar%20wash%7ccasino%7cclothing%20store%7cconvenience%20store%7cgas%20station%7cgrocery%20or%20supermarket%7cgym%7chair%20care%7chardware%20store%7chospital%7chotels%7clibrary%7cliquor%20store%7cmovie%20theater%7cmuseum%7cnight%20club%7cpark%7cpharmacy%7cpost%20office%7crestaurant%7cschool%7cshoe%20store%7cshopping%20mall%7cstore%7cuniversity";
    double lat, lng;
    String cat = "";
    int catId = 0;

    public static Handler mHandler;
    private NewRecentListAdapter adapter;

    public List<Place> arraylist = new ArrayList<Place>();
    public LinearLayout ll_retry;
    public TextView tv_c_retry;

    RelativeLayout loading_layout;

    private SwipyRefreshLayout swipyrefreshlayout;
    private RecyclerView recyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_recent_list_demo, container, false);
    }

    private ArrayList<Place> arrayFavList;

    private void showDatafromDatabase() {
        DBHelper dbHelper = new DBHelper(getActivity());
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        arrayFavList = dbHelper.readFromDatabase(database, false);
        AppDelegate.LogT("Recent frag => " + arrayFavList.size());
        for (int i = 0; i < arraylist.size(); i++) {
            arraylist.get(i).setIsFavorites("false");
        }
        if (arrayFavList.size() > 0 && arraylist.size() > 0) {
            for (int j = 0; j < arrayFavList.size(); j++) {
                for (int i = 0; i < arraylist.size(); i++) {
                    AppDelegate.LogT("main array => " + arraylist.get(i).getPlaceId() + " == " + arrayFavList.get(j).getPlaceId());
                    if (arraylist.get(i).getPlaceId().equalsIgnoreCase(arrayFavList.get(j).getPlaceId())) {
                        arraylist.get(i).setIsFavorites("true");
                        break;
                    }
                }
            }
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        setHandler();
        if (arraylist.size() == 0)
            setValueOnUi();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mHandler = null;
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == Msg.SHOW_PROGRESS) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == Msg.HIDE_PROGRESS) {
                    AppDelegate.hideProgressDialog(getActivity());
//                    loading_layout.setVisibility(View.GONE);
                } else if (msg.what == Msg.INVALIDATE_LIST_1) {
                    if (arraylist.size() > 0) {
                        ll_retry.setVisibility(View.GONE);
//                        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
//                        adapter = new NewFavouriteListAdapter(getActivity(), arraylist, NewRecentListFragment.this, NewFavouriteListAdapter.ADAPTER_RECENT);
//                        listView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                        recyclerView.invalidate();
//                        listView.setClickable(true);
//                        listView.performClick();
                    } else {
                        ll_retry.setVisibility(View.VISIBLE);
//                        mSwipeRefreshLayout.setVisibility(View.GONE);
                    }
                } else if (msg.what == Msg.INVALIDATE_LIST_2) {
                    if (arraylist.size() > 0) {
                        ll_retry.setVisibility(View.GONE);
//                        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                        showDatafromDatabase();
                        mHandler.sendEmptyMessage(Msg.INVALIDATE_LIST_1);
                    } else {
                        ll_retry.setVisibility(View.VISIBLE);
//                        mSwipeRefreshLayout.setVisibility(View.GONE);
                    }
                }
            }
        };
    }

    private void initView(View view) {
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);

        swipyrefreshlayout = (SwipyRefreshLayout) view.findViewById(R.id.swipyrefreshlayout);
        swipyrefreshlayout.setDirection(SwipyRefreshLayoutDirection.TOP);
        swipyrefreshlayout.setOnRefreshListener
                (new SwipyRefreshLayout.OnRefreshListener() {
                     @Override
                     public void onRefresh(SwipyRefreshLayoutDirection direction) {
                         if (direction == SwipyRefreshLayoutDirection.TOP) {
                             if (Is_Internet_Available_Class.internetIsAvailable(getActivity())) {
                                 ll_retry.setVisibility(View.GONE);
                                 callAsync();
                             } else {
                                 ll_retry.setVisibility(View.VISIBLE);
                             }
                         } else {
                             swipyrefreshlayout.setRefreshing(false);
                         }
                     }
                 }
                );

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setBackgroundColor(Color.WHITE);
        recyclerView.setPadding(0, 0, 0, 0);
        recyclerView.setLayoutManager(linearLayoutManager);
        adapter = new NewRecentListAdapter(getActivity(), arraylist, this);
        recyclerView.setAdapter(adapter);

        lat = AppDelegate.getCurrentLatitude();
        lng = AppDelegate.getCurrentLongitude();

        ll_retry = (LinearLayout) view.findViewById(R.id.ll_retry);
        ll_retry.setVisibility(View.GONE);

        tv_c_retry = (TextView) view.findViewById(R.id.tv_c_retry);
        tv_c_retry.setPaintFlags(tv_c_retry.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tv_c_retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Is_Internet_Available_Class.internetIsAvailable(getActivity())) {
                    ll_retry.setVisibility(View.GONE);
                    mHandler.sendEmptyMessage(Msg.SHOW_PROGRESS);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            callAsync();
                        }
                    }, 1500);
                } else {
                    ll_retry.setVisibility(View.VISIBLE);
                }
            }
        });


        loading_layout = (RelativeLayout) view.findViewById(R.id.loading_layout);
        GifMovieView gif1 = (GifMovieView) view.findViewById(R.id.gif1);
        gif1.setMovieResource(R.drawable.nearzila_1);
        GifMovieViewScale2 gif2 = (GifMovieViewScale2) view.findViewById(R.id.gif2);
        gif2.setMovieResource(R.drawable.nearzila_2);

    }

    boolean withRadius = true;
    boolean from_search = false;
    String search_name = "", url_query = "";

    private void setValueOnUi() {
        if (getArguments() != null) {
            try {
                if (getArguments().containsKey("category")) {
                    JSONObject object = new JSONObject(getArguments()
                            .getString("category"));
                    cat = object.getString("category");
                    catId = object.getInt("id");
                }

                search_name = getArguments().getString("name");
                url_query = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=AIzaSyAzQ6opqbcUbBXdjMJsSDRQh7Vw7LFikhc&location="
                        + lat
                        + ","
                        + lng
                        + "&radius=3100&rankBy=distance&sensor=true&keyword="
                        + cat + "&name=" + search_name;
                from_search = true;
            } catch (JSONException e) {
                e.printStackTrace();
                cat = URLsClass.categories;
            }
        } else {
            cat = URLsClass.categories;
        }

        if (Is_Internet_Available_Class.internetIsAvailable(getActivity())) {
            mHandler.sendEmptyMessage(Msg.SHOW_PROGRESS);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    callAsync();
                }
            }, 1500);
        } else {

            ll_retry.setVisibility(View.VISIBLE);
        }
    }

    private void callAsync() {
        SharedPreferences preferences = getActivity().getSharedPreferences(
                Constants.MyPREFERENCES, 0);
//        int radius = Integer.parseInt(preferences.getString(
//                Constants.radious, "0.30"));
        String catStr = "";
        if (catId == 0) {
            catStr = "";
        } else {
            catStr = catId + "";
        }
        String ssid = MySharedPreferences.getInstance().getString(getActivity(), Constants.getSSID, "36d9f2ss652c5s2s5s2s2");
//        ssid = ssid.replaceAll("\"", "");
        final WifiManager wifi = (WifiManager) getActivity().getSystemService(Context.WIFI_SERVICE);
        try {
            if (wifi.isWifiEnabled() == false) {
//            wifi.setWifiEnabled(true);
            } else {
                WifiInfo wifiInfo = wifi.getConnectionInfo();
                List<ScanResult> results = wifi.getScanResults();
                StringBuilder stringBuilder = null;
                for (ScanResult result : results) {
                    if (stringBuilder == null) {
                        stringBuilder = new StringBuilder();
                        stringBuilder.append("" + result.SSID + ",");
                    } else {
                        stringBuilder.append("" + result.SSID);
                    }
                }
                Log.d("wifiInfo", wifiInfo.toString());
                Log.d("SSID", wifiInfo.getSSID());
                ssid = stringBuilder.toString();
                MySharedPreferences.getInstance().putStringKeyValue(getActivity(), Constants.getSSID, wifiInfo.getSSID());
//            MySharedPreferences.getInstance().putStringKeyValue(this, Constants.getSSID, "");
            }
            ssid = ssid.replaceAll("\"", "");
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        try {
//            AppDelegate.showAlert(getActivity(), "Your wifi SSID is \"" + ssid + "\"");
            if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
//                44.667835, -63.560930
//                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.LAT, "44.667835");
//                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.LNG, "-63.560930");
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.LAT, lat);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.LNG, lng);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.catID, catStr);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.radius, "0.5");
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.ssid, ssid);
                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(getActivity(), this, ServerRequestConstants.getLocationsFromServer, mPostArrayList, null);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            mHandler.sendEmptyMessage(Msg.HIDE_PROGRESS);
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        if (mHandler == null) {
            return;
        }
        mHandler.sendEmptyMessage(Msg.HIDE_PROGRESS);
        swipyrefreshlayout.setRefreshing(false);
//        if (!AppDelegate.isValidString(result)) {
//            return;
//        }
        if (apiName.equals(ServerRequestConstants.getLocationsFromServer)) {
            if (AppDelegate.isValidString(result)) {
                try {
                    parseData(new JSONObject(result));
                } catch (JSONException e) {
                    AppDelegate.LogE(e);
                }
            } else {
                ll_retry.setVisibility(View.VISIBLE);
                mHandler.sendEmptyMessage(Msg.HIDE_PROGRESS);
            }
        }
    }

    private void parseData(JSONObject jsonObject) {
        JSONObject json_query = jsonObject.optJSONObject("response");
        try {
            if (json_query != null && AppDelegate.isValidString(json_query.toString()) && !json_query.toString().equalsIgnoreCase("[]")) {
                JSONArray device = json_query.optJSONArray("devices");
                JSONArray buss = json_query.optJSONArray("business");
                JSONArray toplist = json_query.optJSONArray("toplisting");
                // DeviceModel deviceModel = new DeviceModel();
                arraylist.clear();
//                try {
//                    if (toplist != null) {
//                        for (int i = 0; i < toplist.length(); i++) {
//                            Place place = new Place(toplist.optJSONObject(i), 0);
//                            arraylist.add(place);
//                        }
//                    }
//                } catch (Exception e) {
//                    AppDelegate.LogE(e);
//                }
//                try {
//                    if (device != null)
//                        for (int i = 0; i < device.length(); i++) {
//                            Place place = new Place(device.optJSONObject(i), 0);
//                            arraylist.add(place);
//                        }
//                } catch (Exception e) {
//                    AppDelegate.LogE(e);
//                }
                try {
                    if (buss != null) {
                        for (int i = 0; i < buss.length(); i++) {
                            JSONObject jOb = buss.optJSONObject(i);
                            Place place = new Place(jOb, 0);
                            JSONArray jArr = jOb.optJSONArray(Constants.offers);
                            if (jArr != null) {
                                place.addOfferList(jArr);
                            }
                            AppDelegate.LogT("data saved => " + CommonRedirect.saveToDB(getActivity(), place));
                            arraylist.add(place);
                        }
                    }
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        mHandler.sendEmptyMessage(Msg.INVALIDATE_LIST_2);
    }

    public void refresh() {
        adapter.notifyDataSetChanged();
        MySharedPreferences.getInstance().putStringKeyValue(getActivity(),
                Constants.radious, 3100 + "");
        callAsync();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
        Place place = (Place) view.getTag(R.id.title);
        AppDelegate.LogT("onItemClick place => " + place.getUrl());
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {
        if (name.equalsIgnoreCase("place")) {
            Place itemRow = arraylist.get(position);
            DBHelper dbHelper = new DBHelper(getActivity());
            SQLiteDatabase database = dbHelper.getWritableDatabase();
            if (itemRow.isFavorites.equalsIgnoreCase("false")) {
                itemRow.isFavorites = "true";
                dbHelper.setHistoryrow(database, itemRow.isFavorites, itemRow.getPlaceId());
                boolean returnVal = dbHelper.insertIntoDatabasefav(database, itemRow);
                AppDelegate.LogT("item added fav => " + itemRow.getName());
            } else {
                itemRow.isFavorites = "false";
                dbHelper.setHistoryrow(database, itemRow.isFavorites, itemRow.getPlaceId());
                dbHelper.deleteRecordSingle(itemRow);
                AppDelegate.LogT("item removed fav => " + itemRow.getName());
            }
            if (NewFavouriteFragment.mHandler != null) {
                NewFavouriteFragment.mHandler.sendEmptyMessage(Msg.INVALIDATE_LIST_2);
            }
            if (NewHistoryFragment.mHandler != null) {
                NewHistoryFragment.mHandler.sendEmptyMessage(Msg.INVALIDATE_LIST_2);
            }
            mHandler.sendEmptyMessage(Msg.INVALIDATE_LIST_1);
        }
    }
}

