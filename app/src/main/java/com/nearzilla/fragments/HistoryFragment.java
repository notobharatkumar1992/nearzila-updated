package com.nearzilla.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.baseClasses.BaseFragment;
import com.baseClasses.BaseFragmentActivity;
import com.nearzilla.R;
import com.nearzilla.adapters.WifiListHistoryAdapterNew;
import com.nearzilla.db.DBHelper;

import org.gmarz.googleplaces.models.Place;

import java.util.ArrayList;

public class HistoryFragment extends BaseFragment implements OnClickListener {
    public static final String TAG = "HIS";
    private Button favBUTTON;
    private ListView lvWifiList;
    private ArrayList<Place> arrayList;
    private WifiListHistoryAdapterNew adapter;
    private ImageView delete_record;

    private EditText searchView;
    TextView emptyText;
    ArrayList<String> nameArray;
    LinearLayout showLiner;
    int flag = 0;

    public static HistoryFragment getInstance(Bundle bundle) {
        HistoryFragment fragment = new HistoryFragment();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history, null);
        initUi(view);
        setListener();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        showDatafromDatabse();
        lvWifiList.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                Place place = adapter.getList().get(arg2);
                CommonRedirect.goToDetail((BaseFragmentActivity) getActivity(), place);
                /*Bundle bundle = new Bundle();
                bundle.putParcelable(KEY_REFERENCE, place);
				System.out.println("click====" + place.getDBisBussiness());
				if (place.getDBisBussiness().equals("1")) {
					// startMyActivity(BusinessDetailActivity.class, bundle);
					Bundle b = new Bundle();
					String url = place.getUrl();
					System.out.println("clickurlinlocdiv========" + url);
					Intent mIntent1 = new Intent(getActivity(),
							BusinessDetailActivity.class);
					b.putString("urlfordatabase", url);
					mIntent1.putExtras(b);
					startActivity(mIntent1);
				} else {
					startMyActivity(LocationDetailActivity.class, bundle);
				}*/
            }
        });
        return view;
    }


    private void showDatafromDatabse() {
        arrayList = new ArrayList<Place>();
        DBHelper dbHelper = new DBHelper(getActivity());
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        arrayList = dbHelper.readFromDatabaseHistory(database);
        setAdapter();
    }

    private void setAdapter() {
        try {
            if (arrayList.size() > 0) {
                Log.i("setAdapter11", "setAdapter");
                adapter = new WifiListHistoryAdapterNew(getActivity(), R.layout.row_wifilist_his, arrayList, true, 1);
                lvWifiList.setAdapter(adapter);
                lvWifiList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
                adapter.notifyDataSetChanged();
            } else {
                adapter = new WifiListHistoryAdapterNew(getActivity(), R.layout.row_wifilist_his, arrayList, true, 1);
                lvWifiList.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void initUi(View view) {
        lvWifiList = (ListView) view.findViewById(R.id.lvWifiList);
        delete_record = (ImageView) view.findViewById(R.id.delall_his);
        searchView = (EditText) view.findViewById(R.id.serachview);
        searchView.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "proxima-nova-regular.ttf"));
        searchView.clearFocus();
        emptyText = (TextView) view.findViewById(android.R.id.empty);
        lvWifiList.setEmptyView(emptyText);
        showLiner = (LinearLayout) view.findViewById(R.id.lin_his);
    }

    @Override
    protected void setValueOnUi() {
    }

    @Override
    protected void setListener() {
        delete_record.setOnClickListener(this);
        searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                adapter.getFilter().filter(s);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    @Override
    public boolean onBackPressedListener() {
        return false;
    }

    public ArrayList<Place> getArraylist() {
        DBHelper dbHelper = new DBHelper(getActivity());
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        arrayList = dbHelper.readFromDatabase(database, true);
        return arrayList;

    }

    private void callarraydblist() {
        ArrayList<Place> dbArray = adapter.getList();
        nameArray = new ArrayList<String>();
        for (int i = 0; i < dbArray.size(); i++) {
            if (dbArray.get(i).isValuetest()) {
                Place place = dbArray.get(i);
                nameArray.add(place.getName());
            }
        }
    }

    private void callDelete() {
        if (nameArray.size() > 0) {
            DBHelper dbHelper = new DBHelper(getActivity());
            SQLiteDatabase database = dbHelper.getWritableDatabase();
            dbHelper.deleteRecord(database, nameArray);
            showDatafromDatabse();
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_history, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().finish();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View arg0) {
        switch (arg0.getId()) {
            case R.id.delall_his:
                if (arrayList != null && arrayList.size() > 0) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            getActivity());
                    builder.setTitle("Alert");
                    builder.setCancelable(false);
                    builder.setMessage("Are you sure you want to delete these listings from your history?");
                    builder.setNegativeButton("Cancel", null);
                    builder.setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
                                    DBHelper dbHelper = new DBHelper(getActivity());
                                    SQLiteDatabase database = dbHelper
                                            .getWritableDatabase();
                                    dbHelper.deleteAllRecord(getActivity(), 0);
                                    showDatafromDatabse();
                                }
                            });

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }

                break;
            default:
                break;
        }
    }

    public void delete() {
        callarraydblist();
        if (nameArray != null && nameArray.size() > 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Alert");
            builder.setCancelable(false);
            builder.setMessage("Are you sure? You want to delete this item.");
            builder.setNegativeButton("Cancel", null);
            builder.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            callDelete();
                        }
                    });

            AlertDialog dialog = builder.create();
            dialog.show();
        }

    }

    public void deleteAll() {
        if (arrayList.size() > 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Alert");
            builder.setCancelable(false);
            builder.setMessage("Are you sure? You want to delete all items.");
            builder.setNegativeButton("Cancel", null);
            builder.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            DBHelper dbHelper = new DBHelper(getActivity());
                            SQLiteDatabase database = dbHelper
                                    .getWritableDatabase();
                            dbHelper.deleteAllRecord(getActivity(), 0);
                            showDatafromDatabse();
                        }
                    });

            AlertDialog dialog = builder.create();
            dialog.show();
        }

    }
}
