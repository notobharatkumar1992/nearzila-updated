package com.nearzilla.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SpinnerAdapter;

import com.baseClasses.BaseFragment;
import com.nearzilla.AppDelegate;
import com.nearzilla.LocationsActivity;
import com.nearzilla.R;
import com.utilities.UtilsClass;

import org.json.JSONException;
import org.json.JSONObject;

public class SearchFragment extends BaseFragment implements OnItemClickListener {
	SpinnerAdapter adapter;
	ListView listView;
	private ImageView img_search;
	private EditText et_search;

	public static SearchFragment getInstance(Bundle bundle) {
		SearchFragment fragment = new SearchFragment();
		if (bundle != null) {
			fragment.setArguments(bundle);
		}
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_search, null);
		initUi(view);
		setListener();
		return view;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void initUi(View view) {
		listView = (ListView) view.findViewById(R.id.list_search);
		adapter = new com.nearzilla.adapters.SpinnerAdapter(getActivity(),
				AppDelegate.getCategories());
		listView.setAdapter((ListAdapter) adapter);

		et_search = (EditText) view.findViewById(R.id.et_search);
		img_search = (ImageView) view.findViewById(R.id.img_search);
		img_search.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				UtilsClass.hideKeyBoard(getActivity());
				Bundle bundle = new Bundle();
				bundle.putString("name", et_search.getText().toString());

				((LocationsActivity) getActivity()).replaceFragement(
						LocDivFragment.getInstance(bundle), LocDivFragment.TAG);
				((LocationsActivity) getActivity()).updateTabView(0);
			}
		});
	}

	@Override
	protected void setValueOnUi() {

	}

	@Override
	protected void setListener() {
		listView.setOnItemClickListener(this);
	}

	@Override
	public boolean onBackPressedListener() {
		return false;
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		Log.i("itemclick", listView.getItemAtPosition(arg2).toString());

		try {
			JSONObject jsonObject = new JSONObject(listView.getItemAtPosition(
					arg2).toString());
			jsonObject.optString("category");
			Bundle bundle = new Bundle();
			bundle.putString("category", jsonObject.toString());
			bundle.putString("name", et_search.getText().toString());

			((LocationsActivity) getActivity()).replaceFragement(
					LocDivFragment.getInstance(bundle), LocDivFragment.TAG);
			((LocationsActivity) getActivity()).updateTabView(0);

			// Toast.makeText(getActivity(), jsonObject.optString("category"),
			// Toast.LENGTH_LONG).show();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
}
