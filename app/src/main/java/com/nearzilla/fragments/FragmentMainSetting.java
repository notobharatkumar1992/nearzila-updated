package com.nearzilla.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.baseClasses.BaseFragment;
import com.nearzilla.InAppBrowser;
import com.nearzilla.LocationsActivity;
import com.nearzilla.R;

public class FragmentMainSetting extends BaseFragment {

	RelativeLayout search, terms, privacy;

	public static FragmentMainSetting getInstance(Bundle bundle) {
		FragmentMainSetting fragment = new FragmentMainSetting();
		if (bundle != null) {
			fragment.setArguments(bundle);
		}
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_main_setting, null);
		initUi(view);
		setListener();
		return view;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.lin_setting:
			((LocationsActivity) getActivity())
					.replaceFragement(SettingFragment.getInstance(null),
							LocDivFragment.TAG, true);
			break;
		case R.id.lin_terms:
			getActivity().startActivity(
					new Intent(getActivity(), InAppBrowser.class));
			break;
		case R.id.lin_privacy:
			getActivity().startActivity(
					new Intent(getActivity(), InAppBrowser.class));
			break;
		default:
			break;
		}
	}

	@Override
	protected void initUi(View view) {
		// TODO Auto-generated method stub
		search = (RelativeLayout) view.findViewById(R.id.lin_setting);
		terms = (RelativeLayout) view.findViewById(R.id.lin_terms);
		privacy = (RelativeLayout) view.findViewById(R.id.lin_privacy);
	}

	@Override
	protected void setValueOnUi() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void setListener() {
		search.setOnClickListener(this);
		terms.setOnClickListener(this);
		privacy.setOnClickListener(this);
	}

	@Override
	public boolean onBackPressedListener() {
		return false;
	}
}
