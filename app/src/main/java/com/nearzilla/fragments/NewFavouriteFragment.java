package com.nearzilla.fragments;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.library21.custom.SwipeRefreshLayoutBottom;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fortysevendeg.swipelistview.BaseSwipeListViewListener;
import com.fortysevendeg.swipelistview.SwipeListView;
import com.nearzilla.AppDelegate;
import com.nearzilla.R;
import com.nearzilla.activities.MainActivity;
import com.nearzilla.adapters.NewFavouriteListAdapter;
import com.nearzilla.constant.Msg;
import com.nearzilla.db.DBHelper;
import com.nearzilla.interfaces.OnListItemClickListener;

import org.gmarz.googleplaces.models.Place;

import java.util.ArrayList;

/**
 * Created by Bharat on 09/19/2016.
 */
public class NewFavouriteFragment extends Fragment implements OnListItemClickListener {

    public static Handler mHandler;
    private SwipeRefreshLayoutBottom mSwipeRefreshLayout;
    private com.fortysevendeg.swipelistview.SwipeListView listView;
    private NewFavouriteListAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_locations, container, false);
    }

    private ArrayList<Place> arrayFavList = new ArrayList<>();

    public void getDataFromDB() {
        arrayFavList.clear();
        DBHelper dbHelper = new DBHelper(getActivity());
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        arrayFavList.addAll(dbHelper.readFromDatabase(database, false));
        AppDelegate.LogT("Fav frag => " + arrayFavList.size());
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        setHandler();
        setListener();
        mHandler.sendEmptyMessage(2);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mHandler = null;
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (!isAdded()) {
                    return;
                }
                if (msg.what == Msg.SHOW_PROGRESS) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == Msg.HIDE_PROGRESS) {
                    AppDelegate.hideProgressDialog(getActivity());
                } else if (msg.what == Msg.INVALIDATE_LIST_1) {
                    adapter.notifyDataSetChanged();
                    listView.invalidate();
                    listView.setClickable(true);
                    listView.performClick();
                } else if (msg.what == Msg.INVALIDATE_LIST_2) {
                    getDataFromDB();
                    mHandler.sendEmptyMessage(1);
                }
            }
        };
    }

    private void initView(View view) {
        listView = (com.fortysevendeg.swipelistview.SwipeListView) view.findViewById(R.id.listView);
        listView.setSwipeMode(SwipeListView.SWIPE_MODE_RIGHT);
        listView.setAnimationTime(500);
        mSwipeRefreshLayout = (SwipeRefreshLayoutBottom) view.findViewById(R.id.refresh_lin);
        mSwipeRefreshLayout.setRefreshing(false);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayoutBottom.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
        mSwipeRefreshLayout.setColorSchemeResources(R.color.yellow, R.color.blue);

        adapter = new NewFavouriteListAdapter(getActivity(), arrayFavList, this, MainActivity.ADAPTER_FAV);
        listView.setAdapter(adapter);
        listView.setClickable(true);
    }


    private void setListener() {
        listView.setSwipeMode(SwipeListView.SWIPE_MODE_NONE);
        listView.setSwipeListViewListener(new BaseSwipeListViewListener() {
            @Override
            public void onOpened(int position, boolean toRight) {
                Log.i("onOpened", "onOpened");
            }

            @Override
            public void onClosed(int position, boolean fromRight) {
                Log.i("onClosed", "onClosed");
            }

            @Override
            public void onListChanged() {
            }

            @Override
            public void onMove(int position, float x) {
                System.out.println("move====" + position);
            }

            @Override
            public void onStartOpen(int position, int action, boolean right) {
                Log.d("swipe", String.format("onStartOpen %d - action %d",
                        position, action));
                Log.i("onStartOpen", "onStartOpen");
            }

            @Override
            public void onStartClose(int position, boolean right) {
                Log.d("swipe", String.format("onStartClose %d", position));
                Log.i("onStartClose", "onStartClose");
            }

            @Override
            public void onClickFrontView(int position) {
                Log.i("onClickFrontView", "onClickFrontView");
                System.out.println("item click");

                Place place = adapter.getList().get(position);
//                CommonRedirect.goToDetail((BaseFragmentActivity) getActivity(),
//                        place);
                // swipelistview.openAnimate(position); //when you touch front
                // view it will open
            }

            @Override
            public void onDismiss(int[] reverseSortedPositions) {
            }
        });
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {
        if (name.equalsIgnoreCase("place")) {
            Place itemRow = arrayFavList.get(position);
            DBHelper dbHelper = new DBHelper(getActivity());
            SQLiteDatabase database = dbHelper.getWritableDatabase();
            itemRow.isFavorites = "false";
            dbHelper.setHistoryrow(database, itemRow.isFavorites, itemRow.getPlaceId());
            dbHelper.deleteRecordSingle(itemRow);
            AppDelegate.LogT("item removed fav => " + itemRow.getName());
            arrayFavList.remove(position);
            if (NewRecentListFragment.mHandler != null) {
                NewRecentListFragment.mHandler.sendEmptyMessage(Msg.INVALIDATE_LIST_2);
            }
            if (NewHistoryFragment.mHandler != null) {
                NewHistoryFragment.mHandler.sendEmptyMessage(Msg.INVALIDATE_LIST_2);
            }
            mHandler.sendEmptyMessage(Msg.INVALIDATE_LIST_1);
        }
    }
}
