package com.nearzilla.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.baseClasses.BaseFragmentActivity;
import com.nearzilla.BusinessDetailActivity;
import com.nearzilla.LocationDetailActivity;
import com.nearzilla.db.DBHelper;
import com.networkTask.Is_Internet_Available_Class;
import com.utilities.ToastCustomClass;

import org.gmarz.googleplaces.models.Place;

public class CommonRedirect {
    public static String KEY_REFERENCE = "reference";

    public static void goToDetail(BaseFragmentActivity context, Place place) {
        if (Is_Internet_Available_Class.internetIsAvailable(context)) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(KEY_REFERENCE, place);
            if (place.isBussiness) {
                if (place.businesstype.equalsIgnoreCase("1")) {
                    try {
                        DBHelper dbHelper = new DBHelper(context);
                        if (dbHelper.islike(context, place.getPlaceId())) {
                            place.setIsFavorites("true");
                        } else {
                            place.setIsFavorites("false");
                        }
                        dbHelper.insertIntoDatabase(null, place);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        context.startMyActivity(LocationDetailActivity.class, bundle);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    // startMyActivity(BusinessDetailActivity.class, bundle);
                    Bundle b = new Bundle();
                    String url = place.getUrl();
                    String name = place.getName();
                    System.out.println("clickurlinlocdiv========" + name);
                    Intent mIntent1 = new Intent(context, BusinessDetailActivity.class);
                    b.putString("urlfordatabase", url);
                    b.putString("namedetail", place.getName());
                    mIntent1.putExtras(b);
                    context.startActivity(mIntent1);
                }
            } else {
                try {
                    DBHelper dbHelper = new DBHelper(context);
                    if (dbHelper.islike(context, place.getPlaceId())) {
                        place.setIsFavorites("true");
                    } else {
                        place.setIsFavorites("false");
                    }
                    dbHelper.insertIntoDatabase(null, place);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                context.startMyActivity(LocationDetailActivity.class, bundle);
            }
        } else
            ToastCustomClass.showToast(context, "Please start your internet");
    }


    public static boolean saveToDB(Context context, Place place) {
        if (Is_Internet_Available_Class.internetIsAvailable(context)) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(KEY_REFERENCE, place);
            if (place.isBussiness) {
//                if (place.businesstype.equalsIgnoreCase("1")) {
                try {
                    DBHelper dbHelper = new DBHelper(context);
                    if (dbHelper.islike(context, place.getPlaceId())) {
                        place.setIsFavorites("true");
                    } else {
                        place.setIsFavorites("false");
                    }
                    dbHelper.insertIntoDatabase(null, place);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
//                }
            } else {
                try {
                    DBHelper dbHelper = new DBHelper(context);
                    if (dbHelper.islike(context, place.getPlaceId())) {
                        place.setIsFavorites("true");
                    } else {
                        place.setIsFavorites("false");
                    }
                    dbHelper.insertIntoDatabase(null, place);
                    return true;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            ToastCustomClass.showToast(context, "Please start your internet");
        }
        return false;
    }
}
