package com.nearzilla.fragments;

import java.util.ArrayList;

import org.gmarz.googleplaces.models.Place;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.baseClasses.BaseFragment;
import com.baseClasses.BaseFragmentActivity;
import com.nearzilla.BusinessDetailActivity;
import com.nearzilla.LocationDetailActivity;
import com.nearzilla.R;
import com.nearzilla.adapters.WifiListHistoryAdapterNew;
import com.nearzilla.db.DBHelper;

public class FavoritesFragment extends BaseFragment {
	public static final String TAG = "FAV";
	private ArrayList<Place> arrayList;
	private ListView lvWifiList;
	private WifiListHistoryAdapterNew adapter;
	private ImageView delete_Frecord;
	private EditText searEditText;
	public static String KEY_REFERENCE = "reference";
	private TextView emptyView;
	ArrayList<String> nameArray;

	public static FavoritesFragment getInstance(Bundle bundle) {
		FavoritesFragment fragment = new FavoritesFragment();
		if (bundle != null) {
			fragment.setArguments(bundle);
		}
		return fragment;

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		// setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_favorites, null);
		// Toast.makeText(getActivity(), "Fav", Toast.LENGTH_SHORT).show();
		initUi(view);
		setListener();
		setdatabasevalue();
		return view;
	}

	private void setdatabasevalue() {
		// TODO Auto-generated method stub
		lvWifiList.setTextFilterEnabled(true);
		arrayList = new ArrayList<Place>();
		DBHelper dbHelper = new DBHelper(getActivity());
		SQLiteDatabase database = dbHelper.getWritableDatabase();
		arrayList = dbHelper.readFromDatabase(database, false);
		setAdapter();
	}

	private void setAdapter() {
		try {
			if (arrayList.size() > 0) {
				// emptyView.setVisibility(View.GONE);
				Log.i("setAdapter11", "setAdapter");
				adapter = new WifiListHistoryAdapterNew(getActivity(),
						R.layout.row_wifilist_his, arrayList, true, 0);
				lvWifiList.setAdapter(adapter);
				adapter.notifyDataSetChanged();
			} else {
				adapter = new WifiListHistoryAdapterNew(getActivity(),
						R.layout.row_wifilist_his, arrayList, true, 0);
				lvWifiList.setAdapter(adapter);
				adapter.notifyDataSetChanged();
				// emptyView.setVisibility(View.VISIBLE);
				// emptyView.setText("No Data in list");
			}
			searEditText.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					adapter.getFilter().filter(s);

				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});
			lvWifiList.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					Place place = adapter.getList().get(arg2);
					CommonRedirect.goToDetail(
							(BaseFragmentActivity) getActivity(), place);
					/*
					 * Bundle bundle = new Bundle();
					 * bundle.putParcelable(KEY_REFERENCE, place);
					 * System.out.println("click====" +
					 * place.getDBisBussiness()); if
					 * (place.getDBisBussiness().equals("1")) { //
					 * startMyActivity(BusinessDetailActivity.class, // bundle);
					 * Bundle b = new Bundle(); String url = place.getUrl();
					 * System.out.println("clickurlinlocdiv========" + url);
					 * Intent mIntent1 = new Intent(getActivity(),
					 * BusinessDetailActivity.class);
					 * b.putString("urlfordatabase", url);
					 * mIntent1.putExtras(b); startActivity(mIntent1);
					 * 
					 * } else { startMyActivity(LocationDetailActivity.class,
					 * bundle); }
					 */
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void initUi(View view) {
		// TODO Auto-generated method stub
		lvWifiList = (ListView) view.findViewById(R.id.lvWifiList);
		// delete_Frecord = (ImageView) view.findViewById(R.id.delete_Frecord);
		searEditText = (EditText) view.findViewById(R.id.serachviewF);
		searEditText.setTypeface(Typeface.createFromAsset(getActivity()
				.getAssets(), "proxima-nova-regular.ttf"));
		emptyView = (TextView) view.findViewById(android.R.id.empty);
		lvWifiList.setEmptyView(emptyView);
	}

	@Override
	protected void setValueOnUi() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void setListener() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onBackPressedListener() {
		// TODO Auto-generated method stub
		return false;
	}

	private void callarraydblist() {

		// TODO Auto-generated method stub
		ArrayList<Place> dbArray = adapter.getList();
		nameArray = new ArrayList<String>();
		for (int i = 0; i < dbArray.size(); i++) {
			if (dbArray.get(i).isValuetest()) {
				Place place = dbArray.get(i);
				nameArray.add(place.getName());
			}
		}
	}

	private void callDelete() {
		if (nameArray.size() > 0) {

			DBHelper dbHelper = new DBHelper(getActivity());
			SQLiteDatabase database = dbHelper.getWritableDatabase();

			dbHelper.deleteRecordFav(database, nameArray);
			setdatabasevalue();
		}

	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// inflater.inflate(R.menu.menu_history, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();
		switch (item.getItemId()) {

		/*
		 * case R.id.history_delete:
		 * 
		 * } return true; case R.id.history_deleteall: if(arrayList.size()>0){
		 * AlertDialog.Builder builder = new AlertDialog.Builder(
		 * getActivity()); builder.setTitle("Alert");
		 * builder.setCancelable(false);
		 * builder.setMessage("Are you sure? You want to delete all items.");
		 * builder.setNegativeButton("Cancel", null);
		 * builder.setPositiveButton("Ok", new DialogInterface.OnClickListener()
		 * {
		 * 
		 * @Override public void onClick(DialogInterface arg0, int arg1) {
		 * 
		 * InputMethodManager inputManager = (InputMethodManager) ((Dialog)
		 * arg0) .getContext().getSystemService( Context.INPUT_METHOD_SERVICE);
		 * inputManager.hideSoftInputFromWindow( getActivity().getCurrentFocus()
		 * .getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS); DBHelper
		 * dbHelper = new DBHelper(getActivity()); SQLiteDatabase database =
		 * dbHelper.getWritableDatabase();
		 * dbHelper.deleteAllRecord(getActivity(),1); setdatabasevalue();
		 * 
		 * } });
		 * 
		 * AlertDialog dialog = builder.create(); dialog.show(); } return true;
		 */
		case android.R.id.home:
			getActivity().finish();
			break;
		default:
			break;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

	public void delete() {
		callarraydblist();
		if (nameArray != null && nameArray.size() > 0) {
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setTitle("Alert");
			builder.setCancelable(false);
			builder.setMessage("Are you sure? You want to delete this item.");
			builder.setNegativeButton("Cancel", null);
			builder.setPositiveButton("Ok",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface arg0, int arg1) {
							callDelete();

						}
					});

			AlertDialog dialog = builder.create();
			dialog.show();
		}

	}

	public void deleteAll() {
		if (arrayList.size() > 0) {
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setTitle("Alert");
			builder.setCancelable(false);
			builder.setMessage("Are you sure? You want to delete all items.");
			builder.setNegativeButton("Cancel", null);
			builder.setPositiveButton("Ok",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface arg0, int arg1) {

							InputMethodManager inputManager = (InputMethodManager) ((Dialog) arg0)
									.getContext().getSystemService(
											Context.INPUT_METHOD_SERVICE);
							inputManager.hideSoftInputFromWindow(getActivity()
									.getCurrentFocus().getWindowToken(),
									InputMethodManager.HIDE_NOT_ALWAYS);
							DBHelper dbHelper = new DBHelper(getActivity());
							SQLiteDatabase database = dbHelper
									.getWritableDatabase();
							dbHelper.deleteAllRecord(getActivity(), 1);
							setdatabasevalue();

						}
					});

			AlertDialog dialog = builder.create();
			dialog.show();
		}
	}
}
