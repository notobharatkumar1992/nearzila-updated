package com.nearzilla.customView;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.widget.ImageView;

import com.nearzilla.R;

public class MyProgressDialog extends ProgressDialog {

	private AnimationDrawable anim;

	public MyProgressDialog(Context context) {
		super(context);
		// super(context,R.style.progressDialog);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {	
		super.onCreate(savedInstanceState);
		getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		setContentView(R.layout.butter_fly);			
		ImageView img = (ImageView) findViewById(R.id.imageView1);
		anim = (AnimationDrawable) img.getBackground();
	}

	@Override
	public void show() {	
		super.show();
		anim.start();
	}
	
	@Override
	public void dismiss() {
		anim.stop();
		super.dismiss();	
	}	
}
