package com.baseClasses;

import android.app.Application;
import android.graphics.Typeface;
import android.net.wifi.ScanResult;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//import com.instabug.library.Instabug;

public class MyApplication extends Application {

    //private ApiResponse apiResponce;
    private double currentLatitude;
    private double currentLongitude;
    // Screen in Pixels
    private int widthPixel = 1080;
    private int heightPixel = 1920;
    public static long locationDetectionTimeInMin;
    private Date date = null;
    //    private static MyApplication myApplication;
    public static Typeface typeJACKPORT_REGULAR_NCV;
    public static Typeface typeFaceOrgano;
    public static Typeface typeFaceSkipLegDay;
    public static Typeface typeFaceStarzy_Darzy;
    public static List<ScanResult> listofValidwifi = new ArrayList<ScanResult>();


    private JSONArray categories;
    private JSONArray detail;
    private double custLat;
    private double custLng;
    public Typeface proxima_bold;
    public Typeface proxima_regular;
    public Typeface proxima_semibold;

    @Override
    public void onCreate() {
        super.onCreate();
//		Instabug.initialize(this, "1afbdf1647e2bba1a521ec80f4beff47");
        // Get Date ----------------
        date = new Date();

//        myApplication = this;

        proxima_bold = Typeface.createFromAsset(getApplicationContext().getAssets(), "proxima-nova-bold.ttf");
        proxima_regular = Typeface.createFromAsset(getApplicationContext().getAssets(), "proxima-nova-regular.ttf");
        proxima_semibold = Typeface.createFromAsset(getApplicationContext().getAssets(), "proxima-nova-semibold.ttf");
    }

//    public static synchronized MyApplication getApplication() {
//        return myApplication;
//    }

    public double getCurrentLongitude() {
        return currentLongitude;
    }

    public void setCurrentLongitude(double currentLongitude) {
        this.currentLongitude = currentLongitude;
    }

    public double getCurrentLatitude() {
        return currentLatitude;
    }

    public void setCurrentLatitude(double currentLatitude) {
        this.currentLatitude = currentLatitude;
    }

    public int getWidthPixel() {
        return widthPixel;
    }

    public void setWidthPixel(int widthPixel) {
        this.widthPixel = widthPixel;
    }

    public int getHeightPixel() {
        return heightPixel;
    }

    public void setHeightPixel(int heightPixel) {
        this.heightPixel = heightPixel;
    }

    public void setCategories(JSONArray jArr) {
        this.categories = jArr;
    }

    public JSONArray getCategories() {
        return categories;
    }

    public void setDetial(JSONArray jArr) {
        this.detail = jArr;
    }

    public JSONArray getDetial() {
        return detail;
    }

    public void setCustomLat(double lat) {
        // TODO Auto-generated method stub
        custLat = lat;
    }

    public void setCustomLng(double lng) {
        // TODO Auto-generated method stub
        custLng = lng;
    }

    public double getCustLat() {
        return custLat;
    }


    public double getCustLng() {
        return custLng;
    }
}

