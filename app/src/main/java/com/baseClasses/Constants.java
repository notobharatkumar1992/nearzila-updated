package com.baseClasses;

public interface Constants {
	public static final String bundleArg = "bundle";
	public static final String title = "title";	
	public static final String password = "password";
	public static final String name = "name";
	public static final String userImageURL = "imgURL";
	public static final String type = "type";

	public static final int requestCode = 101;

	public static final String list = "list";
	public static final String className = "className";

	public final static String statusOk = "accepted";	
	public final static String statusNotOk = "rejected";
	public final static String success = "Success";
	public final static String dataToFollow = "dataflow";
	public static final String msg = "msg";
	public static final String message = "message";
	public static final String status = "status";	
	public final static String deviceId = "deviceId";
	public final static String lat = "lat";
	public final static String lng = "lng";
	public final static String locationName = "locationName";
	public final static String ssid = "ssid";
	public final static String catID = "catID";
	public final static String radius = "radius";
	public static final String map = "map";
	public static final String set = "set";
	public static final String jObject = "jObject";
	public static final String guest = "guest";
	public static final String position = "position";	
	public static final String url = "url";

	public static final String images = "Images";
	public static final String FRAGMENT_TAG = "frag";

	public static final String object = "object";

	public static final int getCat = 3004;
	public static final int getLocationFromDatabase = 3005;
	public static final int getLocationDevice = 3010;

	//Shardeprefs
	public static final String VERIFY = "verify";
	public static final String VERIFY_NO = "verify_no";
	public static final String EMAIL = "email";
	public static final String USER_NAME = "userName";
	public static final String PH_NUMBER = "phone_no";
	public static final String COUNTRY_CODE = "country_code";
	public static final int INT = 1;
	public static final int FLOW = 1;
	public static final int NOT_FLOW = 0;


	public static final int STATUS = 1;
	public static final int NOT_STATUS = 0;
	public static final String response = "response";
	public static final String category = "category";
	public static final String MyPREFERENCES = "RADIOUS";
	public static final String deletevalue = "deleteposition";
	public static final String getSSID = "getSSID";
	public static final String radious = "radious";
	public static final String businessId = "businessId";
	public static final String business_id = "business_id";
	public static final String deviceID = "deviceID";
	public static final String businessType = "businessType";
	public static final String offers = "offers";
	public static final String disType = "disType";
}
