ECLIPSE ANDROID PROJECT IMPORT SUMMARY
======================================

Risky Project Location:
-----------------------
The tools *should* handle project locations in any directory. However,
due to bugs, placing projects in directories containing spaces in the
path, or characters like ", ' and &, have had issues. We're working to
eliminate these bugs, but to save yourself headaches you may want to
move your project to a location where this is not a problem.
E:\Bharat Kumar\NearZilla\NearZilaStudio\Nearzilla
         -                                        

Ignored Files:
--------------
The following files were *not* copied into the new Gradle project; you
should evaluate whether these are still needed in your project and if
so manually move them:

* proguard-project.txt

Replaced Jars with Dependencies:
--------------------------------
The importer recognized the following .jar files as third party
libraries and replaced them with Gradle dependencies instead. This has
the advantage that more explicit version information is known, and the
libraries can be updated automatically. However, it is possible that
the .jar file in your project was of an older version than the
dependency we picked, which could render the project not compileable.
You can disable the jar replacement in the import wizard and try again:

android-support-v4.jar => com.android.support:support-v4:18.+
android-support-v7-appcompat.jar => com.android.support:appcompat-v7:18.+

Replaced Libraries with Dependencies:
-------------------------------------
The importer recognized the following library projects as third party
libraries and replaced them with Gradle dependencies instead. This has
the advantage that more explicit version information is known, and the
libraries can be updated automatically. However, it is possible that
the source files in your project were of an older version than the
dependency we picked, which could render the project not compileable.
You can disable the library replacement in the import wizard and try
again:

android-support-v7-appcompat => [com.android.support:appcompat-v7:18.+]

Moved Files:
------------
Android Gradle projects use a different directory structure than ADT
Eclipse projects. Here's how the projects were restructured:

* AndroidManifest.xml => instabugwrapper\src\main\AndroidManifest.xml
* assets\ => instabugwrapper\src\main\assets
* libs\instabugcore.jar => instabugwrapper\libs\instabugcore.jar
* libs\instabugsupport.jar => instabugwrapper\libs\instabugsupport.jar
* libs\mimecraft-1.1.1.jar => instabugwrapper\libs\mimecraft-1.1.1.jar
* libs\volley.jar => instabugwrapper\libs\volley.jar
* res\ => instabugwrapper\src\main\res\
* src\ => instabugwrapper\src\main\java

Missing Android Support Repository:
-----------------------------------
Some useful libraries, such as the Android Support Library, are
installed from a special Maven repository, which should be installed
via the SDK manager.

It looks like this library is missing from your SDK installation at:
E:\Bharat Kumar\Eclipse\adt-bundle-windows-x86_64-20130729\adt-bundle-windows-x86_64-20130729\sdk

To install it, open the SDK manager, and in the Extras category,
select "Android Support Repository". You may also want to install the
"Google Repository" if you want to use libraries like Google Play
Services.

Old Build Tools:
----------------
The version of the build tools installed with your SDK is old. It
should be at least version 19.0.1 to work well with the Gradle build
system. To update it, open the Android SDK Manager, and install the
highest available version of Tools > Android SDK Build-tools.

Next Steps:
-----------
You can now build the project. The Gradle project needs network
connectivity to download dependencies.

Bugs:
-----
If for some reason your project does not build, and you determine that
it is due to a bug or limitation of the Eclipse to Gradle importer,
please file a bug at http://b.android.com with category
Component-Tools.

(This import summary is for your information only, and can be deleted
after import once you are satisfied with the results.)
